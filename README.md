# README #


* This repository is for the frontend web application of Bentanayan

* [ReactJS](https://facebook.github.io/flux/)

### How do I get set up? ###

* To setup the environment
*  Go to root folder
*  Type "npm install"
*  Type "bower install"
*  For running locally type "grunt serve"

### To build for production/staging server ###
* type "grunt build"
* Go to dist and copy contents for frontend


### Deployment instructions ###
*  Create a sails project 
*  Copy dist contents to assets folder in sails project
*  Then run sails