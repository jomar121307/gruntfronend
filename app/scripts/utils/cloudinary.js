"use strict";

import Request from "./request.js";
import async from "async";
import moment from "moment";
import superagent from "superagent";

function processFiles(file, callback) {
  const reader = new FileReader();

  reader.addEventListener("load", e => callback(null, e.target.result));
  reader.addEventListener("abort", e => callback("aborted"));
  reader.addEventListener("error", e => callback(e));
  reader.readAsDataURL(file);
}

function uploadVideo(file, progressCb, callback) {
  let user = null;

  try {
    user = JSON.parse(localStorage.user);
  } catch (e) {
    return cb("User info not found.");
  }

  const data = {
    format : file.type.slice(6),
    tags : user.id + "-" + "video",
    timestamp : moment().unix(),
    type : "upload"
  };

  async.waterfall([
    function (waterfallCb) {
      processFiles(file, function (err, fileData) {
        if (err) {
          return waterfallCb(err);
        }

        waterfallCb(null, fileData);
      });
    },
    function (fileData, waterfallCb) {
      Request.post("uploadaccess",
        data,
        function (err, access) {
          if (err) {
            return  waterfallCb(err);
          }

          waterfallCb(null, fileData, access);
        });
    },
    function (fileData, access, waterfallCb) {
      const payload = {
        file : fileData,
        ...data,
        api_key : access.api_key,
        signature : access.signature
      };

      superagent
        .post(config.cloudinaryUrl + "video/upload")
        .send(payload)
        .set("Accept", "application/json")
        .on("progress", (e) => progressCb(e.percent || 0))
        .end((err, res) => {
          if (err) {
            return waterfallCb(err);
          }

          waterfallCb(null, res.body);
        });
    }
  ], function (err, waterfallResult) {
    if (err) {
      return callback(err);
    }

    callback(null, waterfallResult);
  });
}

function uploadImage(file, progressCb, callback) {
  let user = null;

  try {
    user = JSON.parse(localStorage.user);
  } catch (e) {
    return cb("User info not found.");
  }

  const data = {
    format : file.type.slice(6),
    tags : user.id + "-" + "photo",
    timestamp : moment().unix(),
    type : "upload"
  };

  async.waterfall([
    function (waterfallCb) {
      processFiles(file, function (err, fileData) {
        if (err) {
          return waterfallCb(err);
        }

        waterfallCb(null, fileData);
      });
    },
    function (fileData, waterfallCb) {
      Request.post("uploadaccess",
        data,
        function (err, access) {
          if (err) {
            return  waterfallCb(err);
          }

          waterfallCb(null, fileData, access);
        });
    },
    function (fileData, access, waterfallCb) {
      const payload = {
        file : fileData,
        ...data,
        api_key : access.api_key,
        signature : access.signature
      };

      superagent
        .post(config.cloudinaryUrl + "image/upload")
        .send(payload)
        .set("Accept", "application/json")
        .on("progress", (e) => progressCb(e.percent || 0))
        .end((err, res) => {
          if (err) {
            return waterfallCb(err);
          }

          waterfallCb(null, res.body);
        });
    }
  ], function (err, waterfallResult) {
    if (err) {
      return callback(err);
    }

    callback(null, waterfallResult);
  });
}

const Cloudinary = {
  uploadVideo,
  uploadImage
};

export default Cloudinary;
