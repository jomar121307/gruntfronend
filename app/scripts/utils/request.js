/* global config */
"use strict";

var AppActions = require("../app/actions/AppActions.js");

var Request = {
  getToken : function() {
    return localStorage.token;
  },

  setToken : function(token) {
    localStorage.token = token;
  },

  getRefreshToken : function() {
    return localStorage.refreshToken;
  },

  setRefreshToken : function(refreshToken) {
    localStorage.refreshToken = refreshToken;
  },

  setUserInfo : function (user) {
    try {
      localStorage.user = JSON.stringify(user);
    } catch (e) {
      localStorage.user = null;
      console.error(e);
    }
  },

  getUserInfo : function () {
    let user = {};

    try {
      user = JSON.parse(localStorage.user);
    } catch (e) {
      return user;
    }

    return user;
  },

  request : function(type, model, data, token, cb) {
    if ($.isFunction(data)) {
      cb = data;
      data = null;
      token = null;
    } else if ($.isFunction(token)) {
      cb = token;
      token = null;
    } else if (!$.isFunction(cb)) {
      cb = function() {
      };
    }
    console.log("xxxxxxxxxxxxxxxxxxxxxx");
    console.log("type: "+type + " model: "+ model+" token: "+token);
    console.log(data);
    var params = {
      data : data,
      headers : {
        "X-App" : config.appType
      },
      xhrFields : null
    };

    if (!token) {
      token = this.getToken();
    }

    if (token) {
      params.headers = {
        "Authorization" : "Bearer " + token,
        "X-App" : config.appType
      };
      params.xhrFields = {
        withCredentials : true
      };
    }

    $.ajax({
      type : type,
      dataType: "json",
      url: [config.apiServer, model].join("/"),
      headers : params.headers,
      data : params.data,
      xhrFields : params.xhrFields,
      success: function(data) {
        setTimeout(function () {
          cb(false, data);
        }, config.isLocal ? 250 : 0);
      },
      error: function(xhr, status, err) {
        setTimeout(function () {
          cb({
            error : err,
            xhr : xhr,
            status : status
          });
        }, config.isLocal ? 250 : 0);
      }
    });
  },

  get : function(model, data, token, cb) {
    this.request("GET", model, data, token, cb);
  },

  post : function(model, data, token, cb) {
    this.request("POST", model, data, token, cb);
  },

  put : function(model, data, token, cb) {
    this.request("PUT", model, data, token, cb);
  },

  delete : function(model, data, token, cb) {
    this.request("DELETE", model, data, token, cb);
  }
};

module.exports = Request;

