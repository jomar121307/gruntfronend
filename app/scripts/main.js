"use strict";

/**
 * The application config object
 * @global
 */
import config from "./config.js";
import cloudinary from "cloudinary";
import _ from "underscore";
import lodash from "lodash";
import underscore_string  from "underscore.string";

_.mixin(require('safe-obj'));
_.mixin(underscore_string.exports());
_.mixin({merge : lodash.merge});
_.mixin({chunk : lodash.chunk});
_.mixin({cloneDeep : lodash.cloneDeep});

window._ = _;
window.config = config;
window.cloudinary = cloudinary;

toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-center",
  "preventDuplicates": false,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "10000",
  "extendedTimeOut": "1000",
}

cloudinary.config({
  cloud_name : config.cloudinaryCloudName,
  secure : true
});

var fbAppId = "695966983879936";

//
// Staging
//
if (location.host.indexOf("staging") === 0) {
  fbAppId = "555233081295851";
}

//
// Production
//
 else if (location.host.indexOf("www.") === 0) {
	fbAppId = "695966983879936";

//
// Development
//
} else if (location.host.indexOf("localhost") === 0) {
	fbAppId = "553921061427053";
}

// if (location.host.indexOf("staging") === 0) {
//   var s = document.createElement("script");
//   s.type = "text/javascript";
//   s.src = "http://jsconsole.com/remote.js?828CDE3F-5427-4934-91D4-FFEE03D4E93C";
//   $("body").append(s);
// }

window.fbAppId = fbAppId;

window.fbAsyncInit = function() {
  FB.init({
    appId : fbAppId,

    cookie : true,

    xfbml : true,

    version : "v2.4"
  });

  //
  // Wait until jquery becomes available
  //
  function setFbLoaded() {
    if(window.jQuery) {
      $("#fb-root").addClass("fb-sdk-loaded");
    } else {
      setTimeout(setFbLoaded, 1000);
    }
  }

  setTimeout(setFbLoaded, 1000);
};

// Load facebook SDK asynchronously
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

require("./routes.js")();
