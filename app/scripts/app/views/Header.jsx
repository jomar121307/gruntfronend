"use strict";

import React, {Component, PropTypes} from "react";
import shallowCompare from "react-addons-shallow-compare";
import {Link} from "react-router";
import ClassNames from "classnames";

import Utils from "../../utils/utils.js";

import RegisterModal from "../modules/misc/RegisterModal.jsx";
import LoginModal from "../modules/misc/LoginModal.jsx";
import LoginRegisterModal from "../modules/misc/LoginRegisterModal.jsx";

import AppActions from "../actions/AppActions.js";
import AppStores from "../stores/AppStores.js";

export default class Header extends Component {
  constructor (props) {
    super(props);

    this.renderDropDown = this.renderDropDown.bind(this);
    this.renderMessagesBtn = this.renderMessagesBtn.bind(this);
    this.renderLoginSignupBtns = this.renderLoginSignupBtns.bind(this);

    this.handleLogin = this.handleLogin.bind(this);
    this.handleRegister = this.handleRegister.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  render () {
    const {
        location,
        isLoggedIn,
        registrationError,
        registrationComplete,
        signupErrors,
        signupComplete
      } = this.props;
    const {router} = this.context;
    const isActive = location.pathname === "/";

    return (
      <div className="header-container" id="project-navbar-container">
        <nav className="navbar navbar-default">
          <div className="container">
            <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              <Link to="/" className="cd-logo navbar-brand">
                <img className="brand-logo" src="images/logo3.png"/>
              </Link>
            </div>
            <div className="collapse navbar-collapse">
              <ul className="nav navbar-nav navbar-right">
                {this.renderCartBtn()}
                <li className={ClassNames({active : isActive})}>
                  <Link to="/">
                    <span>Buy Products</span>
                  </Link>
                </li>
                {this.renderLoginSignupBtns()}
                {this.renderProfileBtn()}
                {this.renderMessagesBtn()}
                {this.renderDropDown()}
              </ul>
            </div>
            <LoginRegisterModal
              loginUser={this.handleLogin}
              registerUser={this.handleRegister}
              signupErrors={signupErrors}
              signupComplete={signupComplete}
              isLoggedIn={isLoggedIn} />
          </div>
        </nav>
      </div>
    )
  }

  renderCartBtn () {
    const {isLoggedIn, user, notifications, location} = this.props;
    const count = _.safe(notifications, "cart", 0);
    const isActive = location.pathname.indexOf("/cart") === 0;

    return (
      <li className={ClassNames({active : isActive})}>
        <Link to="/cart">
          <span>My Cart</span>
          <span className={ClassNames("text-danger bold badge m-l-xs", {hide : !notifications.cart})}>
            {(count > 99) ? "99+" : count}
          </span>
        </Link>
      </li>
    )
  }

  renderDropDown () {
    const {isLoggedIn, user} = this.props;

    if (!isLoggedIn) {
      return <li/>
    }

    return (
      <li className="dropdown">
        <a href="#"
          className="dropdown-toggle"
          data-toggle="dropdown"
          role="button"
          aria-haspopup="true"
          aria-expanded="false">
            <img className="current-user-img" src={_.safe(user, "profilePhoto.secure_url", "")} />
            <span className="navbar-user">{user.firstName} {user.lastName || ""}</span>
            <span className="caret"></span>
        </a>
        <ul className="dropdown-menu">
          <li>
            <Link to="/" onClick={this.handleLogout}>
              <span>Log Out</span>
            </Link>
          </li>
        </ul>
      </li>
    )
  }

  renderProfileBtn () {
    const {router} = this.context;
    const {isLoggedIn, notifications, location} = this.props;
    const count = _.safe(notifications, "shop", 0);
    const isActive = location.pathname.indexOf("/myshop") === 0;

    if (!isLoggedIn) {
      return <li/>
    }

    return (
      <li className={ClassNames({active : isActive})}>
        <Link to="/myshop">
          <span>My Shop</span>
          <span className={ClassNames("text-danger bold badge m-l-xs hide", {hide : !notifications.shop})}>
            {(count > 99) ? "99+" : count}
          </span>
        </Link>
      </li>
    )
  }

  renderMessagesBtn () {
    const {router} = this.context;
    const {isLoggedIn, notifications, location} = this.props;
    const count = _.safe(notifications, "messages", 0);
    const isActive = location.pathname.indexOf("/messages") === 0;

    if (!isLoggedIn) {
      return <li/>
    }

    return (
      <li className={ClassNames({active : isActive})}>
        <Link to="/messages">
          <span>Messages</span>
          <span className={ClassNames("text-danger bold badge m-l-xs", {hide : !notifications.messages})}>
            {(count > 99) ? "99+" : count}
          </span>
        </Link>
      </li>
    )
  }

  renderLoginSignupBtns () {
    const {isLoggedIn} = this.props;

    if (isLoggedIn) {
      return <li/>
    }

    return (
      <li className="pointer" onClick={this.showLoginModal}>
        <a>
          <span >Login / Signup</span>
        </a>
      </li>
    )
  }

  showLoginModal () {
    $("#LoginRegister-body").modal("show");
  }

  handleLogin (options) {
    const {location} = this.props;
    
    $("#LoginRegister-body2").modal("show");
    AppActions.login(options, () => {
      if (location.pathname === "/resetpassword") {
        this.context.router.replace("/");
      }


    });
  }

  handleRegister (options, callback) {
    AppActions.register(options, () => {
      // this.context.router.replace("/");
      $("#LoginRegister-body").modal("hide");
      callback();
    });
  }

  handleLogout () {
    AppActions.logout(() => this.context.router.replace("/"));
  }
}

Header.contextTypes = {
  router : PropTypes.object.isRequired
};
