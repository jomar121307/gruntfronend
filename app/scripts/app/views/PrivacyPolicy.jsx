"use strict";

//
// Dependencies
//
var React = require("react");

var PrivacyPolicy = React.createClass({
  render: function (){
    return(
      <div className="brand-container hideSidebar" >
        <div className="outer-container">
          <div className="row">
            <div className="col-sm-12">
              <div className="panel panel-default">
                  <div className="panel-heading"><h3>{config.appname} Privacy Policy</h3></div>
                  <div className="panel-body">
                  <p>Last updated: December 14, 2015</p>
                  <p>
                    This Privacy Policy governs the manner in which Bentanayan collects, uses, maintains and discloses information collected from users (each, a "User") of the www.{config.appname}.com website ("Site"). This privacy policy applies to the Site and all products and services offered by Bentanayan.
                  </p>
                  <h4>
                    Personal identification information
                  </h4>
                  <p>
                    We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the sitefill out a form and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.
                  </p>
                  <h4>
                    Non-personal identification information
                  </h4>
                  <p>
                    We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.
                  </p>
                  <h4>
                    Web browser cookies
                  </h4>
                  <p>
                    Our Site may use "cookies" to enhance User experience. {"User's"} web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.
                  </p>
                  <h4>How we use collected information</h4>
                  <ul>
                    <li>
                      Bentanayan collects and uses Users personal information for the following purposes:
                    </li>
                    <li>
                      To improve customer service
                    </li>
                    <li>
                      Your information helps us to more effectively respond to your customer service requests and support needs.
                    </li>
                    <li>
                      To improve our Site
                    </li>
                    <li>
                      We continually strive to improve our website offerings based on the information and feedback we receive from you.
                    </li>
                    <li>
                      To process transactions
                    </li>
                    <li>
                      We may use the information Users provide about themselves when placing an order only to provide service to that order. We do not share this information with outside parties except to the extent necessary to provide the service.
                    </li>
                    <li>
                      To administer a content, promotion, survey or other Site feature
                    </li>
                    <li>
                      To send Users information they agreed to receive about topics we think will be of interest to them.
                    </li>
                    <li>
                      To share your information with third parties
                    </li>
                    <li>
                      We may share or sell information with third parties for marketing or other purposes.
                    </li>
                    <li>
                      To send periodic emails
                    </li>
                    <li>
                      The email address Users provide for order processing, will only be used to send them information and updates pertaining to their order. It may also be used to respond to their inquiries, and/or other requests or questions. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Site.
                    </li>
                  </ul>

                  <h4>
                    How we protect your information
                  </h4>
                  <p>
                    We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.
                    Sensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel and is encrypted and protected with digital signatures.
                    Changes to this privacy policy
                  </p>

                  <p>
                    Bentanayan has the discretion to update this privacy policy at any time. When we do, revise the updated date at the bottom of this page,. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.
                    Your acceptance of these terms
                  </p>
                  <p>
                    By using this Site, you signify your acceptance of this policy and terms of service. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.
                  </p>

                  <h4>
                    Contacting us
                  </h4>

                  <p>
                    If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:
                    Bentanayan
                  </p>

                  <p>
                  www.{config.appname}.com
                  </p>
                  <p>
                    63 C T. Padilla Extension Cebu City
                  </p>
                  <p>
                    (410) 749-7075
                  </p>
                  <p>
                    <a href="mailto:info@bentanayan.com">info@ecommerce.tv</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = PrivacyPolicy;
