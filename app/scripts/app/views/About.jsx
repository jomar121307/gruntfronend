"use strict";

var React = require("react");
var Router = require("react-router");
var Navigation = Router.Navigation;
var Link = Router.Link;
var History = Router.History;
var Helmet = require("react-helmet");
var Utils = require("../../utils/utils.js");

var About = React.createClass({

  mixins : [Navigation],

  getInitialState: function() {
    return {
      loggingIn : false
    };
  },

  componentDidMount : function () {
    $('.slider-wrapper .slide:gt(0)').hide();
      setInterval(function () {
          $('.slide:first-child').fadeOut('slow')
                                 .next('.slide')
                                 .fadeIn('slow')
                                 .end()
                                 .appendTo('.slider-wrapper');
      }, 10000); // 10 seconds
  },

  render : function() {
    return (
      <div>
        <div id="about">
          <Helmet
              title={config.appname + " - About"}
              meta={[
                { property : "og:title", content : config.appname + " - About"},
                { property : "og:site_name", content : config.appname },
                ]} />
          <div className="row">
            <div className="container-fluid no-pad">
              <div className="fadein">
                <div className="slider-wrapper half-unfixed">
                  <div className="slide slide-1"></div>
                  <div className="slide slide-2"></div>
                  <div className="slide slide-3"></div>
                  <div className="slide slide-4"></div>
                  <div className="slide slide-5"></div>
                  <div className="slide slide-6"></div>
                </div>
              </div>
            </div>
          </div>
          <div className="container padding-content">
            <div className="row">
              <div className="col-sm-12">
                <p className="spn-header">What is {config.appname}</p>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12">
                <p className="spn-text">{config.appname} is a video-­‐centric e-­commerce platform for shoppers and those who wants to sell anything. It allows users to create their own shop as well as purchase items with ease.
                </p>
              </div>
            </div>
          </div>
        </div>
        <footer>
          <div className="row">
            <div className="col-sm-12">
              <p>
                <Link to="about" onClick={this._refreshFeed}>
                  <span className="about-spn-f">About {config.appname}</span>
                </Link>
                <span className="p-sd-s">•</span>
                <a href="#"><span className="about-spn-f">Terms of Service</span></a>
                <span className="p-sd-s">•</span>
                <a href="#"><span className="about-spn-f">Privacy Policy</span></a>
              </p>
              <span>&copy; 2015 {config.appname} All Rights Reserved.</span>
            </div>
          </div>
        </footer>
      </div>
    );
  },

  _prevPage : function () {
      // History.length > 1 ? this.goBack() : this.transitionTo("feed");
  }

});

module.exports = About;
