"use strict";

var React = require('react');
var Router = require("react-router");
var Navigation = Router.Navigation;
var Link = Router.Link;

var Footer = React.createClass({
    render : function () {
      return (
        <footer>
          <div className="container no-pad">
            <div className="row">
              <div className="col-sm-6 col-xs-12">
                <p>
                  <Link to="/about" onClick={this._refreshFeed}>
                    <span>About {config.appname}</span>
                  </Link>
                  <span className="p-sd-s">•</span>
                  <Link to="/termsandcondition">
                    <span>Terms of Service</span>
                  </Link>
                  <span className="p-sd-s">•</span>
                  <Link to="/privacypolicy"><span>Privacy Policy</span></Link>
                </p>
                <span className="cpy">&copy; 2016 {config.appname} v{config.version}-rc{config.rc} All Rights Reserved.</span>
              </div>
              <div className="col-sm-6  col-xs-12">
                <div className="pay-container">
                </div>
              </div>
            </div>
          </div>
        </footer>
      );
    }
});

module.exports = Footer;
