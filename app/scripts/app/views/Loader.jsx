"use strict";

import React from "react";
import {PulseLoader} from "halogen";

export default class Loader extends React.Component {
  render () {
    const {colStyle} = this.props;

    return (
      <div className="row text-center">
        <div className="col-xs-12" style={colStyle}>
          <PulseLoader color="#26A65B" size="16px" margin="4px"/>
        </div>
      </div>
    )
  }
}
