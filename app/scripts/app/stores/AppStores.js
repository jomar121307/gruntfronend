"use strict";

import {EventEmitter} from "events";
import async from "async";
import _ from "underscore";
import {slugify} from "underscore.string";

import Utils from "../../utils/utils.js";
import Request from "../../utils/request.js";
import Socket from "../../utils/socket.js";
import {register} from "../dispatcher/AppDispatcher.js";
import AppConstants from "../constants/AppConstants.js";
import Categories from "../constants/ProductCategories.js";
import Provinces from "../constants/PhilippineProvinces.js";

// const categories = _.map(Categories, function (elem) {
//   elem.slug = slugify(elem.name);
//   elem.subcategories = _.map(elem.subcategories, function (elem2) {
//     elem2.slug = slugify(elem2.name);
//     return elem2;
//   });
//   return elem;
// });

const initialState = {
  user : Request.getUserInfo(),
  initialized : false,
  signupErrors : {},
  isLoggedIn : !_.isEmpty(Request.getUserInfo()),
  categories : [],
  provinces : Provinces.map(p => p._id),
  notifications : {},
  paypalInfo : {
    isValid : false,
    isUpdated : false
  }
};

let state = $.extend(true, {}, initialState);

const AppStores = Object.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(AppConstants.APP_CHANGE_EVENT, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(AppConstants.APP_CHANGE_EVENT, callback);
  },

  _emitChange : function () {
    AppStores.emit(AppConstants.APP_CHANGE_EVENT);
  },

  getState : function () {
    return state;
  },

  getInitialState : function () {
    return initialState;
  }
});

const Actions = {
  [AppConstants.APP_INITIALIZE] : function (params) {
    appInitialize((err, data) => {
      if (err) {
        return appSetState(err, {});
      }

      appSetState(null, {
        initialized : true,
        categories : data[0].metadata
      });
    });
  },

  [AppConstants.APP_GET_NOTIFICATIONS] : function (params) {
    if (!state.isLoggedIn) {
      let items = [];

      try {
        items = JSON.parse(localStorage.__aitc || "[]");
      } catch (e) {
        return console.error(e);
      }

      if (items.length !== state.notifications.cart) {
        appSetState(null, {
          notifications : {
            cart : items.length
          }
        });
      }
      return;
    }

    appGetNotifications((err, data) => {
      if (err) {
        return appSetState(err, {});
      }

      appSetState(null, {
        notifications : {
          shop : data.shop,
          cart : data.cart,
          messages : data.messages
        }
      });
    });
  },

  [AppConstants.APP_REGISTER] : function (params) {
    appRegister(params, function (err, data) {
      if (err) {
        const invalidAttr = _.safe(err, "xhr.responseJSON.raw.invalidAttributes");
        const invalidAttr2 = _.safe(err, "xhr.responseJSON.invalidAttributes");

        let error = {};

        _.each(invalidAttr, (v, k) => {
          error[k] = _.safe(v[0], "message")
        });

        _.each(invalidAttr2, (v, k) => {
          error[k] = _.safe(v[0], "message")
        });

        return appSetState(err, {
          signupErrors : error
        });
      }

      appSetState(null, {
        signupErrors : {}
      });

      params.callback();
      toastr.success("Registration successful.");
    });
  },

  [AppConstants.APP_LOGIN] : function (params) {
    appLogin(params.options, function (err, data) {
    $("#LoginRegister-body2").modal("hide");
      if (err) {
        $("#LoginRegister-body").modal("show");
        return appSetState(err, {});
      }

      Request.setToken(data.session.accessToken);
      Request.setUserInfo(data.session.userId);
      appSetState(null, {
        isLoggedIn : true,
        user : data.session.userId
      });
      $("#LoginRegister-body").modal("hide");
      params.callback();
    });
  },

  [AppConstants.APP_LOGOUT] : function (params) {
    appSetState(null, {
      isLoggedIn : false
    });

    appLogout(function (err, data) {
      params.callback(true);

      localStorage.clear();

      if (err) {
        return console.error(err);
      }
    });
  },

  [AppConstants.APP_UPDATE_USER] : function (params) {
    appSetState(null, {
      user : params.user
    });
    Request.setUserInfo(params.user);
  },

  [AppConstants.APP_SET_PAYPAL_INFO_FLAG] : function (params) {
    appSetState(null, {
      paypalInfo : {
        isValid : params.hasInfo,
        isUpdated : true
      }
    });
  },

  [AppConstants.APP_SET_CATEGORIES] : function (params) {
    appSetState(null, {
      categories : params.categories
    });
  }
};

AppStores.dispatchIndex = register(payload => {
  const {type, params} = payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default AppStores;

/**
 * Sets the app state of this store.
 *
 * @method     appSetState
 * @param      {Object}  err     { description }
 * @param      {Object}  obj     { description }
 */
function appSetState(err, obj) {
  if (err) {
    if (typeof err === "string") {
      toastr.error(err);
    } else if (_.safe(err, "xhr.responseJSON.raw.invalidAttributes")) {
      const attrs = _.values(err.xhr.responseJSON.raw.invalidAttributes)[0];
      toastr.error(attrs[0].message);
    } else if (_.safe(err, "xhr.responseText")) {
      toastr.error(err.xhr.responseText);
    } else {
      toastr.error("Error appSetState");
    }

    console.error(err);
  }

  //ensure only valid states are written
  _.each(obj, (v, k) => {
    if (state.hasOwnProperty(k)) {
      state[k] = v;
    } else {
      console.warn("Invalid state for app: " + prop);
    }
  });

  AppStores._emitChange(AppConstants.APP_CHANGE_EVENT);
}

function appInitialize (callback) {
  Request.get("misc", {
    where : JSON.stringify({name : "Product Categories"})
  }, callback);
}

function appGetNotifications (callback) {
  Request.get("zcommercenotifs", callback);
}

function appRegister (options, callback) {
  Request.post("users", options.user, callback);
}

/**
 * Initialize user login through native, facebook or twitter.
 *
 * @method     appLogin
 * @param      {Object}    options   { description }
 * @param      {Function}  callback  { description }
 */
function appLogin (options, callback) {
  async.waterfall([
    //NATIVE
    function (waterfallCb) {
      if (options.type !== "native" && options.type !== "ups") {
        return async.nextTick(waterfallCb);
      }

      Request.post("sessions", {
        userEmail : options.userEmail,
        password : options.password,
        type : options.type
      }, function (err, session) {
        if (err) {
          return waterfallCb(err);
        }

        waterfallCb({
          type : "skip",
          data : session
        });
      });
    },
    //FACEBOOK
    function (waterfallCb) {
      if (options.type !== "facebook") {
        return async.nextTick(waterfallCb);
      }

      FB.login(function (response) {
        waterfallCb(null, response);
      }, {
        scope : "email",
        return_scopes : true
      });
    },
    function (fbResponse, waterfallCb) {
      if (_.isEmpty(fbResponse.authResponse)) {
        return waterfallCb("Error can't login to facebook.");
      }

      localStorage.__fbat = fbResponse.authResponse.accessToken;
      Request.post("sessions", {
        accessToken : _.safe(fbResponse, "authResponse.accessToken", ""),
          socialNetwork : "fb"
        }, function (err, session) {
          if (err) {
            return waterfallCb(err);
          }

          waterfallCb(null, session);
        });
    }
  ], function (err, waterfallResult) {
    if (err && err.type === "skip") {
      return callback(null, err.data);
    } else if (err) {
      return callback(err);
    }

    callback(null, waterfallResult);
  });
}

function appLogout (callback) {
  Request.get("logout", (err, res) => {
    FB.getLoginStatus(function(response) {
        if (response && response.status === 'connected' && !config.isLocal) {
          FB.logout(function(response) {
            callback();
          });
        } else {
          callback();
        }
    });
  });
}
