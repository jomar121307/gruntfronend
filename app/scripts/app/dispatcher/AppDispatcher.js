"use strict";

var Dispatcher = require("flux").Dispatcher;
var objectAssign = require("react/lib/Object.assign");

var AppConstants = require("../constants/AppConstants.js");

var AppDispatcher = objectAssign(new Dispatcher(), {

  /**
   * A bridge function between the views and the dispatcher, marking the action
   * as a server action.
   * @param  {object} action The data coming from the server.
   */
  handleServerAction : function(action) {
    var payload = {
      source : AppConstants.SERVER_ACTION,
      action : action
    };

    this.dispatch(payload);
  },

  /**
   * A bridge function between the views and the dispatcher, marking the action
   * as a view action.
   * @param  {object} action The data coming from the view.
   */
  handleViewAction : function(action) {
    var payload = {
      source : AppConstants.VIEW_ACTION,
      action : action
    };

    this.dispatch(payload);
  }
});

module.exports = AppDispatcher;
