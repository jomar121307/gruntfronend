"use strict";

import {EventEmitter} from "events";
import Request from "../../../utils/request.js";
import {register} from "../../dispatcher/AppDispatcher.js";
import {processState} from "../../../utils/utils.js";

import ResetPasswordConstants from "./ResetPasswordConstants.js";

const initialState = {
  initialized : false,
  renderView : "unsent",
  isRequesting : false,
  showResetFields : false,
  email : "",
  password : "",
  confirmPassword : "",
  error : {}
};

let state = $.extend(true, {}, initialState);

let Stores = Object.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(ResetPasswordConstants.RESETPASSWORD_CHANGE_EVENT, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(ResetPasswordConstants.RESETPASSWORD_CHANGE_EVENT, callback);
  },

  _emitChange : function () {
    Stores.emit(ResetPasswordConstants.RESETPASSWORD_CHANGE_EVENT);
  },

  getInitialState : function () {
    return initialState;
  },

  getState : function () {
    return state;
  }
});

const Actions = {
  [ResetPasswordConstants.RESETPASSWORD_RESET] : function (params) {
    setState(null, initialState);
  },

  [ResetPasswordConstants.RESETPASSWORD_RESET_PASSWORD] : function (params) {
    setState(null, {
      isRequesting : true
    });

    Request.post("users/resetpassword", {
      email : state.email
    }, (err, data) => {
      if (err) {
        return setState(err, {
          isRequesting : false
        });
      }

      setState(null, {
        renderView : "sent",
        isRequesting : false
      });
    });
  },

  [ResetPasswordConstants.RESETPASSWORD_SET_FORM] : function (params) {
    setState(null, {
      error : params.error,
      email : _.safe(params, "form.email"),
      password : _.safe(params, "form.password"),
      confirmPassword : _.safe(params, "form.confirmPassword")
    });
  },

  [ResetPasswordConstants.RESETPASSWORD_SHOW_RESET_FIELDS] : function (params) {
    setState(null, {
      renderView : "resetfields"
    });
  },

  [ResetPasswordConstants.RESETPASSWORD_SET_PASSWORD] : function (params) {
    Request.post("users/setpassword", {
      __p : state.password,
      token : params.rt
    }, (err, data) => {
      if (err) {
        return setState(err, {});
      } else if (data.status === "success") {
        return setState(null, {
          renderView : "resetsuccess",
          password : "",
          confirmPassword : ""
        });
      }
    });
  }
}

Stores.dispatchIndex = register(payload => {
  const {type, params} = payload.action;

  if (type && typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function setState(err, newState) {
  processState({err, newState, state, componentName : ""});
  Stores._emitChange(ResetPasswordConstants.RESETPASSWORD_CHANGE_EVENT);
}
