"use strict";

import React from "react";
import Helmet from "react-helmet";
import {Link} from "react-router";
import ClassNames from "classnames"

import Loader from  "../../views/Loader.jsx";
import ContactOwnerModal from './components/ContactOwner.jsx';
import ApproximateCost from "./components/ApproximateCost.jsx";

import CartActions from "./CartActions.js";
import CartStores from "./CartStores.js";

export default class Cart extends React.Component {
  constructor (props) {
    super (props);

    this.state = CartStores.getInitialState();

    this.renderEmptyCart = this.renderEmptyCart.bind(this);
    this.renderItemCountHeader = this.renderItemCountHeader.bind(this);
    this.listItemGroup = this.listItemGroup.bind(this);
    this.listItems = this.listItems.bind(this);
    this.onChange = this.onChange.bind(this);
    this.handleProceed = this.handleProceed.bind(this);
  }

  componentWillMount () {
    CartStores.addChangeListener(this.onChange);
    CartActions.initialize();
  }

  componentWillUpdate (nextProps, nextState) {
    if (nextState.proceedToShipping && !this.state.proceedToShipping) {
      this.context.router.push("/shipping");
    }
  }

  componentWillUnmount () {
    CartActions.reset();
    CartStores.removeChangeListener(this.onChange);
  }

  render() {
    const {groups} = this.state;
    const itemCount = _.reduce(groups, (acc, group) => {
      return acc + group.items.length;
    }, 0);

    return (
      <div className="container">
        <Helmet
            title={config.appname + " - Cart"}
            meta={[
              { property : "og:title", content : config.appname + " - Cart"},
              { property : "og:site_name", content : config.appname },
              ]} />
        <div id="cart-body">
          <div className="mycart-header-container">
            {
              (!this.state.initialized ? <Loader colStyle={{paddingTop : "50px"}}/> :
                (
                  (itemCount === 0) ? this.renderEmptyCart() : this.renderItemCountHeader(itemCount)
                )
              )
            }
          </div>
          {groups.map(this.listItemGroup)}
        </div>
        <ContactOwnerModal />
      </div>
    );
  }

  renderEmptyCart () {
    return (
      <div className="empty-cart-container text-center">
        <h2>
          <span className="glyphicon glyphicon-shopping-cart"></span>
        </h2>
        <h4>Oops! It seems you don{"'"}t have any item in your cart yet.</h4>
        <p>
          <Link to="/">
            <button className="btn btn-primary">go to shop</button>
          </Link>
        </p>
      </div>
    );
  }

  renderItemCountHeader (itemCount) {
    return (
      <h4 className="bold">{itemCount} {(itemCount === 1) ? "item" : "items"} in your cart</h4>
    );
  }

  onChange () {
    this.setState(CartStores.getState());
  }

  listItemGroup (group, index) {
    const {provinces, updatingItemIndex} = this.state;
    const {items, userId} = group;

    return (
      <div className="panel panel-default" key={index}>
        <div className="panel-heading">
          <h3 className="panel-title">Order from&nbsp;
            <Link to={"/shop/" + _.safe(userId, "id")}>
              {_.safe(userId, "firstName")} {_.safe(userId, "lastName", "")}
            </Link>
          </h3>
        </div>
        <div className="panel-body">
          <div className={ClassNames("loading-div", {"hide" : index !== updatingItemIndex})}>
            <Loader color="#26A65B" size="16px" margin="4px"/>
          </div>
          <div className="row">
            <div className="col-sm-9 cart-item-list-container">
              {items.map(this.listItems.bind(null, index))}
            </div>
            <div className="col-sm-3">
              <ApproximateCost items={items}
                userId={userId}
                provinces={provinces}
                handleProceed={this.handleProceed.bind(null, items[0].shopId.id)} />
            </div>
          </div>
        </div>
      </div>
    )
  }

  listItems (groupIndex, item, index) {
    return (
      <div key={item.id} className="cart-item-list">
        <div className="row">
          <div className="col-sm-4">
            <img src={_.safe(item, "productId.metadata.photos.1.secure_url", "images/login-product-1.jpg")}
              className="img-responsive"/>
          </div>
          <div className="col-sm-8">
            <Link to={`/product/${_.safe(item, "productId.id")}`}>
              <h4>{_.safe(item, "productId.name")}</h4>
            </Link>
            <div className="">
              <label htmlFor="product-quantity">Quantity:</label>
              <select className="form-control input-sm m-l-s quantity-select"
                value={item.quantity}
                onChange={this.handleQuantityChange.bind(null, groupIndex, index)}>
                {_.range(1,11).map(elem => <option key={elem} value={elem}>{elem}</option>)}
              </select>
            </div>
            <div><p>{_.safe(item, "productId.description", "No description available.")}</p></div>
            <h4 className="text-success">₱ {_.safe(item, "productId.price")}</h4>
            <button className="btn btn-xs btn-default hide" data-toggle="modal" data-target="#contactOwner">Contact Shop Owner</button>
            <button className="btn btn-xs btn-danger"
              onClick={this.handleRemove.bind(null, groupIndex, index)}>
                Remove
            </button>
          </div>
        </div>
      </div>
    )
  }

  handleQuantityChange (groupIndex, index, e) {
    CartActions.updateItem(groupIndex, index, e.target.value);
  }

  handleShippingLocationChange (index, e) {
    CartActions.changeItemShippingLocation(index, e.target.value);
  }

  handleRemove (groupIndex, index) {
    CartActions.removeItem(groupIndex, index)
  }

  handleProceed (userId, paymentPlatform) {
    if (this.context.isLoggedIn) {
      return CartActions.proceed({userId, paymentPlatform});
    }

    $("#LoginRegister-body").modal("show");
  }
}

Cart.contextTypes = {
  router : React.PropTypes.object.isRequired,
  isLoggedIn : React.PropTypes.bool.isRequired
};
