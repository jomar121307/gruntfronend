"use strict";

import {EventEmitter} from "events";
import _ from "underscore";
import safeObj from "safe-obj";
import async from "async";

import Request from "../../../utils/request.js";
import {register} from "../../dispatcher/AppDispatcher.js";

import CartConstants from "./CartConstants.js";
import AppStores from "../../stores/AppStores.js";

const initialState = {
  initialized : false,
  groups : [],
  itemsLocation : {},
  provinces : AppStores.getState().provinces,
  updatingItemIndex : -1,
  proceedToShipping : false
};

let state = _.assign({}, initialState);

let Stores = _.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(CartConstants.CART_CHANGE_EVENTS, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(CartConstants.CART_CHANGE_EVENTS, callback);
  },

  emitChange : function () {
    Stores.emit(CartConstants.CART_CHANGE_EVENTS);
  },

  getState : function () {
    return state;
  },

  getInitialState : function () {
    return initialState;
  }
});

const Actions = {
  [CartConstants.CART_INITIALIZE] : function (params) {
    cartInitialize(function (err, data) {
      if (err) {
        return cartSetState(err, {});
      }

      data.items = _.groupBy(data.items, function (item) {
        return item.productId.userId.id;
      })

      data.items = _.map(data.items, function (value, key) {
        return {
          userId : value[0].productId.userId,
          items : value
        }
      });

      cartSetState(null, {
        initialized : true,
        groups : data.items
      });
    });
  },

  [CartConstants.CART_RESET] : function (params) {
    cartSetState(null, initialState);
  },

  [CartConstants.CART_CHANGE_SHIPPING_LOCATION] : function (params) {
    const itemsLocation = _.assign({}, state.itemsLocation, {
      [params.index] : params.location
    });


    cartSetState(null, {
      itemsLocation
    });
  },

  [CartConstants.CART_UPDATE_QUANTITY] : function (params) {
    cartSetState(null, {
      updatingItemIndex : params.groupIndex
    });

    const id = _.safe(state, "groups." + params.groupIndex + ".items." + params.index + ".id");

    if (!id) {
      return cartSetState(null, {
        updatingItemIndex : -1
      });
    }

    params.id = id;
    cartUpdateQuantity(params, function (err, data) {
      if (err) {
        return cartSetState(err, {
          updatingItemIndex : -1
        });
      }

      let newState = _.assign({}, state);
      newState.groups[params.groupIndex].items[params.index].quantity = data.quantity;
      newState.updatingItemIndex = -1;
      cartSetState(null, newState);
    });
  },

  [CartConstants.CART_REMOVE_ITEM] : function (params) {
    cartSetState(null, {
      updatingItemIndex : params.groupIndex
    });

    const id = _.safe(state, "groups." + params.groupIndex + ".items." + params.index + ".id");

    if (!id) {
      return cartSetState(null, {
        updatingItemIndex : -1
      });
    }

    params.id = id;
    cartRemove(params, function (err, data) {
      if (err) {
        return cartSetState(err, {
          updatingItemIndex : -1
        });
      }

      let newState = _.assign({}, state);
      //Remove the product from the grouped items.
      newState.groups[params.groupIndex].items.splice(params.index, 1);
      //If length is zero, remove the group.
      if (!newState.groups[params.groupIndex].items.length) {
        newState.groups.splice(params.groupIndex, 1);
      }

      newState.updatingItemIndex = -1;
      cartSetState(null, newState);
    });
  },

  [CartConstants.CART_PROCEED] : function (params) {
    cartProceedShipping(params, function (err, data) {
      if (err) {
        return cartSetState(err, {});
      }

      cartSetState(null, {
        proceedToShipping : true
      });
    });
  }
};

Stores.dispatchIndex = register(payload => {
  const {type, params} = payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function cartSetState (err, newState) {
  if (err) {
    toastr.error(err.xhr.responseText);
    console.error(err);
  }

  for (var prop in newState) {
    if (state.hasOwnProperty(prop)) {
      state[prop] = newState[prop];
    } else {
      console.warn("Invalid state for cart: " + prop);
    }
  }

  Stores.emitChange(CartConstants.CART_CHANGE_EVENTS);
}

function cartInitialize (callback) {
  let items = [];

  try {
    items = JSON.parse(localStorage.__aitc || "[]");
  } catch (e) {
    console.error(e);
    return callback();
  }

  if (AppStores.getState().isLoggedIn) {
    async.each(items, function (item, cb) {
      Request.post("cart", {
        productId : item.__id,
        quantity : item.__c
      }, cb);
    }, (err, data) => {
      Request.get("cart", callback);
    });

    return localStorage.__aitc = "[]";
  }

  async.map(items, function (item, cb) {
    Request.get(`products/${item.__id}`, function (err, product) {
      if (err) {
        return cb(err);
      }

      let p = {
        id : item.__id,
        quantity : item.__c,
        shopId : _.assign({}, product.shopId),
        userId : _.assign({}, product.userId),
        productId : product
      };

      cb(null, p);
    });
  }, function (err, result) {
    if (err) {
      return callback(err);
    }

    callback(null, {
      items : result
    });
  });
}

function cartUpdateQuantity(options, callback) {
  Request.put(`cart/${options.id}`, {
    quantity : options.quantity
  }, function (err, data) {
    setTimeout(function () {
      callback(err, data);
    }, 300);
  });
}

function cartRemove (options, callback) {
  if (AppStores.getState().isLoggedIn) {
    Request.delete(`cart/${options.id}`, callback);
    return;
  }

  let items = [];
  let idx = -1;

  try {
    items = JSON.parse(localStorage.__aitc || "[]");
  } catch (e) {
    console.error(e);
    return callback();
  }

  idx = _.findIndex(items, {
    __id : options.id
  });

  if (idx !== -1) {
    items.splice(idx, 1);
    localStorage.__aitc = JSON.stringify(items);
  }

  setTimeout(callback, 300);
}

function cartProceedShipping(options, callback) {
  Request.post("setcheckoutinfo", {
    checkoutShopId : options.userId,
    paymentPlatform : options.paymentPlatform
  }, callback);
}
