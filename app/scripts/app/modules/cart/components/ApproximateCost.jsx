"use strict";

import React from "react";
import {Link} from "react-router";

export default class ApproximateCost extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      platform : _.safe(props, "userId.paymentPlatformsAvailable.0"),
      selected : "Philippines",
      index : 0
    };

    this.renderPaymentPlatforms = this.renderPaymentPlatforms.bind(this);
    this.renderShippingCost = this.renderShippingCost.bind(this);

    this.setPaymentPlatform = this.setPaymentPlatform.bind(this);
    this.handleShippingLocationChange = this.handleShippingLocationChange.bind(this);
    this.handleProceedToShipping = this.handleProceedToShipping.bind(this);
  }

  render () {
    const {items, handleProceed} = this.props;
    const {index, platform} = this.state;
    const subtotal = _.reduce(items, function (acc, item) {
      return acc + item.quantity * item.productId.price;
    }, 0);
    const shippingCost = _.reduce(items, function (acc, item) {
      return acc + parseInt(_.safe(item, "productId.metadata.shippingData." + index + ".singleCost", 0));
    }, 0);

    return (
      <div className="compute-cost-container">
        <p className="m-t-s">Ready to ship in 1-2 business days</p>
        <hr className="m-t-s m-b-s"/>
        {this.renderPaymentPlatforms()}
        <div className="row">
          <div className="col-xs-6">
            Item total
          </div>
          <div className="col-xs-6 text-right">
            ₱ {subtotal}
          </div>
        </div>
        <hr className="m-t-s m-b-s"/>
        {this.renderShippingCost(shippingCost)}
        <hr className="m-t-s m-b-s"/>
        <div className="row">
          <div className="col-xs-6">
            <strong>Total</strong>
          </div>
          <div className="col-xs-6 text-right">
            <strong>₱ {subtotal + shippingCost}</strong>
          </div>
        </div>
        <hr className="m-t-s m-b-s"/>
        <div className="row">
          <div className="col-xs-12">
            <button className="btn btn-primary btn-block" onClick={this.handleProceedToShipping}>proceed to checkout</button>
          </div>
        </div>
      </div>
    )
  }

  renderPaymentPlatforms () {
    const {platform} = this.state;
    const {userId} = this.props;

    return _.safe(userId, "paymentPlatformsAvailable", [])
      .map((p, idx, arr) => {

        switch (p) {
          case "paypal":
            return <noscript/>
            return (
              <div>
                <div className="row" onClick={this.setPaymentPlatform.bind(null, "paypal")}>
                  <div className="col-xs-4">
                    <input type="radio"
                      className="paypal-radio-btn"
                      checked={platform === "paypal" || arr.length === 1}
                      onClick={this.setPaymentPlatform.bind(null, "paypal")}/>
                  </div>
                  <div className="col-xs-8">
                    <label htmlFor="pay-paypal">
                      <img src="images/pay-thru.png" className="pay-thru"/>
                    </label>
                  </div>
                </div>
                <hr className="m-t-s m-b-s"/>
              </div>
            )
          case "ecash":
            return (
              <div>
                <div className="row" onClick={this.setPaymentPlatform.bind(null, "ecash")}>
                  <div className="col-xs-4">
                    <input type="radio"
                      checked={platform === "ecash" || arr.length === 1}
                      onClick={this.setPaymentPlatform.bind(null, "ecash")}/>
                  </div>
                  <div className="col-xs-8">
                    <label htmlFor="pay-ecash"><p className="no-margin">Pay with E-cash</p></label>
                  </div>
                </div>
                <hr className="m-t-s m-b-s"/>
              </div>
            )
        }
      });

  }

  renderShippingCost (shippingCost) {
    const {selected} = this.state;
    const {provinces} = this.props;

    if (!shippingCost) {
      return (
        <div className="row">
          <div className="col-xs-12 text-right">
            Free Shipping!
          </div>
        </div>
      );
    }

    return (
      <div>
        <div className="row">
          <div className="col-xs-8">
            Shipping fee
            <select value={selected}
              className="form-control input-sm"
              onChange={this.handleShippingLocationChange}>
                <option key="Philippines" value="Philippines">Philippines</option>
                {provinces.map(p => <option key={p} value={p}>{p}</option>)}
            </select>
          </div>
          <div className="col-xs-4 text-right">
            ₱ {shippingCost}
          </div>
        </div>
        <div className="row">
          <div className="col-xs-6">
          </div>
          <div className="col-xs-6 text-right">
          </div>
        </div>
      </div>
    )
  }

  setPaymentPlatform (platform, e) {
    this.setState({
      platform
    });
  }

  handleShippingLocationChange (e) {
    const {items} = this.props;

    let idx = _.findIndex(_.safe(items[0], "productId.metadata.shippingData", []), {
      location : e.target.value
    });

    if (idx === -1) {
      idx = 0;
    }

    this.setState({
      selected : e.target.value,
      index : idx
    });
  }

  handleProceedToShipping (e) {
    e.preventDefault();

    const {platform} = this.state;
    const {handleProceed} = this.props;

    if (!platform) {
      return toastr.error("Select a payment platform to proceed.");
    }

    handleProceed(platform);
  }
}

ApproximateCost.propTypes = {
  items : React.PropTypes.array.isRequired,
  userId  : React.PropTypes.object.isRequired,
  provinces : React.PropTypes.array.isRequired,
  handleProceed : React.PropTypes.func.isRequired
};
