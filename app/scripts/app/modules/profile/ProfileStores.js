"use strict";

import async from "async";
import {EventEmitter} from "events";
import superagent from "superagent";

import Utils from "../../../utils/utils.js";
import Request from "../../../utils/request.js";
import Cloudinary from "../../../utils/cloudinary.js";
import {register} from "../../dispatcher/AppDispatcher.js";

import AppActions from "../../actions/AppActions.js";
import AppStores from "../../stores/AppStores.js";

import ProfileConstants from "./ProfileConstants.js";

const initialState = {
  user : {},
  userInfoError : {},

  products : [],
  productCount : 0,

  profileVideoMetadata : {
    isUploading : false,
    progress : 0
  },

  coverVideoMetadata : {
    isUploading : false,
    progress : 0
  },

  youtubeMetadata : {
    state : "none",
    type : "",
    videoId : "",
    doneUploading  : false
  },

  initialized : false,
  initializeError : {},
  isSearching : false,
  isLoadingMore : false,

  productPagination : {
    page : 0,
    size : 12
  },

  currentTab : "products",
  hasPaypalInfo : AppStores.getState().hasPaypalInfo
};

let state = $.extend(true, {}, initialState);

let Stores = Object.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(ProfileConstants.PROFILE_CHANGE_EVENT, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(ProfileConstants.PROFILE_CHANGE_EVENT, callback);
  },

  _emitChange : function () {
    Stores.emit(ProfileConstants.PROFILE_CHANGE_EVENT);
  },

  getInitialState : function () {
    return initialState;
  },

  getState : function () {
    return state;
  }
});

const Actions = {
  [ProfileConstants.PROFILE_INITIALIZE] : function (params) {
    profileInitialize(params, function (err, data) {
      if (err) {
        return profileSetState(err, {
          initialized : true,
          initializeError : "" //@TODOS
        });
      }

      profileSetState(null, {
        user : data.user,
        products : data.products.products,
        productCount : data.products.count,
        initialized : true,
        initializeError : {}
      });
    });
  },

  [ProfileConstants.PROFILE_RESET] : function (params) {
    profileSetState(null, initialState);
  },

  [ProfileConstants.PROFILE_LOAD_MORE] : function (params) {
    if (state.productCount <= state.products.length) {
      return;
    }

    const newState = _.assign({}, state);
    ++newState.productPagination.page;
    newState.isLoadingMore = true;
    profileSetState(null, newState);

    profileLoadMore(function (err, data) {
      if (err) {
        return profileSetState(err, {
          isLoadingMore : false
        });
      }

      profileSetState(null, {
        products : state.products.concat(data.products),
        productCount : data.count,
        isLoadingMore : false
      });
    });
  },

  [ProfileConstants.PROFILE_RESET_PAGINATION] : function (params) {
    const newState = _.assign({}, state);
    newState.productPagination = initialState.productPagination;
    profileSetState(null, newState);
  },

  [ProfileConstants.PROFILE_UPDATE] : function (params) {
    const __oldUser = _.assign({}, state);
    const newState = _.merge({}, state, {
      user : params.user
    });

    if (_.isEqual(state, newState)) return;

    profileSetState(null, newState);

    profileUpdate(newState.user, function (err, data) {
      if (err) {
        const newState = _.merge({}, state, {
          user : __oldUser
        });

        return profileSetState(null, newState);
      }

      let newState = _.merge({}, state, {
        youtubeMetadata : {
          doneUploading : true
        }
      });

      profileSetState(null, newState);

      newState = _.merge(null, state, {
        youtubeMetadata : {
          state : "none",
          type : "",
          videoId : "",
          doneUploading  : false
        }
      });

      profileSetState(null, newState);
      AppActions.updateUser(newState.user);
    });
  },

  [ProfileConstants.PROFILE_UPLOAD_PROFILE_VIDEO] : function (params) {
    const newState = _.extend({}, state, {
      profileVideoMetadata : {
        isUploading : true
      }
    });

    profileSetState(null, newState);

    profileUploadVideo({
      file : params.file,
      mediaType : "profileVideo",
      progressCb : function (percent) {
        const newState  = _.merge({}, state, {
          profileVideoMetadata : {
            progress : percent
          }
        });

        profileSetState(null, newState);
      }
    }, function (err, data) {
      if (err) {
        const newState = _.merge({}, state, {
          profileVideoMetadata : {
            progress : 0,
            isUploading : false
          }
        });

        return profileSetState(err, newState);
      }

      //@TODOS improve implementation. para ni sa on-off sa modal :(

      let newState = _.merge({}, state, {
        profileVideoMetadata : {
          progress : 100,
          isUploading : true
        }
      });

      profileSetState(null, newState);

      newState = _.merge({}, state, {
        profileVideoMetadata : {
          progress : 0,
          isUploading : false
        },
        user : data
      });

      profileSetState(null, newState);
    });
  },

  [ProfileConstants.PROFILE_UPLOAD_COVER_VIDEO] : function (params) {
    const newState = _.merge({}, state, {
      coverVideoMetadata : {
        isUploading : true
      }
    });

    profileSetState(null, newState);
    profileUploadVideo({
      file : params.file,
      mediaType : "coverVideo",
      progressCb : function (percent) {
        const newState = _.merge({}, state, {
          coverVideoMetadata : {
            progress : percent
          }
        });

        profileSetState(null, newState);
      }
    }, function (err, data) {
      if (err) {
        const newState = _.merge({}, state, {
          coverVideoMetadata : {
            progress : 0,
            isUploading : false
          }
        });

        return profileSetState(err, newState);
      }

      let newState = _.merge({}, state, {
        coverVideoMetadata : {
          progress : 100,
          isUploading : true
        }
      });

      profileSetState(null, newState);

      newState = _.merge({}, state, {
        coverVideoMetadata : {
          progress : 0,
          isUploading :false
        },
        user : data
      });

      profileSetState(null, newState);
    });
  },

  [ProfileConstants.PROFILE_CHECK_YOUTUBE_LINK] : function (params) {
    const videoId = Utils.youtubeExtractVideoId(params.url);

    if (!params.url) {
      const newState = _.merge({}, state, {
        youtubeMetadata : {
          state : "none",
          videoId : ""
        }
      });

      return profileSetState(null, newState);
    } else if (!videoId) {
      const newState = _.merge({}, state, {
        youtubeMetadata : {
          state : "invalid",
          videoId : ""
        }
      });

      return profileSetState(null, newState);
    }

    const newState = _.merge({}, state, {
      youtubeMetadata : {
        state : "fetching",
        type : params.type,
        videoId : ""
      }
    });

    profileSetState(null, newState);
    profilecheckYoutubeLink(videoId, function (err, data) {
      if (err) {
        const newState = _.merge({}, state, {
          youtubeMetadata : {
            state : "invalid",
            videoId : ""
          }
        });

        return profileSetState(null, newState);
      }

      const newState = _.merge({}, state, {
        youtubeMetadata : {
          state : "valid",
          videoId : videoId,
          metadata : data.body
        }
      });

      profileSetState(null, newState);
    });
  },

  [ProfileConstants.PROFILE_MESSAGE_USER] : function (params) {
    profileMessageUser(params, function (err, data) {
      if (err) {
        return profileSetState(err, {});
      }

      toastr.success("Message sent.");
    });
  }
}

Stores.dispatchIndex = register(payload => {
  const {type, params} =  payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function profileSetState (err, obj) {
  if (err) {
    toastr.error(_.safe(err, "xhr.responseText", err));
    console.error(err);
  }

  for (var prop in obj) {
    if (state.hasOwnProperty(prop)) {
      state[prop] = obj[prop];
    } else {
      console.warn("Invalid state for profile state: " + prop);
    }
  }

  Stores._emitChange(ProfileConstants.PROFILE_CHANGE_EVENT);
}

/**
 * Fetches the user information and store products.
 *
 * @method     profileInitialize
 * @param      {Function}  callback  { description }
 */
function profileInitialize (params, callback) {
  const {productPagination, purchasePagination} = state;
  let url = "";

  if (_.safe(params, "options.shopId")) {
    url = `users/${_.safe(params, "options.shopId")}`;
  } else {
    url = "me";
  }

  Request.get(url, function (err, user) {
    if (err) {
      return callback(err);
    }

    async.parallel({
      products : function (parallelCb) {
        const query = {
          skip : productPagination.size * productPagination.page,
          limit : productPagination.size,
          where : JSON.stringify({
            userId : user.id
          })
        };

        Request.get("products", query, function (err, products) {
          if (err) {
            return parallelCb(err);
          }

          parallelCb(null, products);
        });
      },
    }, function (err, result) {
      if (err) {
        return callback(err);
      }
      result.user = user;
      callback(null, result);
    });
  });
}

/**
 * Load moar products.
 *
 * @method     profileLoadMore
 * @param      {Function}  callback  { description }
 */
function profileLoadMore (callback) {
  const {productPagination} = state;
  const query = {
    skip : productPagination.size * productPagination.page,
    limit : productPagination.size,
    where : JSON.stringify({
      userId : state.user.id
    })
  };

  Request.get("products", query, function (err, products) {
    if (err) {
      return callback(err);
    }

    callback(null, products);
  });
}

/**
 * Update yer profile.
 *
 * @method     profileUpdate
 * @param      {object}    user      { description }
 * @param      {Function}  callback  { description }
 */
function profileUpdate(user, callback) {
  Request.put(`users/${user.id}`, user, callback);
}

function profileUploadVideo (options, callback) {
  Cloudinary.uploadVideo(options.file,
    percent => options.progressCb(percent * .9),
    (err, data) => {
      if (err) {
        return callback(err);
      }

      let newUser = _.assign({}, state.user);
      _.ensure(newUser, "metadata", {});
      newUser.metadata[options.mediaType] = data;

      options.progressCb(95);
      Request.put(`users/${newUser.id}`, newUser, (err, data) => callback(err, data));
    });
}

function profilecheckYoutubeLink (videoId, callback) {
  const payload = {
    id : videoId,
    key : config.youtubeKey,
    part : "snippet"
  };

  superagent
    .get("https://www.googleapis.com/youtube/v3/videos")
    .query(payload)
    .end(callback)
}

function profileMessageUser (options, callback) {
  Request.post(`messages/initiate/${_.safe(state, "user.id")}`, {
    payload : {
      message : options.message
    }
  }, callback)
}
