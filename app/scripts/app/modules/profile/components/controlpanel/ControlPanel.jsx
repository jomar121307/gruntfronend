"use strict";

import React from "react";
import ClassNames from "classnames";

import Utils from "../../../../../utils/utils.js";
import Loader from "../../../../views/Loader.jsx";

import ControlPanelActions from "./ControlPanelActions.js";
import ControlPanelStores from "./ControlPanelStores.js";

export default class ControlPanel extends React.Component {
  constructor (props) {
    super(props);
    this.state = ControlPanelStores.getInitialState();

    this.onChange = this.onChange.bind(this);
    this.renderSubcategories = this.renderSubcategories.bind(this);
    this.listCategories = this.listCategories.bind(this);
    this.listSubCategories = this.listSubCategories.bind(this);
    this.handleHoverCategory = this.handleHoverCategory.bind(this);
    this.handleAddCategory = this.handleAddCategory.bind(this);
    this.handleAddSubcatGroup = this.handleAddSubcatGroup.bind(this);
    this.handleAddGroupling = this.handleAddGroupling.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.uploadThumbnail = this.uploadThumbnail.bind(this);
  }

  componentWillMount () {
    ControlPanelStores.addChangeListener(this.onChange)
    ControlPanelActions.setCategories(this.context.categories);
  }

  componentWillUpdate (nextProps, nextState, nextContext) {
    if (!Utils.deepCompare(this.context.categories, nextContext.categories)) {
      ControlPanelActions.setCategories(nextContext.categories);
    } else if (this.state.isSaving && !nextState.isSaving) {
      $("#addcategoriesmodal").modal("hide");
    }
  }

  componentWillUnmount () {
    clearTimeout(this.initializeTimeout);
    ControlPanelStores.removeChangeListener(this.onChange);
  }

  render () {
    const {categories, addModalMessage} = this.state;

    if (!categories.length) {
      return <Loader colStyle={{paddingTop : "50px"}}/>
    }

    return (
      <div id="controlpanel-container">
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">
              Product Categories <span className="text-danger admin-mode">(ADMIN MODE)</span>
            </h3>
          </div>
          <div className="panel-body">
            <div className="categories-container">
              <h4>
                Categories
                <span className="text-primary m-l-s add-category-hollow-btn" onClick={this.handleAddCategory}>Add Product</span>
              </h4>
              <div className="row">
                {categories.map(this.listCategories)}
              </div>
              <hr/>
            </div>
            {this.renderSubcategories()}
          </div>
        </div>
        <div className="modal fade bs-example-modal-sm"
          id="addcategoriesmodal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="categories-modal-title">
            <div className="modal-dialog modal-sm">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 className="modal-title" id="categories-modal-title">{addModalMessage}</h4>
                </div>
                <div className="modal-body">
                  <div className="row">
                    <div className="col-xs-12 hide">
                      <div className="category-thumbnail-container" onClick={this.uploadThumbnail}>
                        <h3 className="text-center">Add Image</h3>
                      </div>
                      <input className="form-control" id="category-thumbnail-file-btn" ref="add-modal-input-thumbnail" type="file"/>
                    </div>
                    <div className="col-xs-12">
                      <input className="form-control" ref="add-modal-input" placeholder={addModalMessage}/>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-primary" onClick={this.handleSave}>Add</button>
                </div>
              </div>
            </div>
        </div>
      </div>
    )
  }

  uploadThumbnail (e){
    e.preventDefault();
    document.getElementById("category-thumbnail-file-btn").click();
  }

  onChange () {
    this.setState(ControlPanelStores.getState());
  }

  renderSubcategories () {
    const {categories, selectedCategory} = this.state;
    const idx = _.findIndex(categories, {
      slug : selectedCategory
    });
    let chunkedSubcategories = null;

    if (!categories.length) {
      return <div/>
    } else if (idx === -1) {
      return <div/>
    }

    chunkedSubcategories = _.chunk(categories[idx].subcategories, 4);

    return (
      <div className="subcategories-container">
        <h4>
          Subcategories
          <span className="text-primary add-category-hollow-btn m-l-s" onClick={this.handleAddSubcatGroup}>Add Product</span>
        </h4>
        {chunkedSubcategories.map(chunk => {
          return (
            <div className="row" key={_.random(0, 9999)}>
              {chunk.map(this.listSubCategories)}
            </div>
          )
        })}
      </div>
    )
  }

  listCategories (category) {
    const {selectedCategory, hoveredCategory} = this.state;

    return (
      <div key={category.slug + Math.random()}
        className={ClassNames("col-xs-4 pointer ",
        {"active" : selectedCategory === category.slug})}
        onMouseEnter={this.handleHoverCategory.bind(null, "", category.slug)}>
          <span onClick={this.handleSelectCategory.bind(null, category.slug)}>
            {category.name}
          </span>
          <span className={ClassNames("text-danger pull-right p-l-s",
            {hide : hoveredCategory !== category.slug})}
            onClick={this.handleDeleteCategory.bind(null, "", category.slug)}>
              Delete
          </span>
      </div>
    )
  }

  listSubCategories (subcategory, index) {
    const {selectedCategory, hoveredCategory} = this.state;
    const path = `${selectedCategory}`;

    _.ensure(subcategory, "groupling", []);
    return (
      <div className="col-xs-3" key={subcategory.groupheader.slug + Math.random()}>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>
                <h5 className="bold"
                  onMouseEnter={this.handleHoverCategory.bind(null, path, subcategory.groupheader.slug)}>
                    <span>
                      {subcategory.groupheader.name}
                    </span>
                    <span className={ClassNames("text-danger pull-right p-l-s pointer",
                      {hide : hoveredCategory !== `${path}.${subcategory.groupheader.slug}`})}
                      onClick={this.handleDeleteCategory.bind(null, path, subcategory.groupheader.slug)}>
                        Delete
                    </span>
                </h5>
              </th>
            </tr>
          </thead>
          <tbody>
            {subcategory.groupling.map(g => {
              const path = `${selectedCategory}.${subcategory.groupheader.slug}`;
              return (
                <tr className="" key={g.slug + Math.random()}>
                  <td onMouseEnter={this.handleHoverCategory.bind(null, path, g.slug)}>
                    <span>
                      {g.name}
                    </span>
                    <span>
                      <span className={ClassNames("text-danger pull-right p-l-s pointer",
                        {hide : hoveredCategory !== `${path}.${g.slug}`})}
                        onClick={this.handleDeleteCategory.bind(null, path, g.slug)}>
                          Delete
                      </span>
                    </span>
                  </td>
                </tr>
              )
            })}
            <tr>
              <td className="pointer p-t-s m-b-s"
                onClick={this.handleAddGroupling.bind(null, subcategory.groupheader.slug)}>
                <span className="text-primary">Add More</span>
                <i className="text-primary fa fa-plus m-l-xs"></i>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }

  handleSelectCategory (cat, e) {
    ControlPanelActions.selectCategory(cat);
  }

  handleHoverCategory (path, cat, e) {
    const state = this.state;
    if (path) {
      state.hoveredCategory = `${path}.${cat}`;
    } else {
      state.hoveredCategory = cat;
    }
    this.setState(state);
  }

  handleAddCategory (e) {
    ControlPanelActions.showAddModal("category");
    this.refs["add-modal-input"].value = "";
    $("#addcategoriesmodal").modal("show");
  }

  handleDeleteCategory (ancestors, cat, e) {
    if (
          bootbox.dialog({
          size : 'small',
          message : "Are you sure you want to delete?",
          className: "deleteCategoryClass",
          buttons: {
                      buttonName : {
                        label : "Delete",
                        className : "btn-danger",
                        callback: function() {
                          if (ancestors) {
                            ControlPanelActions.deleteCategory(`${ancestors}.${cat}`)
                          } else if (!ancestors) {
                            ControlPanelActions.deleteCategory(`${cat}`);
                          }
                        }
                      }
                    }
          })
        ) {
      return;
    }
  }

  handleAddSubcatGroup (e) {
    ControlPanelActions.showAddModal("subcatgroup");
    this.refs["add-modal-input"].value = "";
    $("#addcategoriesmodal").modal("show");
  }

  handleAddGroupling (subcatgroup, e) {
    ControlPanelActions.showAddModal("subcat", subcatgroup);
    this.refs["add-modal-input"].value = "";
    $("#addcategoriesmodal").modal("show");
  }

  handleSave (e) {
    const value = this.refs["add-modal-input"].value.trim();

    if (!value) {
      return;
    }

    ControlPanelActions.saveCategory(value);
  }
}

ControlPanel.contextTypes = {
 categories : React.PropTypes.array.isRequired
}
