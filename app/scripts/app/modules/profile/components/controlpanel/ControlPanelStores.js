"use strict";
import _ from "underscore";
import {EventEmitter} from "events";
import Request from "../../../../../utils/request.js";
import {register} from "../../../../dispatcher/AppDispatcher.js";

import ControlPanelConstants from "./ControlPanelConstants.js";

import AppActions from "../../../../actions/AppActions.js";

const initialState = {
  categories : [],
  selectedCategory : "",
  addModalMessage : "",
  addPath : "",
  isSaving : false,
};

let state = $.extend(true, {}, initialState);

let Stores = _.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(ControlPanelConstants.CONTROLPANEL_CHANGE_EVENT, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(ControlPanelConstants.CONTROLPANEL_CHANGE_EVENT, callback);
  },

  emitChange : function () {
    Stores.emit(ControlPanelConstants.CONTROLPANEL_CHANGE_EVENT);
  },

  getInitialState : function () {
    return initialState;
  },

  getState : function () {
    return state;
  }
});

const Actions = {
  [ControlPanelConstants.CONTROLPANEL_SET_CATEGORIES] : function (params) {
    if (!params.categories.length) {
      return;
    }

    controlPanelSetState(null, {
      categories : params.categories,
      selectedCategory : params.categories[0].slug
    });
  },

  [ControlPanelConstants.CONTROLPANEL_SELECT_CATEGORY] : function (params) {
    controlPanelSetState(null, {
      selectedCategory : params.category
    });
  },

  [ControlPanelConstants.CONTROLPANEL_SHOW_MODAL] : function (params) {
    if (params.type === "category") {
      controlPanelSetState(null, {
        addModalMessage : "Add Category",
        addPath : ""
      });
    } else if (params.type === "subcatgroup") {
      controlPanelSetState(null, {
        addModalMessage : "Add Subcategory group",
        addPath : state.selectedCategory
      });
    } else if (params.type === "subcat") {
      controlPanelSetState(null, {
        addModalMessage : "Add Subcategory",
        addPath : `${state.selectedCategory}.${params.group}`
      });
    } else {
      controlPanelSetState(null, {
        addModalMessage : ""
      });
    }
  },

  [ControlPanelConstants.CONTROLPANEL_SAVE_CATEGORY] : function (params) {
    controlPanelSetState(null, {
      isSaving : true
    });

    controlPanelAddCategory(params, function (err, data) {
      if (err) {
        return controlPanelSetState(err, {});
      }

      controlPanelSetState(null, {
        isSaving : false,
        categories : data.metadata
      });
    });
  },

  [ControlPanelConstants.CONTROLPANEL_DELETE_CATEGORY] : function (params) {
    controlPanelDeleteCategory(params, function (err, data) {
      if (err) {
        return controlPanelSetState(err, {});
      }

      controlPanelSetState(null, {
        categories : data.metadata
      });
    });
  }
};

Stores.dispatchIndex = register(payload => {
  const {type, params} = payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function controlPanelSetState (err, obj) {
  if (err) {
    toastr.error(_.safe(err, "xhr.responseText", err));
    console.error(err);
  }

  for (let prop in obj) {
    if (state.hasOwnProperty(prop)) {
      state[prop] = obj[prop];
    } else {
      console.warn("Invalid state for control panel state: " + prop);
    }
  }

  Stores.emitChange(ControlPanelConstants.CONTROLPANEL_CHANGE_EVENT);
}

function controlPanelAddCategory (params, callback) {
  Request.post("misc/addcategories", {
    path : state.addPath,
    category : params.value
  }, callback);
}

function controlPanelDeleteCategory (params, callback) {
  Request.post("misc/deletecategories", {
    path : params.path
  }, callback);
}
