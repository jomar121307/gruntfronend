"use strict";

import _ from "underscore";

import AppDispatcher from "../../../../dispatcher/AppDispatcher.js";
import ControlPanelConstants from "./ControlPanelConstants.js";

const ControlPanelActions = {
  setCategories : function (categories) {
    AppDispatcher.handleViewAction({
      type : ControlPanelConstants.CONTROLPANEL_SET_CATEGORIES,
      params : {
        categories
      }
    });
  },

  selectCategory : function (category) {
    AppDispatcher.handleViewAction({
      type : ControlPanelConstants.CONTROLPANEL_SELECT_CATEGORY,
      params : {
        category
      }
    });
  },

  showAddModal : function (type, group) {
    AppDispatcher.handleViewAction({
      type : ControlPanelConstants.CONTROLPANEL_SHOW_MODAL,
      params : {
        type,
        group
      }
    });
  },

  saveCategory : _.throttle(function (value) {
    AppDispatcher.handleViewAction({
      type : ControlPanelConstants.CONTROLPANEL_SAVE_CATEGORY,
      params : {
        value
      }
    });
  }, 100, {trailing : false}),

  deleteCategory : _.throttle(function (path) {
    AppDispatcher.handleViewAction({
      type : ControlPanelConstants.CONTROLPANEL_DELETE_CATEGORY,
      params : {
        path
      }
    });
  }, 1000, {trailing : false})
};

export default ControlPanelActions;
