"use strict";

import React from "react";
import {render} from "react-dom";

import VideoUploadModal from "./VideoUploadModal.jsx";

import ProfileActions from "../ProfileActions.js";

export default class Cover extends React.Component {
  constructor (props) {
    super(props);

    this.renderVideoByType = this.renderVideoByType.bind(this);
  }

  componentWillUpdate (nextProps, nextState) {
    if (_.safe(nextProps, "coverVideoMetadata.progress") === 100 &&
      _.safe(nextProps, "coverVideoMetadata.isUploading")) {
      $("#CoverVideoModal").modal("hide");
    } else if (_.safe(nextProps, "youtubeMetadata.type") === "cover" &&
      _.safe(nextProps, "youtubeMetadata.doneUploading")) {
      $("#CoverVideoModal").modal("hide");
    }
  }

  render () {
    const {user, isOwner, coverVideoMetadata, youtubeMetadata, updateProfile} = this.props;

    if (_.isEmpty(user)) {
      return <div/>
    }

    return (
      <div className="div-container m-b-s">
        <div className="row">
          <div className="col-sm-12">
            <div className="row">
              <div className="col-xs-12 col-sm-12">
                <div className="cover-video-container text-center">
                  {this.renderVideoByType()}
                  {this.renderChangeBtn()}
                </div>
              </div>
            </div>
          </div>
        </div>
        <VideoUploadModal modalId="CoverVideoModal"
          isOwner={user.__self}
          video={_.safe(user, "metadata.coverVideo",
            {secure_url : config.defaultYoutubeCoverVideo, resource_type : "youtube"})}
          checkYoutubeLink={ProfileActions.checkYoutubeLink}
          uploadVideo={ProfileActions.uploadCoverVideo}
          updateProfile={ProfileActions.updateProfile}
          youtubeMetadata={youtubeMetadata}
          videoMetadata={coverVideoMetadata}/>
      </div>
    )
  }

  renderVideoByType() {
    const {user} = this.props;
    const videoId = _.safe(user, "metadata.coverVideo.secure_url");
    const resourceType = _.safe(user, "metadata.coverVideo.resource_type");
    const src = `https://www.youtube.com/embed/${videoId}?autoplay=0&showinfo=0&controls=0`;

    if (videoId && resourceType === "youtube") {
      return <iframe className="cover-video" width="100%" height={465} src={src} frameBorder="0" allowFullScreen/>
    } else if (resourceType === "video") {
      return (
        <video className="cover-video f-w"
          controls={true}
          muted={true}
          autoPlay={true}
          autoBuffer={true}
          src={videoId || config.defaultCoverVideo}>
            Sorry, your browser doesn't support embedded videos,
            but don't worry, you can <a href={videoId || config.defaultCoverVideo}>download it</a>
            and watch it with your favorite video player!
        </video>
      )
    }
    return (
      <div className="cover-video">
        <img src="images/video-placeholder.png" className="img-responsive f-w"/>
      </div>
    )
  }

  renderChangeBtn () {
    const {isOwner} = this.props;

    if (!isOwner) {
      return <div/>
    }

    return (
      <span className="change-cover-video hidden-container" onClick={this.onCoverVideoChange}>
        <i className="glyphicon glyphicon-film m-r-s"></i>
        change cover video
      </span>
    )
  }

  onCoverVideoChange () {
    $("#CoverVideoModal").modal("show");
  }
}
