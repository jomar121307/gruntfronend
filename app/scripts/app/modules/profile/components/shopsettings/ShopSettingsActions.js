"use strict";

import _ from "underscore";

import AppDispatcher from "../../../../dispatcher/AppDispatcher.js";
import ShopSettingsConstants from "./ShopSettingsConstants.js";

const ShopSettingsActions = {
  initialize : function () {
    AppDispatcher.handleViewAction({
      type : ShopSettingsConstants.SHOPSETTINGS_INITIALIZE
    });
  },

  setInfo : function (params) {
    AppDispatcher.handleViewAction({
      type : ShopSettingsConstants.SHOPSETTINGS_SET_INFO,
      params : {
        ...params
      }
    });
  },

  save : _.throttle(function (info) {
    AppDispatcher.handleViewAction({
      type : ShopSettingsConstants.SHOPSETTINGS_SAVE,
      params : {
        info
      }
    });
  }, 2000)
};

export default ShopSettingsActions;
