"use strict";

import keyMirror from "keymirror";

export default keyMirror({
  PURCHASES_CHANGE_EVENT : null,
  PURCHASES_INITIALIZE : null
});
