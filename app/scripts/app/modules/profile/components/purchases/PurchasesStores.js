"use strict";

import {EventEmitter} from "events";
import Request from "../../../../../utils/request.js";
import {register} from "../../../../dispatcher/AppDispatcher.js";

import PurchasesConstants from "./PurchasesConstants.js";

const initialState = {
  initialized : false,
  purchases : [],
  purchaseCount : 0,
  pagination : {
    page : 0,
    size : 100
  }
};

let state = $.extend(true, {}, initialState);

let Stores = Object.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(PurchasesConstants.PURCHASES_CHANGE_EVENT, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(PurchasesConstants.PURCHASES_CHANGE_EVENT, callback);
  },

  emitChange : function () {
    Stores.emit(PurchasesConstants.PURCHASES_CHANGE_EVENT);
  },

  getInitialState : function () {
    return initialState;
  },

  getState : function () {
    return state;
  }
});

const Actions = {
  [PurchasesConstants.PURCHASES_INITIALIZE] : function (params) {
    purchasesInitialize(function (err, data) {
      if (err) {
        return purchasesSetState(err, {});
      }

      purchasesSetState(null, {
        initialized : true,
        purchases : data.orders,
        purchaseCount : data.count
      });
    });
  }
}

Stores.dispatchIndex = register(payload => {
  const {type, params} =  payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function purchasesSetState (err, obj) {
  if (err) {
    toastr.error(_.safe(err, "xhr.responseText", err));
    console.error(err);
  }

  for (let prop in obj) {
    if (state.hasOwnProperty(prop)) {
      state[prop] = obj[prop];
    } else {
      console.warn("Invalid state for purchases state: " + prop);
    }
  }

  Stores.emitChange(PurchasesConstants.PURCHASES_CHANGE_EVENT);
}

function purchasesInitialize (callback) {
  const {pagination} = state;
  const query = {
    skip : pagination.size * pagination.page,
    limit : pagination.size,
    purchases : true,
    sort : "createdAt DESC"
  };

  Request.get("orders", query, function (err, purchases) {
    if (err) {
      return callback(err);
    }

    callback(null, purchases);
  });
}
