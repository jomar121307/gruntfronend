"use strict";

import React from "react";
import {render} from "react-dom";
import {Link} from "react-router";
import moment from "moment";

import MessageOwner from "./MessageOwner.jsx";
import VideoUploadModal from "./VideoUploadModal.jsx";
import ProfileActions from "../ProfileActions.js";

export default class User extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
      editing : false,
      error : {},
      birthday : {
        year : "1975",
        month : "Jan",
        day : "1"
      }
    };

    this.renderVideoByType = this.renderVideoByType.bind(this);
    this.renderChangeBtn = this.renderChangeBtn.bind(this);
    this.renderMessageMeBtn = this.renderMessageMeBtn.bind(this);
    this.editProfileInformation = this.editProfileInformation.bind(this);
    this.saveProfileInformation = this.saveProfileInformation.bind(this);
    this.closeProfileInformation = this.closeProfileInformation.bind(this);
    this.onEditingInfo = this.onEditingInfo.bind(this);
    this.onViewingInfo = this.onViewingInfo.bind(this);
    this.onSelectBirthday = this.onSelectBirthday.bind(this);
    this.onProfileVideoChange = this.onProfileVideoChange.bind(this);
    this.renderAddProductBtn = this.renderAddProductBtn.bind(this);
    this.onMessageUser = this.onMessageUser.bind(this);
  }

  componentWillUpdate (nextProps, nextState) {
    if (_.safe(nextProps, "profileVideoMetadata.progress") === 100 &&
      _.safe(nextProps, "profileVideoMetadata.isUploading")) {
      $("#ProfileVideoModal").modal("hide");
    } else if (_.safe(nextProps, "youtubeMetadata.type") === "profile" &&
      _.safe(nextProps, "youtubeMetadata.doneUploading")) {
      $("#ProfileVideoModal").modal("hide");
    }
  }

  render () {
    const {user,
        isOwner,
        profileVideoMetadata,
        youtubeMetadata,
        updateProfile,
        handleSend} = this.props;
    const {editing} = this.state;

    if (_.isEmpty(user)) {
      return (
        <div className="timeline-wrapper">
          <div className="timeline-item">
            <div className="animated-background">
              <div className="background-masker content-top"></div>
              <div className="background-masker content-second-line"></div>
              <div className="background-masker content-second-right"></div>
              <div className="background-masker content-third-line"></div>
              <div className="background-masker content-third-end"></div>
              <div className="background-masker content-fourth-line"></div>
              <div className="background-masker content-fourth-end"></div>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="div-container m-b-s">
        <div className="avatar-photo-container text-center">
          <input type="file" className="hidden" id="profileVideo" accept="video" />
          {this.renderVideoByType()}
          {this.renderChangeBtn(user.__self)}
        </div>
        {editing ? this.onEditingInfo() : this.onViewingInfo()}
        <VideoUploadModal modalId="ProfileVideoModal"
          isOwner={user.__self}
          video={_.safe(user, "metadata.profileVideo",
            {secure_url : config.defaultYoutubeProfileVideo, resource_type : "youtube"})}
          checkYoutubeLink={ProfileActions.checkYoutubeLink}
          uploadVideo={ProfileActions.uploadProfileVideo}
          updateProfile={ProfileActions.updateProfile}
          youtubeMetadata={youtubeMetadata}
          videoMetadata={profileVideoMetadata}/>
          <MessageOwner user={user}
           handleSend={handleSend}/>
      </div>
    )
  }

  renderVideoByType () {
    const {user} = this.props;
    const videoId = _.safe(user, "metadata.profileVideo.secure_url");
    const resourceType = _.safe(user, "metadata.profileVideo.resource_type");
    const src = `https://www.youtube.com/embed/${videoId}?autoplay=0&showinfo=0&controls=0`;

    if (videoId && resourceType === "youtube") {
      return <iframe className="profile-video" width="100%" height={124} src={src} frameBorder="0" allowFullScreen/>
    } else if (resourceType === "video") {
      return (
        <video className="profile-video f-w"
          muted={true}
          autoPlay={true}
          autoBuffer={true}
          loop={true}
          src={videoId || config.defaultProfileVideo}>
            Sorry, your browser doesn't support embedded videos,
            but don't worry, you can <a href={videoId || config.defaultProfileVideo}>download it</a>
            and watch it with your favorite video player!
        </video>
      )
    }

    return (
      <div className="profile-video-placeholder">
        <img src="images/video-placeholder.png" className="img-responsive" />
      </div>
    )
  }

  renderChangeBtn (isOwner) {
    if (!isOwner) {
      return <div/>
    }

    return (
      <span className="change-pp hidden-container" onClick={this.onProfileVideoChange}>
        <i className="fa fa-camera-retro m-r-s"></i>
        change profile video
      </span>
    )
  }

  onEditingInfo () {
    const {user, isOwner} = this.props;
    const {error} = this.state;
    const year = (user.birthday && moment(new Date(user.birthday)).year()) || this.state.birthday.year;
    const month = (user.birthday && moment(new Date(user.birthday)).month()) || this.state.birthday.month;
    const {day} = this.state.birthday;

    if (!user.__self) {
      return <div/>
    }

    return (
      <div className="info-container">
        <div className="d-flex">
          <div className={"form-group m-r-s no-m-b" + (error.firstName ? " has-error" : "")} style={{width : "50%"}}>
            <input className="form-control" ref="firstName" placeholder="Firstname" defaultValue={user.firstName}/>
            <span className="control-label pull-left">{error.firstName}</span>
          </div>
          <div className={"form-group no-m-b" + (error.lastName ? " has-error" : "")} style={{width : "50%"}}>
            <input className="form-control" ref="lastName" placeholder="Lastname" defaultValue={user.lastName || ""}/>
            <span className="control-label pull-left">{error.lastName}</span>
          </div>
        </div>
        <div className={"form-group no-m-b m-t-s" + (error.location ? " has-error" : "")}>
          <i className="fa fa-location-arrow m-r-s editing-glyphicon-style"></i>
          <input className="form-control" ref="location" placeholder="Country" />
          <span className="control-label pull-left">{error.location}</span>
        </div>
        <div className={"form-group no-m-b m-t-s" + (error.username ? " has-error" : "")}>
          <i className="glyphicon glyphicon-user m-r-s editing-glyphicon-style"></i>
          <input className="form-control" ref="username" placeholder="Username"/>
          <span className="control-label pull-left">{error.username}</span>
        </div>
        <div className="form-group no-m-b m-t-s">
          <div className="d-flex birthday-row">
            <span className="fa fa-birthday-cake m-r-s  control-label" />
            <select className="m-r-s form-control" onClick={this.onSelectBirthday.bind(null, "day")} defaultValue={day}>
              {_.range(1, moment(year + "-" + month, "YYYY-MMM").daysInMonth() + 1)
                .map(day => <option key={day}>{day}</option>)}
            </select>
            <select className="m-r-s form-control" onClick={this.onSelectBirthday.bind(null, "month")} defaultValue={month}>
              {_.range(12)
                .map(month => {
                  month = moment().month(month).format("MMM");
                  return (
                    <option key={month} value={month}>
                      {month}
                    </option>
                  )
                })}
            </select>
            <select className="form-control" onClick={this.onSelectBirthday.bind(null, "year")}  defaultValue={year}>
              {_.range(1900, moment().year() + 1)
                .map(year => <option key={year} value={year}>{year}</option>)}
            </select>
          </div>
        </div>
        <div className="m-t-s">
          <button className="btn btn-default m-r-s" onClick={this.closeProfileInformation}>Cancel</button>
          <button className="btn btn-primary" onClick={this.saveProfileInformation}>Save</button>
        </div>
      </div>
    );
  }

  onViewingInfo () {
    const {user, hasPaypalInfo} = this.props;
    let bday = user.birthday;

    if (bday) {
      bday = moment(bday).format("MMMM DD, YYYY");
    }

    return (
      <div className="info-container">
        <h4 className="m-t-s ellipsis">
          <span>{user.firstName} {user.lastName || ""}</span>
          {this.renderEditProfileBtn(user.__self)}

        </h4>
        {
          _.safe(user, "email") ?
          <div className="m-t-s ellipsis">
            <i className="fa fa-envelope m-r-s"></i>
            <span>{user.email}</span>
          </div> : ""
        }
        <div className="m-t-s ellipsis">
          <i className="glyphicon glyphicon-user m-r-s"></i>
          <span>{_.safe(user, "location.username", "Edit to add your Username")}</span>
        </div>
        {
          _.safe(user, "location.country") ?
          <div className="m-t-s ellipsis">
            <i className="fa fa-location-arrow m-r-s"></i>
            <span>{_.safe(user, "location.country", "")}</span>
          </div> : ""
        }
        <div className="m-t-s ellipsis">
          <span className="glyphicon glyphicon-calendar m-r-s" />
          <span>Joined {moment(user.createdAt).format("MMMM DD, YYYY")}</span>
        </div>
        {
          user.birthday ?
          <div className="m-t-s">
            <span className="fa fa-birthday-cake m-r-s" />
            {bday}
          </div> : ""
        }
        <div className="m-t-s">
          {this.renderMessageMeBtn(user.__self)}
          {this.renderAddProductBtn(user.__self, hasPaypalInfo)}
        </div>
      </div>
    );
  }

  renderAddProductBtn (isOwner, hasPaypalInfo) {
    if (!isOwner) {
      return <div/>
    }

    return (
      <Link to="/product/add">
        <button className="btn btn-primary btn-block m-t-s">
          add product
        </button>
      </Link>
    )
  }

  renderEditProfileBtn (isOwner) {
    if (!isOwner) {
      return <div/>
    }

    return (
      <span className="glyphicon glyphicon-edit glyphicons-edit-style m-l-s"
        onClick={this.editProfileInformation} />
    )
  }

  renderMessageMeBtn (isOwner) {
    const {user} = this.props;
    if (isOwner) {
      return <div/>
    }

    return (
      <button className="btn btn-primary btn-block"
        onClick={this.onMessageUser} >
          message me
      </button>
    )
  }

  onProfileVideoChange () {
    const {user} = this.props;

    if (!user.__self) {
      return;
    }

    $('#ProfileVideoModal').modal('show');
  }

  editProfileInformation () {
    this.setState({
      editing : true
    });
  }

  saveProfileInformation () {
    const {day, month, year} = this.state.birthday;
    let user = {
      firstName : this.refs["firstName"].value,
      lastName : this.refs["lastName"].value,
      location : {
        country : this.refs["location"].value
      },
      birthday : new Date(year + "-" + month + "-" + day)
    };
    let error = {};

    if (!user.firstName) {
      error.firstName = "First name is required.";

      return this.setState({
        error
      });
    } else if (!user.lastName) {
      error.lastName = "Last name is required.";

      return this.setState({
        error
      });
    } else if (!moment(user.birthday).isValid()) {
      error.birthday = "Birthday is not a valid date."

      return this.setState({
        error
      });
    }

    this.setState({
      editing : false
    });
    ProfileActions.updateProfile(user);
  }

  closeProfileInformation () {
   this.setState({
      editing : false,
      birthday : {
        year : "1975",
        month : "1",
        day : "1"
      }
    });
  }

  onSelectBirthday (type, e) {
    const {birthday} = this.state;
    birthday[type] = e.target.value;
    this.setState({
      birthday : birthday
    });
  }

  onMessageUser (e) {
    const {isLoggedIn} = this.context;

    if (!isLoggedIn) {
      $("#LoginRegister-body").modal("show");
    } else {
      $("#messageOwner").modal("show");
    }
  }
}

User.contextTypes = {
  router : React.PropTypes.object.isRequired,
  isLoggedIn : React.PropTypes.bool.isRequired
};
