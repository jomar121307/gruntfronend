"use strict";

import React, {Component} from "react";

export default class CustomerOrders extends Component {
  render() {
    return (
      <div id="customer-order-container">
        <h4>Customer Orders</h4>
        <p>Customers who bought your item(s) is/are shown below. Click the row to see the complete details and manage them.</p>
        <div className="table-responsive m-b-s">
          <table className="table">
            <thead>
              <tr>
                <th>Order #</th>
                <th>Bought by</th>
                <th>Purchased Date</th>
                <th>Status</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    )
  }
};
