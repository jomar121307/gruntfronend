"use strict";

import async from "async";
import {EventEmitter} from "events";

import Request from "../../../../../utils/request.js";
import {register} from "../../../../dispatcher/AppDispatcher.js";

import OrdersConstants from "./OrdersConstants.js";

const initialState = {
  initialized : false,
  orders : [],
  orderCount : 0,
  pagination : {
    page : 0,
    size : 100
  },
  updatingStatus : false,
  activateRefundSetup : false,
};

let state = $.extend(true, {}, initialState);

let Stores = Object.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(OrdersConstants.ORDERS_CHANGE_EVENT, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(OrdersConstants.ORDERS_CHANGE_EVENT, callback);
  },

  _emitChange : function () {
    Stores.emit(OrdersConstants.ORDERS_CHANGE_EVENT);
  },

  getInitialState : function () {
    return initialState;
  },

  getState : function () {
    return state;
  }
});

const Actions = {
  [OrdersConstants.ORDERS_INITIALIZE] : function (params) {
    ordersInitialize(params, function (err, data) {
      if (err) {
        return ordersSetState(err, {
          initialized : false
        });
      }

      ordersSetState(null, {
        initialized : true,
        orders : data.orders,
        orderCount : data.count
      });
    });
  },

  [OrdersConstants.ORDERS_SET_STATUS] : function (params) {
    ordersSetState(null, {
      updatingStatus : true
    });

    ordersSetStatus(params, function (err, data) {
      if (err) {
        return ordersSetState(err, {
          updatingStatus : false
        });
      } else if (data.message === "No api access.") {
        ordersSetState(null, {
          updatingStatus : false,
          activateRefundSetup : true
        });

        ordersSetState(null, {
          activateRefundSetup : false
        });

        toastr.warning("Unable to perform refund. Application has not access to acount.");
        return;
      }

      const newOrders = _.extend([], true, state.orders);
      const idx = _.findIndex(state.orders, {
        id : params.orderId
      });

      if (idx === -1) {
        return;
      }

      newOrders[idx] = data;
      ordersSetState(null, {
        orders : newOrders,
        updatingStatus : false
      });

      toastr.success("Status changed.");
    });
  }
}

Stores.dispatchIndex = register(payload => {
  const {type, params} =  payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function ordersSetState (err, obj) {
  if (err) {
    toastr.error(_.safe(err, "xhr.responseText", err));
    console.error(err);
  }

  for (var prop in obj) {
    if (state.hasOwnProperty(prop)) {
      state[prop] = obj[prop];
    } else {
      console.warn("Invalid state for orderlist state: " + prop);
    }
  }

  Stores._emitChange(OrdersConstants.ORDERS_CHANGE_EVENT);
}

function ordersInitialize (options, callback) {
  const {pagination} = state;
  const query = {
    dogmode : options.dogmode,
    skip : pagination.size * pagination.page,
    limit : pagination.size,
    sort : "createdAt DESC"
  };

  Request.get("orders", query, function (err, orders) {
    if (err) {
      return callback(err);
    }

    callback(null, orders);
  });
}

function ordersSetStatus (params, callback) {
  Request.post(`orders/updatestatus/${params.orderId}`, {
    status : params.status
  }, callback);
}
