"use strict";

import _ from "underscore";

import AppDispatcher from "../../../../dispatcher/AppDispatcher.js";
import OrdersConstants from "./OrdersConstants.js";

const OrdersActions = {
  initialize : function (dogmode) {
    AppDispatcher.handleViewAction({
      type : OrdersConstants.ORDERS_INITIALIZE,
      params : {
        dogmode
      }
    });
  },

  setStatus : function (orderId, status) {
    AppDispatcher.handleViewAction({
      type : OrdersConstants.ORDERS_SET_STATUS,
      params : {
        orderId,
        status
      }
    });
  }
};

export default OrdersActions;
