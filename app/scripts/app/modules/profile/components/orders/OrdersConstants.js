"use strict";

import keyMirror from "keymirror";

export default keyMirror({
  ORDERS_CHANGE_EVENT : null,
  ORDERS_INITIALIZE : null,
  ORDERS_SET_STATUS : null
});
