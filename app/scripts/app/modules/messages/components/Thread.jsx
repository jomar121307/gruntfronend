"use strict";

import React from "react";
import {Link} from "react-router";
import moment from "moment";
import ClassNames from "classnames"
import Utils from "../../../../utils/utils.js";

export default class Thread extends React.Component {
  constructor (props) {
    super(props);
  }

  componentDidMount () {
    const {isLoading, loadMore} = this.props;

    document.getElementById('mainContainer')
      .getElementsByClassName('gm-scroll-view')[0]
      .onscroll = function () {
        if ((1 - (this.scrollTop / this.scrollHeight)) > 0.7 && !isLoading) {
          loadMore();
        }
      }
  }

  render () {
    const {thread} = this.props;

    if (_.isEmpty(thread)) {
      return <div/>
    }

    const loggedUser = thread.users[0].__self && thread.users[0] || thread.users[1];
    const otherUser = !thread.users[0].__self && thread.users[0] || thread.users[1];

    return (
      <div className="media">
        {thread.messages.map(this.listMessages.bind(this, loggedUser, otherUser))}
      </div>
    )
  }

  listMessages (loggedUser, otherUser, message, index, messages) {
    const now = moment();
    const msgTimestamp = moment(new Date(message.createdAt));
    const prevMsgTimestamp = !!index && moment(new Date(messages[index - 1].createdAt));
    let isChain = false;
    let user = otherUser;
    let thumb = _.safe(otherUser, "metadata.profileVideo.secure_url", config.defaultYoutubeProfileVideo);
    let timeFormat = "h:mma";
    const content = _.safe(message, "payload.message", "")
      .split("\n").map((s,i) => <span key={s + i}>{s}<br/></span>);

    if (loggedUser.id === message.from) {
      user = loggedUser;
      thumb = _.safe(loggedUser, "metadata.profileVideo.secure_url", config.defaultYoutubeProfileVideo);
    }

    if (_.safe(user, "metadata.profileVideo.resource_type", "youtube") === "youtube") {
      thumb = cloudinary.url(`https://i.ytimg.com/vi/${thumb}/default.jpg`, {
        height: 45,
        width: 45,
        crop: "fill",
        type : "fetch"
      });
    } else if (_.safe(user, "metadata.profileVideo.resource_type") === "video") {
      const profileVideo = user.metadata.profileVideo;

      thumb = cloudinary.url(`${profileVideo.public_id}.jpg`, {
        height: 45,
        transformation: ["media_lib_thumb"],
        width: 45,
        crop: "fill",
        resource_type: "video",
        secure : true
      });
    }

    if (!message || new Date() < new Date(message.createdAt)) {
      message = {createdAt : new Date()};
      timeFormat = "ddd";
    } else if (now.date() - msgTimestamp.date() > 0 &&
      now.diff(msgTimestamp, "days") < 7) {
      timeFormat = "ddd";
    } else if (now.diff(msgTimestamp, "days") > 7 &&
      now.year() - msgTimestamp.year() < 1) {
      timeFormat = "MMM D";
    } else if (now.year() - msgTimestamp.year() >= 1) {
      timeFormat = "MM/DD/YY";
    }

    if (!!index && user.id === messages[index - 1].from &&
      msgTimestamp.diff(prevMsgTimestamp, "minutes") < 5) {
      isChain = true;
    }

    return (
      <div className="row" key={message.id}>
        <div className="col-sm-12 p-b-s">
          <div className={ClassNames("media-left", {"chain-msg-padding" : isChain})}>
            <Link to={`/shops/${user.id}`} className={ClassNames({hide : isChain})}>
              <img className="media-object" src={thumb} alt={user.firstName} />
            </Link>
          </div>
          <div className="media-body">
            <h4 className={ClassNames("media-heading", {hide : isChain})}>
              <span className="chat-name">{user.firstName} </span>
              <span className="sent-message">
                • {moment(message.createdAt).format(timeFormat)}
              </span>
            </h4>
            <div className={ClassNames("arrow-left", {hide : isChain})}></div>
            <span className="bubble-chat">{content}</span>
          </div>
        </div>
      </div>
    )
  }
}
