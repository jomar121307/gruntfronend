"use strict";

import keyMirror from "keymirror";

export default keyMirror({
  MESSAGES_CHANGE_EVENTS : null,
  MESSAGES_INITIALIZE : null,
  MESSAGES_SELECT_THREAD : null,
  MESSAGES_SEND : null,
  MESSAGES_UPDATE_THREADS : null,
  MESSAGES_LOAD_MORE_MESSAGE : null
});
