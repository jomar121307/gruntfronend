"use strict";

import _ from "underscore";

import AppDispatcher from "../../dispatcher/AppDispatcher.js";
import MessagesConstants from "./MessagesConstants.js";

const PaymentActions = {
  initialize : function (userId) {
    AppDispatcher.handleViewAction({
      type : MessagesConstants.MESSAGES_INITIALIZE,
      params : {
        userId
      }
    });
  },

  selectThread : function (threadId) {
    AppDispatcher.handleViewAction({
      type : MessagesConstants.MESSAGES_SELECT_THREAD,
      params : {
        threadId
      }
    });
  },

  send : function (message) {
    AppDispatcher.handleViewAction({
      type : MessagesConstants.MESSAGES_SEND,
      params : {
        message
      }
    });
  },

  updateThreads : function () {
    AppDispatcher.handleViewAction({
      type : MessagesConstants.MESSAGES_UPDATE_THREADS
    });
  },

  loadMore : _.throttle(function () {
    AppDispatcher.handleViewAction({
      type : MessagesConstants.MESSAGES_LOAD_MORE_MESSAGE
    });
  }, 1000, {trailing : false})
}

export default PaymentActions;
