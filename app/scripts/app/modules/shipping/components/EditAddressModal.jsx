import React from 'react';

export default class EditAddressModal extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="modal fade" id="EditAddressModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 className="modal-title" id="myModalLabel">Edit Address</h4>
                  </div>
                  <div className="modal-body">
                    <form className="form-horizontal">
                      <div className="form-group">
                        <label htmlFor="fullname" className="col-sm-4 control-label">Full Name</label>
                        <div className="col-sm-8">
                          <input type="text" className="form-control" id="fullname" defaultValue="Lester James Infiesto"/>
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="city" className="col-sm-4 control-label">City</label>
                        <div className="col-sm-8">
                          <input type="text" className="form-control" id="city" placeholder="Town / City" defaultValue="Cebu"/>
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="stateProvinceRegion" className="col-sm-4 control-label">State/Province/Region</label>
                        <div className="col-sm-8">
                          <input type="text" className="form-control" id="stateProvinceRegion" defaultValue="Cebu" />
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="zipCode" className="col-sm-4 control-label">Zip Code</label>
                        <div className="col-sm-8">
                          <input type="text" className="form-control" id="zipCode" defaultValue="6000"/>
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor="country" className="col-sm-4 control-label">Country</label>
                        <div className="col-sm-8">
                          <input type="text" className="form-control" id="country" defaultValue="Philippines" disabled="disabled"/>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-primary">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
        );
    }
}
