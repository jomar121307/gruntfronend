import React from 'react';
import {Link} from "react-router";

export default class AddAddress extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {hasAddresses, handleAddAddress} = this.props;

    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          <h3 className="panel-title">
            Add Address
            {this.renderBackBtn(hasAddresses)}
          </h3>
        </div>
        <div className="panel-body">
          <form className="form-horizontal">
            <div className="form-group">
              <label htmlFor="fullname" className="col-sm-4 control-label">
                Full Name
              </label>
              <div className="col-sm-8">
                <input type="text"
                  className="form-control"
                  id="fullname"
                  ref="fullname" />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="streetAddress" className="col-sm-4 control-label">
                Address Line
              </label>
              <div className="col-sm-8">
                <input type="text"
                  className="form-control"
                  id="streetAddress"
                  ref="streetAddress"
                  placeholder="Street address, P.O. box, company name, c/o" />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="city" className="col-sm-4 control-label">City</label>
              <div className="col-sm-8">
                <input type="text"
                  className="form-control"
                  id="city"
                  ref="city"
                  placeholder="Town / City" />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="province" className="col-sm-4 control-label">
                Province
              </label>
              <div className="col-sm-8">
                <input type="text"
                  className="form-control"
                  id="province"
                  ref="province" />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="zipCode" className="col-sm-4 control-label">
                Zip Code
              </label>
              <div className="col-sm-8">
                <input type="text"
                  className="form-control"
                  id="zipCode"
                  ref="zipCode" />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="country" className="col-sm-4 control-label">
                Country
              </label>
              <div className="col-sm-8">
                <input type="text"
                  className="form-control"
                  id="country"
                  value="Philippines"
                  disabled="disabled"/>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="country" className="col-sm-4 control-label"></label>
              <div className="col-sm-8">
                <button className="btn btn-primary btn-block" onClick={handleAddAddress}>
                  <i className="fa fa-book m-r-s"></i>add address</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }

  renderBackBtn (render) {
    if (render) {
      return <div/>
    }

    return (
      <Link to="/cart" className="back-link">
        <span className="pull-right">
          <span className="glyphicon glyphicon-chevron-left m-r-s"></span>
          <span>BACK</span>
        </span>
      </Link>
    )
  }
}
