"use strict";

import keyMirror from "keymirror";

export default keyMirror({
  SHIPPING_CHANGE_EVENT : null,
  SHIPPING_INITIALIZE : null,
  SHIPPING_RESET : null,
  SHIPPING_ADD : null,
  SHIPPING_SET_ERROR : null,
  SHIPPING_REMOVE : null,
  SHIPPING_PROCEED : null
});
