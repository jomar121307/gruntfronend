"use strict";

import AppDispatcher from "../../dispatcher/AppDispatcher.js";
import ShippingConstants from "./ShippingConstants.js";

const ShippingActions = {
  initialize : function () {
    AppDispatcher.handleViewAction({
      type : ShippingConstants.SHIPPING_INITIALIZE
    });
  },

  reset : function () {
    AppDispatcher.handleViewAction({
      type : ShippingConstants.SHIPPING_RESET
    });
  },

  add : function (address) {
    AppDispatcher.handleViewAction({
      type : ShippingConstants.SHIPPING_ADD,
      params : {
        address
      }
    });
  },

  setError : function (error) {
    AppDispatcher.handleViewAction({
      type : ShippingConstants.SHIPPING_SET_ERROR,
      params : {
        error
      }
    });
  },

  remove : function (id, index) {
    AppDispatcher.handleViewAction({
      type : ShippingConstants.SHIPPING_REMOVE,
      params : {
        id,
        index
      }
    });
  },

  proceed : function (id) {
    AppDispatcher.handleViewAction({
      type : ShippingConstants.SHIPPING_PROCEED,
      params : {
        id
      }
    });
  }
}

export default ShippingActions;
