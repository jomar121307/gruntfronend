"use strict";

import {EventEmitter} from "events";
import async from "async";
import _ from "underscore";
import safeObj from "safe-obj";

import Request from "../../../utils/request.js";
import {register} from "../../dispatcher/AppDispatcher.js";

import ShippingConstants from "./ShippingConstants.js";
import AppStores from "../../stores/AppStores.js";

const initialState = {
  initialized : false,
  showAddressOption : "",
  addresses : [],
  proceedToCheckout : false,
  invalidCheckoutInfo : false,
  provinces : AppStores.getState().provinces,
  error : {}
}

let state = _.assign({}, initialState);

let Stores = _.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(ShippingConstants.SHIPPING_CHANGE_EVENTS, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(ShippingConstants.SHIPPING_CHANGE_EVENTS, callback);
  },

  emitChange : function () {
    Stores.emit(ShippingConstants.SHIPPING_CHANGE_EVENTS);
  },

  getState : function () {
    return state;
  },

  getInitialState : function () {
    return initialState;
  }
});

const Actions = {
  [ShippingConstants.SHIPPING_INITIALIZE] : function (params) {
    shippingInitialize(function (err, data) {
      if (err) {
        return shippingSetState(err, {
          initialized : true
        });
      } else if (_.safe(data, "checkoutinfo.metadata.zcommerce.checkoutinfo.checkoutUserId")) {
        return shippingSetState(null, {
          invalidCheckoutInfo : true
        });
      }

      shippingSetState(null, {
        addresses : data.addresses,
        initialized : true
      });
    });
  },

  [ShippingConstants.SHIPPING_RESET] : function (params) {
    shippingSetState(null, initialState);
  },

  [ShippingConstants.SHIPPING_ADD] : function (params) {

    shippingAdd(params, function (err, data) {
      if (err) {
        return shippingSetState(err, {});
      }

      let newState = _.cloneDeep(state);
      newState.addresses.push(data);
      newState.error = {};
      shippingSetState(null, newState);
    });
  },

  [ShippingConstants.SHIPPING_SET_ERROR] : function (params) {
    shippingSetState(null, {
      error : params.error
    });
  },

  [ShippingConstants.SHIPPING_REMOVE] : function (params) {
    const oldState = _.assign({}, state);
    const newState = _.assign({}, state);
    newState.addresses.splice(params.index, 1);
    shippingSetState(null, newState);

    shippingRemove(params, function (err, data) {
      if (err) {
        return shippingSetState(err, oldState);
      }
    });
  },

  [ShippingConstants.SHIPPING_PROCEED] : function (params) {
    shippingProceed(params, function (err, data) {
      if (err) {
        return shippingSetState(err, {});
      }

      shippingSetState(null, {
        proceedToCheckout : true
      });
    });
  }
};

Stores.dispatchIndex = register(payload => {
  const {type, params} = payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function shippingSetState (err, newState) {
  if (err) {
    toastr.error(err.xhr.responseText);
    console.error(err);
  }

  for (var prop in newState) {
    if (state.hasOwnProperty(prop)) {
      state[prop] = newState[prop];
    } else {
      console.warn("Invalid state for shipping: " + prop);
    }
  }

  Stores.emitChange(ShippingConstants.CART_CHANGE_EVENTS);
}

function shippingInitialize (callback) {
  async.parallel({
    checkoutinfo : function (parallelCb) {
      Request.get("users/me", parallelCb);
    },
    addresses : function (parallelCb) {
      Request.get("shipping",{
        limit : 20
      },parallelCb)
    }
  }, callback)
}

function shippingAdd (options, callback) {
  Request.post("shipping", {
    address : options.address
  }, callback);
}

function shippingRemove (options, callback) {
  Request.delete(`shipping/${options.id}`, callback);
}

function shippingProceed (options, callback) {
  Request.post("setcheckoutinfo", {
    shippingId : options.id
  }, callback);
}
