"use strict";

import React, {Component} from "react";
import ReactDOM, {render} from "react-dom";
import {Link} from "react-router";
import EditAddressModal from "./components/EditAddressModal.jsx";
import AddAddress from "./components/AddAddress.jsx";
import ClassNames from "classnames";

import ShippingActions from "./ShippingActions.js";
import ShippingStores from "./ShippingStores.js";

export default class Shipping extends Component {
  constructor(props) {
    super(props);
    this.state = ShippingStores.getInitialState();

    this.onChange = this.onChange.bind(this);
    this.renderHasAddress = this.renderHasAddress.bind(this);
    this.handleSelectAddress = this.handleSelectAddress.bind(this);
    this.renderAddresslist = this.renderAddresslist.bind(this);
    this.handleAddAddress = _.throttle(this.handleAddAddress.bind(this), 1000, {trailing : false});
    this.handleRemoveAddress = _.throttle(this.handleRemoveAddress.bind(this), 1000, {trailing : false});
  }

  componentWillMount () {
    ShippingStores.addChangeListener(this.onChange);
    ShippingActions.initialize();
  }

  componentWillUpdate (nextProps, nextState) {
    if (nextState.proceedToCheckout && !this.state.proceedToCheckout) {
      this.context.router.push("/payment");
    }

    if (nextState.addresses.length > this.state.addresses.length) {
      this.refs["address-form"].reset();
    }
  }

  componentWillUnmount () {
    ShippingActions.reset();
    ShippingStores.removeChangeListener(this.onChange);
  }

  render() {
    const {addresses, provinces, error} = this.state;
    return (
      <div className="container">
        <div id="shipping-body">
          {this.renderHasAddress()}
          <EditAddressModal />
          <div className="panel panel-default">
            <div className="panel-heading">
              <h3 className="panel-title">
                Add Address
                {this.renderBackBtn(!!addresses.length)}
              </h3>
            </div>
            <div className="panel-body">
              <form className="form-horizontal" ref="address-form">
                <div className={ClassNames("form-group", {"has-error" : error.name})}>
                  <label htmlFor="name" className="col-sm-4 control-label">
                    <span>Full Name</span>
                  </label>
                  <div className="col-sm-8">
                    <input type="text"
                      className="form-control"
                      ref="name" />
                    <label className={ClassNames("control-label", {hide : !error.name})}>
                      {error.name}
                    </label>
                  </div>
                </div>
                <div className={ClassNames("form-group", {"has-error" : error.streetAddress})}>
                  <label htmlFor="addressLine" className="col-sm-4 control-label">
                    <span>Street address</span>
                  </label>
                  <div className="col-sm-8">
                    <input type="text"
                      className="form-control"
                      ref="streetAddress"
                      placeholder="Street address, P.O. box, company name, c/o" />
                    <label className={ClassNames("control-label", {hide : !error.streetAddress})}>
                      {error.streetAddress}
                    </label>
                  </div>
                </div>
                <div className={ClassNames("form-group", {"has-error" : error.city})}>
                  <label htmlFor="city" className="col-sm-4 control-label">
                    <span>City</span>
                  </label>
                  <div className="col-sm-8">
                    <input type="text"
                      className="form-control"
                      ref="city"
                      placeholder="Town / City" />
                    <label className={ClassNames("control-label", {hide : !error.city})}>
                      {error.city}
                    </label>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="province" className="col-sm-4 control-label">
                    <span>Province</span>
                  </label>
                  <div className="col-sm-8">
                    <select className="form-control" ref="province">
                      {provinces.map(p => <option key={p} value={p}>{p}</option>)}
                    </select>
                  </div>
                </div>
                <div className={ClassNames("form-group", {"has-error" : error.zipcode})}>
                  <label htmlFor="zipcode" className="col-sm-4 control-label">
                    <span>Zip Code</span>
                  </label>
                  <div className="col-sm-8">
                    <input type="text"
                      className="form-control"
                      ref="zipcode" />
                    <label className={ClassNames("control-label", {hide : !error.zipcode})}>
                      {error.zipcode}
                    </label>
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="country" className="col-sm-4 control-label">
                    <span>Country</span>
                  </label>
                  <div className="col-sm-8">
                    <input type="text"
                      className="form-control"
                      ref="country"
                      value="Philippines"
                      disabled="disabled" />
                  </div>
                </div>
                <div className="form-group">
                  <label htmlFor="country" className="col-sm-4 control-label"></label>
                  <div className="col-sm-8">
                    <button className="btn btn-primary btn-block" onClick={this.handleAddAddress}>
                      <i className="fa fa-book m-r-s"></i>add address</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderHasAddress () {
    const {addresses} = this.state;

    if (!addresses.length) {
      return <div/>
    }

    return (
      <div>
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">Choose Existing Address
            <Link to="/cart" className="back-link">
              <span className="pull-right">
                <span className="glyphicon glyphicon-chevron-left m-r-s"></span>
                <span>BACK</span>
              </span>
            </Link>
            </h3>
          </div>
          <div className="panel-body">
            <form>
              <ul className="list-unstyled">
                {addresses.map(this.renderAddresslist)}
              </ul>
            </form>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <p className="or-hr">
              <span>or</span>
            </p>
          </div>
        </div>
      </div>
    )
  }

  onChange () {
    this.setState(ShippingStores.getState());
  }

  renderAddresslist (entry, index) {
    const {showAddressOption} = this.state;
    const {fullName, streetAddress, city, province, zipcode, country} = entry.address;
    const address = [fullName, streetAddress, city, province, zipcode, country].join(" ");

    return (
      <li key={entry.id}>
        <div className="row">
          <div className="col-sm-8 d-flex">
            <input type="radio"
              id={entry.id}
              name="address"
              className="address-radio"
              onClick={this.handleSelectAddress.bind(null, entry.id)}/>
            <label className="" htmlFor={entry.id}>{address}</label>
          </div>
          <div className="col-sm-4">
            <span className={"pull-right " + (showAddressOption === entry.id ? "" : "hide")}>
              <button className="btn btn-xs btn-default m-r-s"
                onClick={this.handleRemoveAddress.bind(null, entry.id, index)}>
                remove
              </button>
              <button className="btn btn-xs btn-default m-r-s hide"
                data-toggle="modal"
                data-target="#EditAddressModal">
                  edit
              </button>
              <Link to="/payment">
                <button className="btn btn-xs btn-primary"
                  onClick={this.handleProceed.bind(null, entry.id)}>
                    ship here
                </button>
              </Link>
            </span>
          </div>
        </div>
      </li>
    );
  }

  renderBackBtn (render) {
    if (render) {
      return <div/>
    }

    return (
      <Link to="/cart" className="back-link">
        <span className="pull-right">
          <span className="glyphicon glyphicon-chevron-left m-r-s"></span>
          <span>BACK</span>
        </span>
      </Link>
    )
  }

  handleSelectAddress (addressID) {
    this.setState({
      showAddressOption : addressID
    });
  }

  handleAddAddress (e) {
    e.preventDefault();
    const address = {
      country : "Philippines"
    };
    let error = {};


    _.each(this.refs, (v, k) => {
      const value = _.safe(v, "value", "").trim();

      switch (k) {
        case "name":
          if (!value) {
            error[k] = "Full name is required.";
          } else {
            address.fullName = value;
          }
          break;
        case "streetAddress":
          if (!value) {
            error[k] = "Street address is required.";
          } else {
            address.streetAddress = value;
          }
          break;
        case "city":
          if (!value) {
            error[k] = "City is required.";
          } else {
            address.city = value;
          }
          break;
        case "province":
          if (!value) {
            error[k] = "Province is required.";
          } else {
            address.province = value;
          }
          break;
        case "zipcode":
          if (!value) {
            error[k] = "Zip code is required.";
          } else {
            address.zipcode = value;
          }
          break;
      }
    });

    if (!_.isEmpty(error)) {
      return ShippingActions.setError(error);
    }

    ShippingActions.add(address);
  }

  handleRemoveAddress (id, index, e) {
    e.preventDefault();
    ShippingActions.remove(id, index);
  }

  handleProceed (id, e) {
    e.preventDefault();
    ShippingActions.proceed(id);
  }
};

Shipping.contextTypes = {
  router : React.PropTypes.object.isRequired
}
