"use strict";

import React from "react";
import {Link} from "react-router";
import ClassNames from "classnames";

export default class HighlightItem extends React.Component {
  render () {
    const {containerClassName, product, initialized} = this.props;
    let image = cloudinary.url(_.safe(product,"metadata.photos.1.public_id", "") + ".jpg", {
      height : 302,
      crop : "scale"
    });
    let productName = _.safe(product, "name", "Product Coming Soon");

    if (!_.safe(product, "metadata.photos.1.public_id")) {
      image = "";
    }

    if (!initialized) {
      productName = "";
    }

    return (
      <div className={ClassNames("highlight-item-div", containerClassName)}>
        <Link className="text-decor-none" to={"/product/" +  _.safe(product, "id")}>
          <img className="img-full" src={image}/>
        </Link>
        <div className="item-desc">
          <p className="item-title">{productName}</p>
          <Link to={"/shop/" + _.safe(product, "userId.id")}>
            <p className="item-more-desc">
              {_.safe(product, "userId.firstName") + " " + _.safe(product, "userId.lastName", "Unknown")}
            </p>
          </Link>
          <p className={ClassNames("item-price", {"hide" : !!!_.safe(product, "currencyId")})}>
            {_.safe(product, "price" , "&nbsp;")} {_.safe(product, "currencyId.code", "PHP")}
          </p>
        </div>
      </div>
    )
  }
}
