"use strict";

import React from "react";
import {render} from "react-dom";
import {Link} from "react-router";
import {PulseLoader} from 'halogen';
import ClassNames from "classnames";

import HighlightItem from "./HighlightItem.jsx";

export default class Highlights extends React.Component {
  constructor (props){
    super(props);
    this.state = {
      currentCategory : "Fashion"
    };

    this.onHoverCategory = this.onHoverCategory.bind(this);
    this.listMainCategories = this.listMainCategories.bind(this);
    this.renderSubCategories = this.renderSubCategories.bind(this);
    this.renderCategories = this.renderCategories.bind(this);
  }

  render () {
    const {products, hightLightHeader, brands, initialized} = this.props;
    const {currentCategory} = this.state;

    return (
      <div className="row">
        <div className="col-sm-12">
          <h4>{hightLightHeader || "Categories"}</h4>
          <div className="row">
            <div className="col-sm-4 col-xs-12">
              {this.renderCategories()}
            </div>
            <div className="col-sm-8 col-xs-12" id="highlights-container">
              <div className="row">
                <div className="col-sm-12 hidden-xs">
                  <div className="row">
                    <div className="col-sm-12 m-b-s">
                      <div id="highlight-carousel" className="carousel slide highlight-item-div" data-ride="carousel">
                        <ol className="carousel-indicators">
                          <li data-target="#highlight-carousel" data-slide-to="0" className="active"></li>
                          <li data-target="#highlight-carousel" data-slide-to="1"></li>
                          <li data-target="#highlight-carousel" data-slide-to="2"></li>
                        </ol>
                        <div className="carousel-inner" role="listbox">
                          <div className="item active">
                            <img src="images/highlight-1.jpg" alt="..."/>
                          </div>
                          <div className="item">
                            <img src="images/cover-2.jpg" alt="..."/>
                          </div>
                          <div className="item">
                            <img src="images/cover-3.jpg" alt="..."/>
                          </div>
                        </div>
                        <a className="left carousel-control" href="#highlight-carousel" role="button" data-slide="prev">
                          <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          <span className="sr-only">Previous</span>
                        </a>
                        <a className="right carousel-control" href="#highlight-carousel" role="button" data-slide="next">
                          <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          <span className="sr-only">Next</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-12">
                  <div className="row">
                    <div className="col-sm-3 hidden-xs">
                      <HighlightItem product={products[0]} initialized={initialized}/>
                    </div>
                    <div className="col-sm-3 hidden-xs">
                      <HighlightItem product={products[1]} initialized={initialized}/>
                    </div>
                    <div className="col-sm-3 hidden-xs">
                      <HighlightItem product={products[2]} initialized={initialized}/>
                    </div>
                    <div className="col-sm-3 hidden-xs">
                      <HighlightItem product={products[3]} initialized={initialized}/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderCategories () {
    const {categories} = this.context;

    if (!categories.length) {
      return (
        <div className="row text-center" style={{paddingTop : "250px"}}>
          <PulseLoader color="#26A65B" size="16px" margin="4px"/>
        </div>
      )
    }

    return (
      <div className="row">
        <div className="col-xs-6">
          <div className="main-category-filter">
            <ul className="list-unstyled">
              {categories.map(this.listMainCategories)}
            </ul>
          </div>
        </div>
        <div className="col-xs-6">
          <div className="subcategory-filter">
            {this.renderSubCategories()}
          </div>
        </div>
      </div>
    )
  }

  renderSubCategories () {
    const {currentCategory} = this.state;
    const {categories} = this.context;
    const index = _.findIndex(categories, {name : currentCategory});
    const subCategories = categories[index].subcategories;
    return (
      <div>
        {subCategories.map(subcat => {
          return (
            <div key={subcat.groupheader.slug}>
              <p className="bold m-b-s">
                <Link to={`/c/${categories[index].slug}/${subcat.groupheader.slug}`}>
                  {subcat.groupheader.name}
                </Link>
              </p>
              <ul className="list-unstyled">
                {subcat.groupling.map(e => {
                  return (
                    <Link key={e.slug}
                      to={`/c/${categories[index].slug}/${subcat.groupheader.slug}/${e.slug}`}>
                        <li>{e.name}</li>
                    </Link>
                  )
                })}
              </ul>
            </div>
          )
        })}
      </div>
    );
  }

  listMainCategories (category) {
    return (
      <li key={category.slug}
        onMouseEnter={this.onHoverCategory.bind(null, category.name)}>
          {category.name}
      </li>
    );
  }

  onHoverCategory (types, e) {
    this.setState({
      currentCategory : types
    });
  }
}

Highlights.contextTypes = {
  categories : React.PropTypes.array.isRequired
}
