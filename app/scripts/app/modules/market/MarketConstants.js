"use strict";

import keyMirror from "keymirror";

export default keyMirror({
  MARKET_CHANGE_EVENT : null,
  MARKET_RESET : null,
  MARKET_SET_FILTERS : null,
  MARKET_INITIALIZE : null,
  MARKET_SEARCH_PRODUCT_NAME : null,
  MARKET_LOAD_MORE : null,
  MARKET_RESELL_PRODUCT : null
});
