"use strict";

import async from "async";
import {EventEmitter} from "events";

import Request from "../../../utils/request.js";
import Cloudinary from "../../../utils/cloudinary.js";

import {register} from "../../dispatcher/AppDispatcher.js";
import MarketConstants from "./MarketConstants.js";

const initialState = {
  brands : [],
  products : [],
  highlightedProducts : [],
  highlightedProductsCount : 0,
  productCount : 0,
  query : null,
  catlab : 'none',
  initialized : false,
  isSearching : false,
  isLoadingMore : false,

  page : 0,
  pageSize : 24,
  filters : {}
};

let state = $.extend(true, {}, initialState);

let Stores = Object.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(MarketConstants.MARKET_CHANGE_EVENT, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(MarketConstants.MARKET_CHANGE_EVENT, callback);
  },

  _emitChange : function () {
    Stores.emit(MarketConstants.MARKET_CHANGE_EVENT);
  },

  getInitialState : function () {
    return initialState;
  },

  getState : function () {
    return state;
  }
});

const Actions = {
  [MarketConstants.MARKET_RESET] : function (params) {
    marketResetState();
  },

  [MarketConstants.MARKET_INITIALIZE] : function (params) {
    if (params.options.reinitialize) {
      marketSetState(null, {
        isSearching : true
      });
    }

    marketInitialize(params.options, function (err, data) {
      if (err && params.options.reinitialize) {
        marketSetState(err, {
          initialized : true,
          isSearching : false
        });
        return;
      } else if (err) {
        return marketSetState(err, {
          initialized : true
        });
      }

      let obj = {
        products : data.products.products,
        productCount : data.products.count,
        highlightedProducts : data.highlights.products,
        highlightedProductsCount : data.highlights.count,
        // categories : data.categories || state.categories,
        brands : data.brands || state.brands,
        initialized : true
      }

      if (params.options.reinitialize) {
        obj.isSearching = false;
      }

      marketSetState(null, obj);
    });
  },

  [MarketConstants.MARKET_LOAD_MORE] : function (params) {
    if (state.productCount <= state.products.length) {
      return;
    }

    marketSetState(null, {
      isLoadingMore : true,
      page : state.page + 1
    });

    marketLoadMore(function (err, data) {
      if (err) {
        return marketSetState(err, {
          isLoadingMore : false
        });
      }

      marketSetState(null, {
        products : state.products.concat(data.products),
        productCount : data.count,
        isLoadingMore : false
      });
    });
  },

  [MarketConstants.MARKET_SEARCH_PRODUCT_NAME] : function (params) {
    const newState = _.assign({}, state, {
      isSearching : true,
      page : 0
    });

    marketSetState(null, newState);
    marketSearchProducts(params.payload, function (err, data) {
      if (err) {
        return marketSetState(err, {
          isSearching : false
        });
      }

      marketSetState(null, {
        products : data.products,
        productCount : data.count,
        isSearching : false
      });
    });
  }
}

Stores.dispatchIndex = register(payload => {
  const {type, params} = payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function marketResetState() {
  marketSetState(null, initialState);
}

function marketSetState(err, obj) {
  if (err) {
    toastr.error(err.xhr.responseText);
    console.error(err);
  }

  //ensure only valid states are written
  for (var prop in obj) {
    if (state.hasOwnProperty(prop)) {
      state[prop] = obj[prop];
    } else {
      console.warn("Invalid state for market: " + prop);
    }
  }

  Stores._emitChange(MarketConstants.MARKET_CHANGE_EVENT);
}

/**
 * Fetch all brands and categories to used as product filters
 *
 * @method     marketInitialize
 * @param      {object}  options  { description }
 * @param      {Function}  callback  { description }
 */
function marketInitialize (options, callback) {
  async.parallel({
    categories : function (parallelCb) {
      if (options.reinitialize) {
        return async.nextTick(parallelCb);
      }

      Request.get("misc", {
        where : JSON.stringify({name : "Product Categories"})
      }, parallelCb);
    },

    brands : function (parallelCb) {
      if (options.reinitialize) {
        return async.nextTick(parallelCb);
      }

      Request.get("brands", parallelCb);
    },

    products : function (parallelCb) {
      const query = {
        limit : state.pageSize,
        where : {},
        sort : 'category'
      };

      if (_.safe(options, "params.splat")) {
        query.where.category = {
          startsWith : options.params.splat.replace(/[/]/g, ".")
        };
      }

      if (_.safe(options, "query.q")) {
        query.where.name = {
          contains : options.query.q
        };
      }

      query.where = JSON.stringify(query.where);
      Request.get("products", query, parallelCb);
    },

    highlights : function (parallelCb) {
      const query = {
        random : true,
        where : {
          xRandom : Math.random(),
          yRandom : Math.random(),
        },
        limit : 6,
        sort : {
          createdAt : 1
        }
      };


      if (_.safe(options, "splat")) {
        query.where.category = options.params.splat.replace(/[/]/g, ".");
      }

      query.where = JSON.stringify(query.where);
      Request.get("products", query, parallelCb);
    }
  }, callback);
}

/**
 * Fetches more products.
 *
 * @method     marketLoadMore
 * @param      {Function}  callback  { description }
 */
function marketLoadMore (callback) {
  let query = {
    where : {
      category : state.filters.category,
      brand : state.filters.brand,
      name : state.filters.name
    },
    skip : state.pageSize * state.page,
    limit : state.pageSize
  };

  if (!query.where.name) {
    delete query.where.name;
  }

  if (!query.where.category) {
    delete query.where.category;
  }

  if (!query.where.brand) {
    delete query.where.brand;
  }

  query.where = JSON.stringify(query.where);

  Request.get("products", query, callback);
}

/**
 * Find products based on query.
 *
 * @method     marketSearchProducts
 * @param      {Function}  callback  { description }
 */
function marketSearchProducts (options, callback) {
  let query = {
    where : {
      name : {
        contains : options.searchTerm
      }
    }
  };

  if (!options.searchTerm) {
    delete query.where.name;
  }

  if (_.safe(options, "splat")) {
    query.where.category = options.splat.replace("/", ".");
  }

  query.where = JSON.stringify(query.where);

  Request.get("products", query, callback);
}
