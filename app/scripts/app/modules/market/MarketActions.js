"use strict";

import _ from "underscore";
import AppDispatcher from "../../dispatcher/AppDispatcher.js";
import MarketConstants from "./MarketConstants";

const MarketActions = {
  initialize : function (options) {
    AppDispatcher.handleViewAction({
      type : MarketConstants.MARKET_INITIALIZE,
      params : {
        options
      }
    });
  },

  searchProductName : _.debounce(function (payload) {
    AppDispatcher.handleViewAction({
      type : MarketConstants.MARKET_SEARCH_PRODUCT_NAME,
      params : {
        payload
      }
    })
  }, 500),

  reset : function () {
    AppDispatcher.handleViewAction({
      type : MarketConstants.MARKET_RESET
    });
  },

  loadMore : _.throttle(function () {
    AppDispatcher.handleViewAction({
      type : MarketConstants.MARKET_LOAD_MORE
    });
  }, 500, {trailing : false}),

  resellProduct : function (id) {
    AppDispatcher.handleViewAction({
      type : MarketConstants.MARKET_RESELL_PRODUCT,
      params : {
        productId : id
      }
    });
  }
}

export default MarketActions;
