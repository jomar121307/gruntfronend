"use strict";

import React, {Component, PropTypes} from "react";
import classNames from "classnames"
import ReactTooltip from "react-tooltip";

import Utils from "../../../../utils/utils.js";

import Cropper from "../../misc/Cropper.jsx";

export default class ProductName extends Component {
  constructor (props) {
    super(props);

    this.uploadImage = this.uploadImage.bind(this);
  }

  shouldComponentUpdate (nextProps, nextState) {
    return !Utils.deepCompare(nextProps, this.props);
  }

  render () {
    const {
      mode,
      errors,
      name,
      brand,
      description,
      photo,
      cropperOptions,
      handleInputChange,
      handlePhotoChange
    } = this.props;

    return (
      <div className="brand-container">
        <div className="outer-container">
          <div className="div-container">
            <div className="row">
              <div className="col-sm-12">
                <h4>{mode} Product</h4>
                <hr/>
                <div className="row">
                  <form className="form-horizontal">
                    <div className="col-sm-6">
                      <div className={classNames("form-group",
                        {"has-error" : errors.brand})}>
                        <label htmlFor="brand" className="col-sm-4 control-label">
                          Brand :
                        </label>
                        <div className="col-sm-8">
                          <input className={classNames("form-control",
                            {"input-has-error" : errors.brand})}
                            value={brand}
                            onChange={handleInputChange.bind(null, "brand")}
                            data-tip
                            data-for={!!!errors.brand || "productForm_brand"}/>
                          <ReactTooltip id="productForm_brand" type="error" effect="solid">
                            <span>{errors.brand}</span>
                          </ReactTooltip>
                        </div>
                      </div>
                      <div className={classNames("form-group", {"has-error" : errors.name})}>
                        <label htmlFor="name" className="col-sm-4 control-label">
                          Name :
                        </label>
                        <div className="col-sm-8">
                          <input className={classNames("form-control",
                            {"input-has-error" : errors.name})}
                            value={name}
                            onChange={handleInputChange.bind(null, "name")}
                            data-tip
                            data-for={!!!errors.name || "productForm_name"}/>
                          <ReactTooltip id="productForm_name" type="error" effect="solid">
                            <span>{errors.name}</span>
                          </ReactTooltip>
                        </div>
                      </div>
                      <div className={classNames("form-group", {"has-error" : errors.description})}>
                        <label htmlFor="description" className="col-sm-4 control-label">
                          Description :
                        </label>
                        <div className="col-sm-8">
                          <textarea className={classNames("form-control",
                            {"input-has-error" : errors.description})}
                            value={description}
                            onChange={handleInputChange.bind(null, "description")}
                            data-tip
                            data-for={!!!errors.description || "productForm_description"}>
                          </textarea>
                          <ReactTooltip id="productForm_description" type="error" effect="solid">
                            <span>{errors.description}</span>
                          </ReactTooltip>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className={classNames("panel", {
                        "panel-default" : !!!errors.photo,
                        "panel-danger" : !!errors.photo })}
                        data-tip
                        data-for={!!!errors.photo || "productForm_photo"}>
                        <div className="panel-heading">
                          <h3 className="panel-title">Product Photo</h3>
                        </div>
                        <div className="panel-body">
                          <Cropper id="image-cropper"
                            fileInputId="selectedPic"
                            options={cropperOptions}
                            photo={photo}
                            onPhotoChange={handlePhotoChange}/>
                          <button className="btn btn-primary btn-block m-t-s" onClick={this.uploadImage}>select image</button>
                        </div>
                        <ReactTooltip id="productForm_photo" type="error" effect="solid">
                          <span>{errors.photo}</span>
                        </ReactTooltip>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  uploadImage (e){
    e.preventDefault();
    document.getElementById("selectedPic").click();
  }
}

ProductName.propTypes = {
  errors : PropTypes.object.isRequired,
  name : PropTypes.string.isRequired,
  brand : PropTypes.string.isRequired,
  description : PropTypes.string.isRequired,
  photo : PropTypes.string.isRequired,
  cropperOptions : PropTypes.object.isRequired
}
