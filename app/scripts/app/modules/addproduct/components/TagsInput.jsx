"use strict";

import React, {Component, PropTypes} from "react";
import Tags from "react-tag-input";

export default class TagsInput extends Component {
  constructor (props) {
    super(props);

    this.state = {
      tags : []
    }

    this.onAdd = this.onAdd.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onDrag = this.onDrag.bind(this);
  }

  componentWillMount () {
    const tags = this.props.tags.map((tag, idx) => {
      return {
        id : _.random(0,999) + "" + idx,
        text : tag
      }
    });

    this.setState({
      tags : tags
    });
  }

  componentWillReceiveProps (nextProps) {
    const tags = nextProps.tags.map((tag, idx) => {
      return {
        id : _.random(0,999) + "" + idx,
        text : tag
      }
    });

    this.setState({
      tags : tags
    });
  }

  render () {
    const {tags} = this.state;

    return (
      <div>
        <Tags.WithContext tags={tags}
          autofocus={false}
          placeholder="Add tags for this product."
          class="form-control"
          handleAddition={this.onAdd}
          handleDelete={this.onDelete}
          handleDrag={this.onDrag} />
      </div>
    )
  }

  onAdd (tag) {
    const {onChange} = this.props;
    let tags = this.state.tags;

    if (tags.length === 10) return;

    if (tags.length) {
      tags.push({
        id : tags[tags.length - 1].id + 1,
        text : tag
      });
    } else {
      tags.push({
        id : 0,
        text : tag
      });
    }

    this.setState({
      tags : tags
    });

    onChange(tags.map(tag => tag.text));
  }

  onDelete (tagIndex) {
    const {onChange} = this.props;
    let tags = this.state.tags;

    tags.splice(tagIndex, 1);
    this.setState({
      tags : tags
    });

    onChange(tags.map(tag => tag.text));
  }

  onDrag (tag, currentPosition, newPosition) {
  }
}

React.propTypes = {
  tags : PropTypes.array.isRequired,
  onChange : PropTypes.func.isRequired
}
