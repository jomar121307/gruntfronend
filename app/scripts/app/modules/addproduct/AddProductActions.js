"use strict";

import _ from "underscore";

import AppDispatcher from "../../dispatcher/AppDispatcher.js";
import AddProductConstants from "./AddProductConstants.js";

const AddProductActions = {
  initialize : function (productId) {
    AppDispatcher.handleViewAction({
      type : AddProductConstants.ADDPRODUCT_INITIALIZE,
      params : {
        productId
      }
    });
  },

  reset : function () {
    AppDispatcher.handleViewAction({
      type : AddProductConstants.ADDPRODUCT_RESET
    });
  },

  setProduct : function (newProduct) {
    AppDispatcher.handleViewAction({
      type : AddProductConstants.ADDPRODUCT_SET_PRODUCT,
      params : {
        newProduct
      }
    });
  },

  setFormError : function (errors) {
    AppDispatcher.handleViewAction({
      type : AddProductConstants.ADDPRODUCT_SET_FORM_ERROR,
      params : {
        errors
      }
    });
  },

  setCommissionType : function (type) {
    AppDispatcher.handleViewAction({
      type : AddProductConstants.ADDPRODUCT_SET_COMMISSION_TYPE,
      params : {
        type
      }
    });
  },

  setShippingType : function (type) {
    AppDispatcher.handleViewAction({
      type : AddProductConstants.ADDPRODUCT_SET_SHIPPING_TYPE,
      params : {
        type
      }
    });
  },

  setTac : function (hasAgreed) {
    AppDispatcher.handleViewAction({
      type : AddProductConstants.ADDPRODUCT_AGREE_TO_TAC,
      params : {
        hasAgreed
      }
    });
  },

  uploadProduct : _.throttle(function (product) {
    AppDispatcher.handleViewAction({
      type : AddProductConstants.ADDPRODUCT_FINISH,
      params : {
        product
      }
    });
  }, 500),

  checkName : _.debounce(function (name) {
    AppDispatcher.handleViewAction({
      type : AddProductConstants.ADDPRODUCT_CHECK_NAME,
      params : {
        name
      }
    });
  }, 500)
}

export default AddProductActions;
