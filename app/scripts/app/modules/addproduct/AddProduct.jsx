"use strict";

import React, {Component, PropTypes} from "react";
import {Link} from 'react-router';
import ClassNames from "classnames";
import ReactTooltip from "react-tooltip";
import numeral from "numeral";
import {PulseLoader} from "halogen";

import Utils from "../../../utils/utils.js";

import Loader from "../../views/Loader.jsx";
import TagsInput from "./components/TagsInput.jsx";
import ShippingInformation from "./components/ShippingInformation.jsx";
import ProductName from "./components/ProductName.jsx";
import ProductInformation from "./components/ProductInformation.jsx";

import AddProductActions from "./AddProductActions.js";
import AddProductStores from "./AddProductStores.js";

const MAX_PRICE = 10000000;

export default class AddProduct extends Component {
  constructor (props) {
    super (props);

    this.state = AddProductStores.getInitialState();

    this.onChange = this.onChange.bind(this);
    this.shippingtype = this.shippingtype.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleTagsChange = this.handleTagsChange.bind(this);
    this.handleCommissionTypeChange = this.handleCommissionTypeChange.bind(this);
    this.handlePhotoChange = this.handlePhotoChange.bind(this);
    this.onAddLocation = this.onAddLocation.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }

  componentWillMount () {
    AddProductStores.addChangeListener(this.onChange);
    AddProductActions.initialize(this.props.params.id);
  }

  componentWillUpdate(nextProps, nextState) {
    const {uploadProgress, isUploading} = this.state;

    if (nextState.mode === "Edit" && !nextState.queriedProduct.__self) {
        this.context.router.replace("/");
    } else if (uploadProgress === 100 && isUploading &&
      !nextState.uploadProgress && !nextState.isUploading) {
        this.context.router.push(`/product/${nextState.product.id}`);
    } else if (nextState.initialized && nextState.invalidShopSettings) {
      this.context.router.replace({
        pathname : "myshop/shopsettings",
        query : {
          nppl : true
        }
      });
      toastr.error("Please provide an eCash account to continue.")
      return;
    }
  }

  componentWillUnmount () {
    AddProductActions.reset();
    AddProductStores.removeChangeListener(this.onChange);
  }

  render() {
    const {
      initialized,
      invalidShopSettings,
      mode,
      product,
      shippingType,
      cropperOptions,
      provinces,
      errors,
      agreedToTerms,
      uploadProgress,
      isUploading
    } = this.state;
    const {categories} = this.context;

    let saveBtnValue = "Create Product";

    if (!initialized && invalidShopSettings) {
      return <Loader colStyle={{paddingTop : "50px"}} />
    }

    if (mode === "Edit" && isUploading) {
      saveBtnValue = "Updating Product";
    } else if (mode === "Edit") {
      saveBtnValue = "Update Product";
    } else if (isUploading) {
      saveBtnValue = "Creating Product"
    }

    cropperOptions.onImageError = (e) => {
      if (e.code === 1) {
        toastr.error(e.message + " It must be at least 300px by 300px.");
      } else {
        toastr.error(e.message);
      }
    }

    return (
      <div id="addProduct-body">
        <div className="add-product-header-container text-center">
          <h3>{mode} Product</h3>
          <p>{"Let's"} get started! Tell us about your product.</p>
        </div>
        <ProductName mode={mode}
          errors={errors}
          name={product.name || ""}
          brand={product.brand || ""}
          description={product.description || ""}
          photo={_.safe(product, "metadata.photos.0.secure_url", "")}
          cropperOptions={cropperOptions}
          handleInputChange={this.handleInputChange}
          handlePhotoChange={this.handlePhotoChange} />
        <ProductInformation errors={errors}
          selectedCategory={product.category || ""}
          categories={categories}
          quantity={product.quantity || 0}
          status={_.safe(product, "metadata.status") || ""}
          tags={_.safe(product, "metadata.tags"), []}
          price={parseInt(product.price) || 0}
          resellCommission={_.safe(product, "metadata.resellCommission",
            {type : "percentage", value : 0})}
          handleInputChange={this.handleInputChange}
          handleTagsChange={this.handleTagsChange}
          handleCommissionTypeChange={this.handleCommissionTypeChange} />
        <div className="brand-container">
          <div className="outer-container">
            <div className="div-container">
              <div className="row">
                <div className="col-sm-12">
                  <h4>Shipping Information</h4>
                  <hr/>
                  <div className="row">
                    <div className="col-sm-8">
                      <ShippingInformation provinces={provinces}
                        shippingData={_.safe(product, "metadata.shippingData",
                          [{location : "Philippines", singleCost : 0, multipleCost : 0}])}
                        onAddLocation={this.onAddLocation}/>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-12">
                      <hr />
                      <p>
                        <span>
                          <input type="checkbox"
                            id="agreeTermsAndCondition"
                            checked={agreedToTerms}
                            onClick={this.handleInputChange.bind(null, "tac")}/>
                        </span>
                        <span className="m-l-s">
                          <span>By Clicking, you agree to our </span>
                          <Link to="termsandconditions">
                            Terms and Conditions.
                          </Link>
                        </span>
                      </p>
                      <button className={ClassNames("btn btn-primary", {"disabled" : isUploading})}
                        onClick={this.handleSave}>
                          {uploadProgress ? `${saveBtnValue} (${uploadProgress}%)` : saveBtnValue}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  onChange () {
    this.setState(AddProductStores.getState());
  }

  shippingtype (types, e) {
    AddProductActions.setShippingType(types);
  }

  handleInputChange (type, e) {
    let newProduct = _.merge({}, this.state.product);
    let errors = {};
    let value = e.target.value;

    switch (type) {
      case "brand":
        if (!value) {
          errors.brand = "Brand name is required.";
        } else if (value.length > 50) {
          errors.brand = "Brand name must be less than 50 characters.";
        }
        break;
      case "name":
        if (!value) {
          errors.name = "Product name is required."
        } else if (value.length > 100) {
          errors.name = "Product name must be less than 100 characters.";
        } else {
          AddProductActions.checkName(value);
        }
        break;
      case "description":
        if (!value) {
          errors.description = "Product description is required.";
        } else if (value.length > 500) {
          errors.description = "Product description is limited to 500 characters.";
        }
        break;
      case "quantity":
        value = parseFloat(value);
        if (_.isNaN(value)) {
          errors.quantity = "Product quantity is not a number.";
          value = 0;
        } else if (!value) {
          errors.quantity = "Product quantity must have a positive value.";
        } else if (value > 9999) {
          errors.quantity = "Product quantity must be between 1 and 9999.";
        }
        break;
      case "price":
        if (!value) {
          errors.price = "Product price is required.";
        } else if (value > MAX_PRICE) {
          errors.price = `Product price must not be greater than ₱${numeral(MAX_PRICE).format("0,0.00")}`;
        }
        break;
      case "resell":
        value = parseFloat(value);
        _.ensure(newProduct, "metadata.resellCommission", {
          type : "percentage",
          value : 0
        });

        if (value === NaN || value < 0) {
          errors.resell = "Resell commission must not have a negative value.";
        } else if (newProduct.metadata.resellCommission.type === "percentage" && value >= 100) {
          errors.resell = "You can't give all your income as commission."
        } else if (newProduct.metadata.resellCommission.type === "fixed" && value >= MAX_PRICE) {
          errors.resell = "Resell commission is too big."
        }
        break;
    }

    if (type === "resell") {
      newProduct.metadata.resellCommission.value = value;
    } else if (type === "category") {
      newProduct.category = value;
    } else if (type === "subcategory") {
      const splat = newProduct.category.split(".");
      splat[1] = value;
      newProduct.category = splat.join(".");
    } else if (type === "subcategorySquared") {
      const splat = newProduct.category.split(".");
      splat[2] = value;
      newProduct.category = splat.join(".");
    } else if (type === "tac") {
        return AddProductActions.setTac(!this.state.agreedToTerms);
    } else if (type === "status") {
      newProduct.metadata.status = value;
    } else {
      newProduct = _.merge(newProduct, {
        [type] : value
      });
    }

    if (!_.isEmpty(errors)) {
      AddProductActions.setProduct(newProduct);
      AddProductActions.setFormError(errors);
      return;
    }

    AddProductActions.setProduct(newProduct);
    // AddProductActions.setFormError({});
    ReactTooltip.hide();
  }

  handlePhotoChange (e) {
    const file = e.target.files[0];
    let isPhoto = false;

    if (!file) return;
    isPhoto = file.type && file.type.indexOf("image") !== -1;

    if (!file.size) {
      toastr.error("Invalid file.");
    } else if (file.size > config.uploadLimits.imageSize * 1000 * 1000) {
      toastr.warning("File size exceeds the limit of " + config.uploadLimits.imageSize + " MB.");
    } else if (isPhoto) {
      let {product} = this.state;

      product.metadata.photos = [{
        type : "original",
        file : file
      }];
      AddProductActions.setProduct(product);
    }
  }

  handleTagsChange (tags) {
    let newProduct = this.state.product;
    _.ensure(newProduct, "metadata.tags", []);
    newProduct.metadata.tags = tags;

    AddProductActions.setProduct(newProduct);
  }

  handleCommissionTypeChange (e) {
    let {errors, product} = this.state;
    _.ensure(product, "metadata.resellCommission", {});
    product.metadata.resellCommission = {
      type : e.target.value,
      value : 0
    }

    delete errors.resell;

    AddProductActions.setFormError(errors);
    AddProductActions.setProduct(product);
  }

  onAddLocation (shippingData) {
    let newProduct = this.state.product;

    newProduct.metadata = _.merge(newProduct.metadata, {
      shippingData : shippingData
    });

    this.setState({
      product : newProduct
    });
  }

  handleSave (e) {
    e.preventDefault();
    const {categories} = this.context;
    let {product, agreedToTerms, isUploading} = this.state;
    let errors = {};

    if (isUploading) {
      return;
    }

    _.each(product, (value, key) => {
      switch (key) {
        case "brand":
          product.brand = value.trim();
          if (!value) {
            errors.brand = "Brand name is required.";
          } else if (value.length > 50) {
            errors.brand = "Brand name must be less than 50 characters.";
          }
          break;
        case "name":
          product.name = value.trim();
          if (!value) {
            errors.name = "Product name is required."
          } else if (value.length > 100) {
            errors.name = "Product name must be less than 100 characters.";
          } else if (this.state.errors.name) {
            errors.name = this.state.errors.name;
          }
          break;
        case "description":
          product.description = value.trim();
          if (!value) {
            errors.description = "Product description is required.";
          } else if (value.length > 500) {
            errors.description = "Product description is limited to 500 characters.";
          }
          break;
        case "quantity":
          value = parseFloat(value);
          if (_.isNaN(value)) {
            errors.quantity = "Product quantity is not a number.";
            value = 0;
          } else if (!value) {
            errors.quantity = "Product quantity must have a positive value.";
          } else if (value > 9999) {
            errors.quantity = "Product quantity must be between 1 and 9999.";
          }
          break;
        case "price":
          if (!value) {
            errors.price = "Product price is required.";
          } else if (value > MAX_PRICE) {
            errors.price = `Product price must not be greater than ₱${numeral(MAX_PRICE).format("0,0.00")}`;
          }
          break;
        case "metadata":
          _.each(value, (metaValue, metaKey) => {
            switch (metaKey) {
              case "resellCommission":
                metaValue = parseFloat(metaValue);

                if (metaValue === NaN || metaValue < 0) {
                  errors.resell = "Resell commission must not have a negative value.";
                } else if (product.metadata.resellCommission.type === "percentage" && metaValue >= 100) {
                  errors.resell = "You can't give all your income as commission.";
                } else if (product.metadata.resellCommission.type === "fixed" && metaValue >= MAX_PRICE) {
                  errors.resell = "Resell commission is too big.";
                }
                break;
              case "photos":
                if (!metaValue.length) {
                  errors.photo = "Product photo is required.";
                }
                break;
            }
          });
          break;
        case "category":
          const splat = value.split(".");
          const idx = _.findIndex(categories, {
            slug : splat[0]
          });
          const subcategory = categories[idx];

          if (!value || splat.length < 1) {
            errors.category = "Product category is required."
          } else if (splat.length < 2) {
            errors.subcategory = "Subcategory is required."
          } else if (splat.length === 2) {
            const subcat =  _.filter(subcategory.subcategories, subcat => subcat.groupheader.slug === splat[1])[0];
            if (_.safe(subcat, "groupling.length")) {
              errors.subcategorySquared = "Subcategory is required."
            }
          }
          break;
      }
    });

    if (!_.isEmpty(errors)) {
      return AddProductActions.setFormError(errors);
    } else if (!agreedToTerms) {
      return toastr.error("In order to continue, you must agree to our terms and conditions.");
    }

    //@TODOS need better implementation for editing products
    // this uploads a new cropped image every product update
    if (_.safe(product, "metadata.photos.1.secure_url")) {
      const dataString = $("#image-cropper").cropit("export", {
        type : "image/jpeg",
        quality : .9
      });

      const blob =  Utils.dataURItoBlob(dataString);

      product.metadata.photos[1] = {
        type : "cropped",
        file : blob
      };
    } else {
      const dataString = $("#image-cropper").cropit("export", {
        type : "image/jpeg",
        quality : .9
      });

      const blob =  Utils.dataURItoBlob(dataString);

      product.metadata.photos.push({
        type : "cropped",
        file : blob
      });
    }

    AddProductActions.uploadProduct(product);
  }
}

AddProduct.contextTypes = {
  router : PropTypes.object.isRequired,
  categories : PropTypes.array.isRequired,
};
