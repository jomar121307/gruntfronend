"use strict";

import async from "async";
import {EventEmitter} from "events";
import _   from "underscore";
_.mixin(require('safe-obj'));

import Request from "../../../utils/request.js";
import Cloudinary from "../../../utils/cloudinary.js";
import {register} from "../../dispatcher/AppDispatcher.js";
import AddProductConstants from "./AddProductConstants.js";
import AppStores from "../../stores/AppStores.js";

const initialState = {
  mode : "Add",
  initialized : false,
  invalidShopSettings : true,

  product : {
    brand : "",
    name : "",
    description  : "",
    category : "",
    quantity : 0,
    price : 0,
    metadata : {
      resellCommission : {
        value : 0,
        type : "percentage"
      },
      photos : [],
      tags : [],
      shippingData : [{
        location : "Philippines",
        singleCost : 0,
        multpleCost : 0
      }],
      status : "Available"
    }
  },

  cropperOptions : {
    imageBackground: true,
    imageBackgroundBorderWidth: 15
  },

  isCheckingName : false,

  uploadProgress : 0,
  isUploading : false,

  queriedProduct : {},
  errors : {},
  agreedToTerms : false,

  provinces : AppStores.getInitialState().provinces
};

let state = _.assign({}, initialState);

let Stores = _.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(AddProductConstants.ADDPRODUCT_CHANGE_EVENTS, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(AddProductConstants.ADDPRODUCT_CHANGE_EVENTS, callback);
  },

  emitChange : function () {
    Stores.emit(AddProductConstants.ADDPRODUCT_CHANGE_EVENTS);
  },

  getInitialState : function () {
    return initialState;
  },

  getState : function () {
    return state;
  }
});

const Actions = {
  [AddProductConstants.ADDPRODUCT_INITIALIZE] : function (params) {
    addProductInitialize(params, function (err, data) {
      if (err) {
        return addProductSetState(err, {});
      }

      let newProduct = _.assign({}, initialState.product);
      console.log("1");
      console.log(newProduct);
      let mode = "Add";
      let queriedProduct = {}

      if (params.productId) {

        _.each(initialState.product, function (value, key) {
          console.log("loop");
          console.log(data.product[key]);
          newProduct[key] = data.product[key];
        });
        mode = "Edit";
        queriedProduct = data.product;
      } else if (_.isEmpty(data.shopSettings.paypalInfo) &&
        _.isEmpty(data.shopSettings.eCashInfo)) {
        return addProductSetState(null, {
          initialized : true,
          invalidShopSettings : true
        });
      };

      console.log("xxx addProductSetState xxx");
      console.log(newProduct);
      console.log(mode);
      console.log(queriedProduct);
      addProductSetState(null, {
        initialized : true,
        invalidShopSettings : false,
        product : newProduct,
        mode : mode,
        queriedProduct : queriedProduct
      });
    });
  },

  [AddProductConstants.ADDPRODUCT_RESET] : function (params) {
    addProductSetState(null, initialState);
  },

  [AddProductConstants.ADDPRODUCT_SET_PRODUCT] : function (params) {
    addProductSetState(null, {
      product : params.newProduct
    });
  },

  [AddProductConstants.ADDPRODUCT_SET_FORM_ERROR] : function (params) {
    addProductSetState(null, {
      errors : params.errors
    });
  },

  [AddProductConstants.ADDPRODUCT_SET_COMMISSION_TYPE] : function (params) {
    addProductSetState(null, {
      commissionType : params.type
    })
  },

  [AddProductConstants.ADDPRODUCT_SET_SHIPPING_TYPE] : function (params) {
    addProductSetState(null, {
      shippingType : params.type
    });
  },

  [AddProductConstants.ADDPRODUCT_AGREE_TO_TAC] : function (params) {
    addProductSetState(null, {
      agreedToTerms : params.hasAgreed
    });
  },

  [AddProductConstants.ADDPRODUCT_FINISH] : function (params) {
    if (state.isUploading) {
      return;
    }

    addProductSetState(null, {
      isUploading : true
    });

    addProductUpload({
      product : params.product,
      progressCb : function (percent) {
        addProductSetState(null, {
          uploadProgress : Math.floor(percent)
        });
      }
    }, function (err, data) {
      if (err) {
        return addProductSetState(null, {
          isUploading : false,
          uploadProgress : 0
        });
      }

      addProductSetState(null, {
        uploadProgress : 100,
        isUploading : true
      });

      addProductSetState(null, {
        uploadProgress : 0,
        isUploading : false,
        product : data
      });
    });
  },

  [AddProductConstants.ADDPRODUCT_CHECK_NAME] : function (params) {
    if (!params.name) {
      return;
    }

    addProductSetState(null, {
      isCheckingName : true
    });

    addProductCheckName(params, function (err, data) {
      if (err) {
        return addProductSetState(err, {});
      }

      let errors = _.assign({}, state.errors);
      errors.name = data === "Available" ? "" : "Product name is already taken on your shop.";

      addProductSetState(null, {
        isCheckingName : false,
        errors : errors
      });
    });
  }
};

Stores.dispatchIndex = register(payload => {
  const {type, params} = payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function addProductSetState(err, newState) {
  if (err && typeof err === "string") {
    toastr.error(err)
    console.error(err);
  } else if (_.safe(err, "xhr.responseText")) {
    toastr.error(err.xhr.responseText);
    console.error(err);
  }

  for (var prop in newState) {
    if (state.hasOwnProperty(prop)) {
      state[prop] = newState[prop];
    } else {
      console.warn("Invalid state for addproduct: " + prop);
    }
  }

  Stores.emitChange(AddProductConstants.ADDPRODUCT_CHANGE_EVENTS);
}

function addProductResetState() {
  const newState = _.assign({}, state, initialState);
  addProductSetState(null, newState);
}

function addProductInitialize (options, callback) {
  async.parallel({
    product : function (parallelCb) {
      if (!options.productId) {
        return async.nextTick(parallelCb);
      }
      console.log("xxx parallelCb xxx");
      console.log(parallelCb);
      Request.get(`products/${options.productId}`, parallelCb);
    },

    shopSettings : function (parallelCb) {
      console.log("xxx shopsettings xxx");
      console.log(shopsettings);
      Request.get("shopsettings", parallelCb);
    }
  }, callback)
}

function addProductUpload (options, callback) {
  let newProduct = _.assign({}, options.product);
  console.log("xxx addProductUpload xxx");
  console.log(newProduct);
  async.waterfall([
    function (waterfallCb) {
      let query = {
        name : newProduct.name
      };

      if (state.mode === "Edit") {
        query.id = state.queriedProduct.id;
      }

      Request.get("products/checkname", query, (err, data) => {
        if (err) {
          return waterfallCb(err);
        } else if (data === "Taken") {
          return waterfallCb("Product name is already used in your shop.");
        }

        waterfallCb();
      });
    },
    function (waterfallCb) {
      const original = newProduct.metadata.photos[0];
      if (original.secure_url) {
        return async.nextTick(waterfallCb);
      }

      Cloudinary.uploadImage(original.file,
        percent => {
          console.log("xxx CLOUDINARY UPLOAD IMAGE xxx");
          console.log(percent, state.uploadProgress);
          options.progressCb((percent * 0.4) + state.uploadProgress)
        }, (err, data) => {
          if (err) {
            return waterfallCb(err);
          }

          newProduct.metadata.photos[0] = _.merge(data, {
            photoType : original.type
          });
          waterfallCb();
        });
    },
    function (waterfallCb) {
      const cropped = newProduct.metadata.photos[1];
      Cloudinary.uploadImage(cropped.file,
        percent => {
          console.log(percent, state.uploadProgress);
          options.progressCb((percent * 0.4) + state.uploadProgress)
        }, (err, data) => {
          if (err) {
            return waterfallCb(err);
          }

          newProduct.metadata.photos[1] = _.merge(data, {
            photoType : cropped.type
          });
          waterfallCb();
        });
    },
    function (waterfallCb) {
      if (state.mode === "Add") {
        options.progressCb(10 + state.uploadProgress);
        Request.post("products",
          newProduct,
          function (err, data) {
            waterfallCb(err, data);
          });
        return;
      }

      options.progressCb(35 + state.uploadProgress);
      console.log("xxx REQUEST PUT xxx");
      console.log(`products/${state.queriedProduct.id}`);
      Request.put(`products/${state.queriedProduct.id}`,
        newProduct,
        function (err, data) {
          waterfallCb(err, data);
        });
    },
  ], function (err, waterfallResult) {
    if (err) {
      return callback(err);
    }

    callback(null, waterfallResult);
  });
}

function addProductCheckName (options, callback) {
  let query = {
    name : options.name
  };

  if (state.mode === "Edit") {
    query.id = state.queriedProduct.id;
  }

  Request.get("products/checkname", query, callback);
}
