"use strict";

import React, {Component, PropTypes} from "react";
import {render} from "react-dom";

export default class LoginModal extends Component {
  constructor (props) {
    super(props);

    this.onLogin = this.onLogin.bind(this);
  }

  componentWillReceiveProps (nextProps) {
    if (!this.props.isLoggedIn && nextProps.isLoggedIn) {
      // this.refs["username"].value = "";
      // this.refs["password"].value = "";
    }
  }

  render() {
    return (
      <div className="modal fade" id="loginModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div className="modal-dialog modal-sm" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close">
                  <span aria-hidden="true">
                    &times;
                  </span>
              </button>
              <h4 className="modal-title" id="myModalLabel">Login</h4>
            </div>
            <div className="modal-body">
              <div className="row row-space">
                <div className="col-sm-12 no-pad">
                  <input type="text"
                    id="loginUsername"
                    ref="username"
                    placeholder="Username"
                    className="form-control"/>
                </div>
              </div>
              <div className="row row-space">
                <div className="col-sm-12 no-pad">
                  <input type="password"
                    id="loginPassword"
                    ref="password"
                    placeholder="Password"
                    className="form-control"/>
                </div>
              </div>
              <div className="row row-space">
                <div className="col-sm-12 no-pad">
                  <button type="button"
                    className="btn btn-primary btn-block text-center bold"
                    onClick={this.onLogin.bind(null, "native")}>
                      Login
                  </button>
                </div>
              </div>
              <div className="row row-space">
                <div className="col-sm-12 no-pad">
                  <div className="strike">
                     <span>OR</span>
                  </div>
                </div>
              </div>
              <div className="row row-space">
                <div className="col-sm-12 no-pad">
                  <button type="button"
                    className="btn btn-primary btn-block text-center bold"
                    onClick={this.onLogin.bind(null, "facebook")}>
                    Login with Facebook
                  </button>
                </div>
              </div>
              <div className="row row-space">
                <div className="col-sm-12 no-pad hide">
                  <button type="button"
                    className="btn btn-primary btn-block text-center bold"
                    onClick={this.onLogin.bind(null, "twitter")}>
                    Login with Twitter
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  onLogin (type, e) {
    const {loginUser} = this.props;
    loginUser({
      type,
      username : this.refs["username"].value.trim(),
      password : this.refs["password"].value.trim()
    });
  }
}

LoginModal.propTypes = {
  loginUser : PropTypes.func.isRequired
};
