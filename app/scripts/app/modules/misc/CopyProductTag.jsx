"use strict";

var React = require("react");
var TimerMixin = require("react-timer-mixin");
var CopyToClipboard = require("react-copy-to-clipboard");

var CopyProductTag = React.createClass({
  mixins : [TimerMixin],

  getInitialState : function () {
    return {
      tagCopied : false
    }
  },

  componentDidMount : function () {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
  },

  render : function () {
    var product = this.props.product,
      tagCopied = this.state.tagCopied;
    return (
      <div id="product-tags" className="hide">
        <CopyToClipboard
          text={"#prod-" + product.id + ":{{" + product.name + "}}"}
          onCopy={this._showTag}>
            <span className={"copy-product-tag" + (tagCopied ? "-copied" : "")}>
              {tagCopied ? "Copied!!" : <div className="glyphicon glyphicon-tag"
                                             data-toggle="tooltip"
                                             data-placement="bottom"
                                             title="Copy Tag">
                                        </div> }
            </span>
        </CopyToClipboard>
      </div>
    )
  },

  _showTag : function () {
    var self = this;
    this.setTimeout(function() {
      self.setState({
        tagCopied : false
      });
    }, 500);
    this.setState({
      tagCopied : true
    });
  }
});

module.exports = CopyProductTag;
