"use strict";

import React from "react";
import {render} from "react-dom";
import {Link} from "react-router";
import Tooltip from "rc-tooltip";

import CopyProductTag from "./CopyProductTag.jsx";
import CommissionIcons from "./CommissionIcons.jsx";

export default class ShopItem extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    const blank = "";
    const {product, productPhoto, inOwnShop, className} = this.props;
    let resellBtnClass = null;
    let resellMsg = "\u00a0";
    const hasDeleteBtn = false;
    const hasResellBtn = false;

    if (product.isOwned && product.isResold) {
      resellMsg = "You're reselling this product";
      resellBtnClass = "";
    } else if (product.isResoldByYou) {
      resellMsg = "Already in shop";
      resellBtnClass = "";
    } else if (product.allowResell && !product.isOwned) {
      resellMsg = "Resell in shop";
      resellBtnClass = "resell-btn-container";
    }

    return (
      <div className={className}>
        <div className="highlight-item-div">
          <Link className="text-decor-none" to={"/product/" + _.safe(product, "id")}>
            <img className="img-full"
              src={cloudinary.url(_.safe(product, "metadata.photos.1.public_id", ""), {
                height : 302,
                crop : "scale"
              }) || blank}/>
          </Link>
          <div className="item-desc">
            <p className="item-title">{product.name}</p>
            <Link to={"/shop/" + _.safe(product, "userId.id")}>
              <p className="item-more-desc">
                {_.safe(product, "userId.firstName") + " " + _.safe(product, "userId.lastName" , "")}
              </p>
            </Link>
              <p className="item-price">{_.safe(product, "price")} {_.safe(product, "currencyId.code")}</p>
          </div>
        </div>
      </div>
    )
  }

  _handleAddToShop () {
    var {product} = this.props;
    var {resellerShopId} = product;
    var resellStatus = product.ownerId.id === localStorage.userId && product.isResold;

    if (resellStatus) {
      return;
    }

    if (_.isFunction(this.props.resellToShop) && !resellerShopId) {
      this.props.resellToShop(this.props.product.id);
    }
  }

  _deleteProduct  (productId) {
    var {onDeleteProduct} = this.props;

    if (_.isFunction(onDeleteProduct)) {
      bootbox.confirm("You can not undo this action. Are you sure you want to proceed?",
        function (choice) {
          if (choice) {
            onDeleteProduct(productId);
          }
      });
    }
  }
}
