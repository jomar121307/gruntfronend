import React, {Component, PropTypes} from 'react';
import {Link} from "react-router";
import ClassNames from "classnames";
import Utils from "../../../utils/utils.js";
import {PulseLoader} from 'halogen';

export default class LoginRegisterModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab : "login",
      type : "",
      loginErrors : {},
      signupErrors : {},
      registrationErrors: {}
    };

    this.selectedTab = this.selectedTab.bind(this);
    this.onLogin = this.onLogin.bind(this);
    this.onRegister = this.onRegister.bind(this);
    this.onResetPassword = this.onResetPassword.bind(this);



    this.sendToVerify = this.sendToVerify.bind(this);
    this.onSocialRegister = this.onSocialRegister.bind(this);
    this.keyNumeric = this.keyNumeric.bind(this);

  }

  componentWillReceiveProps (nextProps) {
    const {type} = this.state;

    if (!this.props.isLoggedIn && nextProps.isLoggedIn && type === "native") {
      this.refs["loginPassword"].value = "";
    } else if (!_.isEmpty(nextProps.signupErrors)) {
      this.setState({
        signupErrors : _.merge({}, nextProps.signupErrors)
      });
    }
  }

  render() {

    const {
      selectedTab
    } = this.state;
    return (<div>
      <div className="modal fade"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="myModalLabel"
        id="LoginRegister-body">
          <div className="modal-dialog modal-lg" role="document">
            <div className="modal-content">
              <div className="modal-header no-pad">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
               </div>
              <div className="modal-body">
                <div className="row">
                  <div className="col-sm-8 no-pad-right-i">
                    <div id="carousel-example-generic" className="carousel slide carousel-fade" data-ride="carousel">
                      <ol className="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" className="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                      </ol>
                      <div className="carousel-inner" role="listbox">
                        <div className="item active">
                          <img src="http://res.cloudinary.com/ogagtep/image/upload/c_fill,h_338/v1459833765/cover-1_ryvne4.jpg"
                            className="crsl-img" />
                          <div className="carousel-caption">
                            <span className="fa fa-thumbs-o-up crsl-icon"></span>
                            <h2>Easy</h2>
                            <p>Time for a sharp Easy Shopping.</p>
                          </div>
                        </div>
                        <div className="item">
                          <img src="http://res.cloudinary.com/ogagtep/image/upload/c_fill,h_338/v1459833783/cover-2_sqbdo2.jpg"
                             className="crsl-img" />
                          <div className="carousel-caption">
                            <span className="fa fa-usd crsl-icon"></span>
                            <h2>Save</h2>
                            <p>Bargain, you{"'"}ve got it!</p>
                          </div>
                        </div>
                        <div className="item">
                          <img src="http://res.cloudinary.com/ogagtep/image/upload/c_fill,h_338/v1459833767/cover-3_uejjkh.jpg"
                            className="crsl-img" />
                          <div className="carousel-caption">
                            <span className="fa fa-paper-plane crsl-icon"></span>
                            <h2>Reliable</h2>
                            <p>You{"'"}re assured the product is delivered!</p>
                          </div>
                        </div>
                      </div>
                      <a className="left carousel-control"
                        href="#carousel-example-generic"
                        role="button"
                        data-slide="prev">
                          <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          <span className="sr-only">Previous</span>
                      </a>
                      <a className="right carousel-control"
                        href="#carousel-example-generic"
                        role="button"
                        data-slide="next">
                          <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          <span className="sr-only">Next</span>
                      </a>
                    </div>
                  </div>

                  <div className="col-sm-4 no-pad-left-i">
                    <ul className="nav nav-tabs" id="LoginSignupTab1">
                      <li className={ClassNames("pointer",
                        {active : selectedTab === "login"})}
                        onClick={this.selectedTab.bind(null, "login")}>
                          <a>Login</a>
                      </li>
                      <li className={ClassNames("pointer",
                        {active : selectedTab === "signup"})}
                        onClick={this.selectedTab.bind(null, "signup")}>
                          <a>Signup</a>
                      </li>
                    </ul>
                    <div className="tab-content">
                      <div className="tab-pane active" id="LoginSignupTab2">
                        {selectedTab === "login" ? this.renderLoginTab() : this.renderSignupTab()}
                      </div>
                      <div className="tab-pane active hide" id="SocialRegistrationTab">
                        {this.renderSocialRegistrationTab()}
                      </div>
                    </div>
                  </div>



                </div>
              </div>
            </div>
          </div>
      </div>
      <div className="modal fade"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="myModalLabel"
        id="LoginRegister-body2" data-backdrop="static" data-keyboard="false">

        <div className="row text-center" style={{paddingTop : "250px"}}>
          <PulseLoader color="#26A65B" size="36px" margin="4px"/>
        </div>

        </div>
      </div>
    );
  }

  selectedTab(tab, e){
    e.preventDefault();
    const {selectedTab} = this.state;

    $("#LoginSignupTab1").show();
    $("#LoginSignupTab2").show();

    $("#SocialRegistrationTab").hide();

    if (selectedTab === tab) return;

    this.setState({
      selectedTab : tab
    });
  }

  sendToVerify (regType, e) {
    e.preventDefault();
    const {sendToVerify} = this.state;
    let {registrationErrors} = this.state;

    if (regType=="mobile") {

      $.ajax({ 
        type: 'POST', 
        url: 'https://resourceapi.globalpinoyremittance.com/ups_registration_service/check_mobile_number', 
        data: { 'mobile_number': this.refs["socialregistrationMobileNumber"].value.trim() }, 
        dataType: 'json',
        success: function (result) {
        }
      });
    }
    else if (regType=="email") {

      $.ajax({ 
        type: 'POST', 
        url: 'https://resourceapi.globalpinoyremittance.com/ups_registration_service/check_email_address', 
        data: { 'email_address': this.refs["socialregistrationEmail"].value.trim() }, 
        dataType: 'json',
        success: function (result) {
        }
      });
    }

  }

  renderLoginTab () {
    const {loginErrors} = this.state;

    return (
      <div className="p-s">
          <div className={ClassNames("row row-space form-group", {"has-error" : loginErrors.loginEmail})}>
            <div className="col-sm-12 no-pad">
              <input type="text"
                key={_.random(9999)}
                className={ClassNames("form-control", {"input-has-error" : loginErrors.loginEmail})}
                ref="loginEmail"
                placeholder="Username/Email"/>
            </div>
          </div>
          <div className={ClassNames("row row-space form-group", {"has-error" : loginErrors.loginPassword})}>
            <div className="col-sm-12 no-pad">
              <input type="password"
                key={_.random(9999)}
                ref="loginPassword"
                className={ClassNames("form-control", {"input-has-error" : loginErrors.loginPassword})}
                placeholder="Password"/>
            </div>
          </div>
          <div className="row row-space">
            <div className="col-sm-6 no-pad-left">
              <button className="btn btn-primary btn-block text-center bold"
                onClick={this.onLogin.bind(null, "native")}>
                  Login
              </button>
            </div>
            <div className="col-sm-6 no-pad-right">
              <button className="btn btn-primary btn-block btn-ups text-center bold"
                onClick={this.onLogin.bind(null, "ups")}>
                  Login with Ups
              </button>
            </div>
          </div>
        <div className="row row-space">
          <div className="col-sm-12 no-pad">
            <div className="strike">
               <span>OR</span>
            </div>
          </div>
        </div>
        <div className="row row-space">
          <div className="col-sm-12 no-pad">
            <button type="button"
              className="btn btn-facebook btn-block text-center bold"
              onClick={this.onLogin.bind(null, "facebook")}>
              Login with Facebook
            </button>
          </div>
        </div>
        <div className="row row-space">
          <div className="col-sm-12 no-pad text-center">
            <a className="pointer" onClick={this.onResetPassword}>
              Forgot password?
            </a>
          </div>
        </div>
        <div className="row row-space hide">
          <div className="col-sm-12 no-pad">
            <button type="button"
              className="btn btn-twitter btn-block text-center bold"
              onClick={this.onLogin.bind(null, "twitter")}>
              Login with Twitter
            </button>
          </div>
        </div>
      </div>
    );
  }

  renderSignupTab () {
    const {signupErrors} = this.state;

    return (
      <div className="p-s">
        <form onSubmit={this.onRegister} >
          <div className={ClassNames("row row-space form-group", {"has-error" : signupErrors.signupFirstName})}>
            <div className="col-sm-12 no-pad">
              <input type="text"
                maxLength="50"
                ref="signupFirstName"
                className={ClassNames("form-control", {"input-has-error" : signupErrors.signupFirstName})}
                placeholder={signupErrors.signupFirstName || "First Name"}/>
              <label className="control-label pull-left" style={{fontSize : "0.9em"}}>
              </label>
            </div>
          </div>
          <div className={ClassNames("row row-space form-group", {"has-error" : signupErrors.signupLastName})}>
            <div className="col-sm-12 no-pad">
              <input type="text"
                maxLength="50"
                ref="signupLastName"
                className={ClassNames("form-control", {"input-has-error" : signupErrors.signupLastName})}
                placeholder={signupErrors.signupLastName || "Last Name"} />
            </div>
          </div>
          <div className={ClassNames("row row-space form-group", {"has-error" : signupErrors.email})}>
            <div className="col-sm-12 no-pad">
              <input type="text"
                maxLength="50"
                ref="signupEmail"
                className={ClassNames("form-control", {"input-has-error" : signupErrors.email})}
                placeholder={signupErrors.email || "Email"} />
            </div>
          </div>
          <div className={ClassNames("row row-space form-group", {"has-error" : signupErrors.signupPassword})}>
            <div className="col-sm-12 no-pad">
              <input type="password"
                maxLength="50"
                ref="signupPassword"
                className={ClassNames("form-control", {"input-has-error" : signupErrors.signupPassword})}
                placeholder={signupErrors.signupPassword || "Password"} />
            </div>
          </div>
          <div className={ClassNames("row row-space form-group", {"has-error" : signupErrors.signupConfirmPassword})}>
            <div className="col-sm-12 no-pad">
              <input type="password"
                maxLength="50"
                ref="signupConfirmPassword"
                className={ClassNames("form-control", {"input-has-error" : signupErrors.signupConfirmPassword})}
                placeholder={signupErrors.signupConfirmPassword || "Confirm Password"} />
            </div>
          </div>
          <div className="row row-space">
            <div className="col-sm-12 no-pad">
              <p>By clicking register, you agree to our <Link to="/termsandconditions" target="_blank">Terms and Conditions</Link></p>
              <button type="submit" className="btn btn-primary btn-block text-center bold">
                  Register
              </button>
              <p className="m-t-s hide"><input type="checkbox" className="newsletter-checkbox" />Newsletter Sign Up</p>
            </div>
          </div>
        </form>
        <div className="row row-space hide">
          <div className="col-sm-12 no-pad">
            <div className="strike">
               <span>OR</span>
            </div>
          </div>
        </div>
        <div className="row row-space hide">
          <div className="col-sm-12 no-pad">
            <button type="button"
              className="btn btn-facebook btn-block text-center bold"
              onClick={this.onLogin.bind(null, "facebook")}>
              Register with Facebook
            </button>
          </div>
        </div>
        <div className="row row-space hide">
          <div className="col-sm-12 no-pad">
            <button type="button"
              className="btn btn-twitter btn-block text-center bold"
              onClick={this.onLogin.bind(null, "twitter")}>
              Register with Twitter
            </button>
          </div>
        </div>
      </div>
    );
  }



  renderSocialRegistrationTab () {
    const {registrationErrors} = this.state;

    return (
      <div className="p-s">
        <form onSubmit={this.onSocialRegister} >
          <div className="col-sm-12 no-pad"><h4>{config.appname} Social Registration</h4></div>



          <div className={ClassNames("row row-space form-group", {"has-error" : registrationErrors.socialregistrationCountry})}>
            <div className="col-sm-12 no-pad">
                <select
                className={ClassNames("form-control", {"input-has-error" : registrationErrors.socialregistrationCountry})}
                ref="socialregistrationCountry"
                >
                  <option value=""> Select Country</option> 
                  <option value="PH"> Philippines</option>
                  <option value="ALL"> Others </option>
                </select>
              <label className="control-label pull-left" style={{fontSize : "0.9em"}}>
              </label>
            </div>
          </div>


          <div className={ClassNames("row row-space form-group", {"has-error" : registrationErrors.socialregistrationMobileNumber})}>
            <div className="col-sm-9 no-pad">
              <input type="text"
                maxLength="11"
                ref="socialregistrationMobileNumber"
                className={ClassNames("form-control", {"input-has-error" : registrationErrors.socialregistrationMobileNumber})}
                placeholder={registrationErrors.socialregistrationMobileNumber || "11-Digit Mobile Number"} />

            </div>
            <div className="col-sm-3">
              <button type="submit" 
              className="btn btn-facebook btn-block text-center bold"
              onClick={this.sendToVerify.bind(null, "mobile")}>
                  Send
              </button>
            </div>
          </div>
          <div className={ClassNames("row row-space form-group", {"has-error" : registrationErrors.socialregistrationPIN1})}>
            <div className="col-sm-5 no-pad">
              <input type="React.PropTypes.number"
                maxLength="4"
                ref="socialregistrationPIN1"
                className={ClassNames("form-control", {"input-has-error" : registrationErrors.socialregistrationPIN1})}
                placeholder={registrationErrors.socialregistrationPIN1 || "PIN (Mobile)"} />

            </div>
            <div className="col-sm-3">
              <button type="submit" className="glyphicon glyphicon-ok">
              </button>
            </div>
          </div>

          <div className={ClassNames("row row-space form-group", {"has-error" : registrationErrors.socialregistrationEmail})}>
            <div className="col-sm-9 no-pad">
              <input type="text"
                maxLength="100"
                ref="socialregistrationEmail"
                className={ClassNames("form-control", {"input-has-error" : registrationErrors.socialregistrationEmail})}
                placeholder={registrationErrors.socialregistrationEmail || "Email Address"} />

            </div>
            <div className="col-sm-3">
              <button type="submit" 
              className="btn btn-facebook btn-block text-center bold"
              onClick={this.sendToVerify.bind(null, "email")}>
                  Send
              </button>
            </div>
          </div>
          <div className={ClassNames("row row-space form-group", {"has-error" : registrationErrors.socialregistrationPIN2})}>
            <div className="col-sm-5 no-pad">
              <input type="text"
                maxLength="4"
                ref="socialregistrationPIN2"
                className={ClassNames("form-control", {"input-has-error" : registrationErrors.socialregistrationPIN2})}
                placeholder={registrationErrors.socialregistrationPIN2 || "PIN (Email)"} />

            </div>
            <div className="col-sm-3">
              <button type="submit" className="glyphicon glyphicon-ok">
              </button>
            </div>
          </div>

          <div className={ClassNames("row row-space form-group", {"has-error" : registrationErrors.socialregistrationPassword})}>
            <div className="col-sm-12 no-pad">
              <input type="password"
                maxLength="8"
                ref="socialregistrationPassword"
                className={ClassNames("form-control", {"input-has-error" : registrationErrors.socialregistrationPassword})}
                placeholder={registrationErrors.socialregistrationPassword || "Password"} />
            </div>
          </div>

          <div className={ClassNames("row row-space form-group", {"has-error" : registrationErrors.socialregistrationConfirmPassword})}>
            <div className="col-sm-12 no-pad">
              <input type="password"
                maxLength="8"
                ref="socialregistrationConfirmPassword"
                className={ClassNames("form-control", {"input-has-error" : registrationErrors.socialregistrationConfirmPassword})}
                placeholder={registrationErrors.socialregistrationConfirmPassword || "Confirm Password"} />
            </div>
          </div>

          <div className="row row-space">
            <div className="col-sm-12 no-pad">
              <p>By clicking confirm registration, you agree to our <Link to="/termsandconditions" target="_blank">Terms and Conditions</Link></p>
              <p className="m-t-s hide"><input type="checkbox" className="newsletter-checkbox" />Newsletter Sign Up</p>
            </div>
            <div className="col-sm-4">
              <button type="submit" onClick={this.selectedTab.bind(null, "login")}>
              <span className="glyphicon glyphicon-arrow-left">
              </span>
                  Back
              </button>
            </div>
            <div className="col-sm-8 no-pad">
              <button type="submit" className="btn btn-primary btn-block text-center bold">
                  Confirm Registration
              </button>
            </div>
          </div>
        </form>
        
      </div>
    );
  }

  onLogin (type, e) {
    e.preventDefault();

    const {loginUser} = this.props;
    let {loginErrors} = this.state;

    if (type === "facebook") {
      loginUser({
        type,
        userEmail : "",
        password : ""
      });
      //toastr.error($("#LoginSignupTab").html());
      $("#LoginSignupTab1").hide();
      $("#LoginSignupTab2").hide();
      $("#SocialRegistrationTab").show();
      $("#SocialRegistrationTab").attr("class","tab-pane active");

      //$("#renderSocialRegistrationTab").modal("show");
      //this.renderSocialRegistrationTab();

      //this.selectedTab.bind(null, "socialregistration")
      return;
    }

    _.each(this.refs, (ref, key) => {
      const value = ref.value.trim();

      switch (key) {
        case "loginEmail":
          if (!value) {
            loginErrors.loginEmail = "Username is required.";
          } else {
            delete loginErrors.loginEmail
          }

          break;
        case "loginPassword":
          if (!value) {
            loginErrors.loginPassword = "Password is required.";
          } else {
            delete loginErrors.loginPassword;
          }

          break;
      }
    });

    if (!_.isEmpty(loginErrors)) {
      return this.setState({
        loginErrors : loginErrors
      });
    }

    this.setState({
      type : type
    });

    loginUser({
      type,
      userEmail : _.safe(this ,"refs.loginEmail.value", "").trim(),
      password : _.safe(this ,"refs.loginPassword.value", "").trim()
    });
  }

  onRegister (e) {
    e.preventDefault();

    const {registerUser} = this.props;
    let {signupErrors} = this.state;
    const {registrationErrors} = this.state;

    _.each(this.refs, (ref, key) => {
      const value = ref.value.trim();
      switch (key) {
        case "signupFirstName":
          if (!value) {
            signupErrors.signupFirstName = "First name is required.";
          } else {
            delete signupErrors.signupFirstName;
          }
          break;
        case "signupLastName":
          if (!value) {
            signupErrors.signupLastName = "Last name is required.";
          } else {
            delete signupErrors.signupLastName;
          }
          break;
        case "signupEmail":
          if (!value) {
            signupErrors.email = "Email address is required.";
          } else if (!config.emailRegex.test(value)) {
            signupErrors.email = "Email address is invalid."
            this.refs["signupEmail"].value = "";
          } else {
            delete signupErrors.email;
          }
          break;
        case "signupPassword":
          if (!value) {
            signupErrors.signupPassword = "Password is required.";
          } else if (value.length < 8) {
            signupErrors.signupPassword = "Password must be atleast 8 characters."
          } else {
            delete signupErrors.signupPassword;
          }
          break;
        case "signupConfirmPassword":
          const password = this.refs["signupPassword"].value.trim();
          if (!value) {
            signupErrors.signupConfirmPassword = "Password is required.";
          } else if (value.length < 8) {
            signupErrors.signupPassword = "Password must be atleast 8 characters."
            signupErrors.signupConfirmPassword = "Password must be atleast 8 characters."

            this.refs["signupPassword"].value = "";
            this.refs["signupConfirmPassword"].value = "";
          } else if (password !== value) {
            signupErrors.signupConfirmPassword = "Passwords don't match.";
            signupErrors.signupPassword = "Passwords don't match.";

            this.refs["signupPassword"].value = "";
            this.refs["signupConfirmPassword"].value = "";
          } else {
            delete signupErrors.signupPassword;
            delete signupErrors.signupConfirmPassword;
          }
          break;
      }
    });


    if (!_.isEmpty(signupErrors)) {
      return this.setState({
        signupErrors : signupErrors
      });
    }

    registerUser({
      firstName : this.refs["signupFirstName"].value.trim(),
      lastName : this.refs["signupLastName"].value.trim(),
      email : this.refs["signupEmail"].value.trim(),
      password : this.refs["signupPassword"].value.trim(),
      confirmPassword : this.refs["signupPassword"].value.trim()
    }, () => {
      _.each(this.refs, (r, k) => {
        r.value = "";
      });
    });
  }

  onSocialRegister(e) {
    e.preventDefault();

    const {socialregisterUser} = this.props;
    let {registrationErrors} = this.state;

    _.each(this.refs, (ref, key) => {
      const value = ref.value.trim();
      switch (key) {
        case "socialregistrationCountry":
          if (!value) {
            registrationErrors.socialregistrationCountry = "Select Country???";
          } else {
            delete registrationErrors.socialregistrationCountry;
          }
          break;
        case "socialregistrationMobileNumber":
          if (!value) {
            registrationErrors.socialregistrationMobileNumber = "Mobile number is required.";
          } else {
            delete registrationErrors.socialregistrationMobileNumber;
          }
          break;
        case "socialregistrationEmail":
          if (!value) {
            registrationErrors.socialregistrationEmail = "Email address is required.";
          } else if (!config.emailRegex.test(value)) {
            registrationErrors.socialregistrationEmail = "Email address is invalid."
            this.refs["socialregistrationEmail"].value = "";
          } else {
            delete registrationErrors.socialregistrationEmail;
          }
          break;
        case "socialregistrationPIN1":
          if (!value) {
            registrationErrors.socialregistrationPIN1 = "PIN required.";
          } else {
            delete registrationErrors.socialregistrationPIN1;
          }
          break;
        case "socialregistrationPIN2":
          if (!value) {
            registrationErrors.socialregistrationPIN2 = "PIN required.";
          } else {
            delete registrationErrors.socialregistrationPIN2;
          }
          break;
        case "socialregistrationPassword":
          if (!value) {
            registrationErrors.socialregistrationPassword = "Password is required.";
          } else if (value.length < 8) {
            registrationErrors.socialregistrationPassword = "Password must be atleast 8 characters."
          } else {
            delete registrationErrors.socialregistrationPassword;
          }
          break;
        case "socialregistrationConfirmPassword":
          const password = this.refs["socialregistrationPassword"].value.trim();
          if (!value) {
            registrationErrors.socialregistrationConfirmPassword = "Password is required.";
          } else if (value.length < 8) {
            registrationErrors.socialregistrationPassword = "Password must be atleast 8 characters."
            registrationErrors.socialregistrationConfirmPassword = "Password must be atleast 8 characters."

            this.refs["socialregistrationPassword"].value = "";
            this.refs["socialregistrationConfirmPassword"].value = "";
          } else if (password !== value) {
            registrationErrors.socialregistrationConfirmPassword = "Passwords don't match.";
            registrationErrors.socialregistrationPassword = "Passwords don't match.";

            this.refs["socialregistrationPassword"].value = "";
            this.refs["socialregistrationConfirmPassword"].value = "";
          } else {
            delete registrationErrors.socialregistrationPassword;
            delete registrationErrors.socialregistrationConfirmPassword;
          }
          break;
        
      }
    });

    if (!_.isEmpty(registrationErrors)) {
      return this.setState({
        registrationErrors : registrationErrors
      });
    }

    socialregisterUser({
      mobileNumber : this.refs["socialregistrationMobileNumber"].value.trim(),
    }, () => {
      _.each(this.refs, (r, k) => {
        r.value = "";
      });
    });
  }

  keyNumeric (e) {
    e.preventDefault();

}

  onResetPassword (e) {
    e.preventDefault();
    $("#LoginRegister-body").modal("hide");
    this.context.router.push("/resetpassword");
  }
}

LoginRegisterModal.contextTypes = {
  router : PropTypes.object.isRequired
};
