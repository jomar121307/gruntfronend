"use strict";

import React, {Component, PropTypes} from "react";
import {render} from "react-dom";
import {Link} from "react-router";
import Rating from "react-rating";

//components
import Loader from "../../views/Loader.jsx";
import Carousel from "../misc/Carousel.jsx";
import Reviews from "./components/Reviews.jsx";

import ProductActions from "./ProductActions.js";
import ProductStores from "./ProductStores.js";

export default class Product extends Component {
  constructor (props){
    super(props);
    this.state = ProductStores.getInitialState();

    this.renderAddToCartBtn = this.renderAddToCartBtn.bind(this);
    this.onChange = this.onChange.bind(this);
    this.handeAddToCart = this.handeAddToCart.bind(this);
    this.handleProductShare = this.handleProductShare.bind(this);
  }

  componentWillMount () {
    ProductStores.addChangeListener(this.onChange);
    ProductActions.initialize(this.props.params.id);
  }

  componentWillUpdate (nextProps, nextState) {
    if (nextState.addedToCart) {
      this.context.router.push("/cart");
    }
  }

  componentWillUnmount () {
    ProductActions.reset();
    ProductStores.removeChangeListener(this.onChange);
  }

  render () {
    const {initialized, product} = this.state;
    const category = _.safe(product, "category", "").split(".");

    if (!initialized) {
      return <Loader colStyle={{paddingTop : "50px"}}/>
    }

    return (
      <div className="brand-container m-t-s" id="selecteditem-container">
        <div className="container">
          <div className="div-container m-b-s" id="search-container">
            <div className="row">
              <div className="col-sm-12">
                <input className="form-control" id="search-bar" placeholder="Search Products, Brands, Categories"/>
              </div>
            </div>
          </div>
          <div className="div-container m-b-s">
            <div className="row">
              <div className="col-sm-12">
                <h3>
                  <span>{_.safe(product, "name")}</span>
                  <span className="pull-right breadcrumbs">
                    {category.map(this.listCategory)}
                  </span>
                </h3>
                <hr/>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-5">
                <img className="img-responsive f-w"
                  src={_.safe(product, "metadata.photos.1.secure_url", "images/login-product-3.jpg")}/>
              </div>
              <div className="col-sm-7">
                <p className="shop-price fs-14 bold">
                  <span className="m-r-xs">Price:</span>
                  <span className="text-success">{_.safe(product, "price")} {_.safe(product, "currencyId.code", "")}</span>
                </p>
                <p className="fs-14">
                  <span className="m-r-xs bold">Available:</span>
                  {_.safe(product, "quantity", 0)} {_.safe(product, "quantity", 0) > 1 ? "pcs." : "pc."}
                </p>
                <p>
                  <span className="shop-product-owner bold">Store:
                    <Link to={"/shop/" + _.safe(product, "userId.id")}>
                      {_.safe(product, "userId.firstName") + " " + _.safe(product, "userId.lastName", "")}
                    </Link>
                  </span>
                </p>
                {(product.userId.location.country) ?
                  <p>
                    <span className="bold m-r-xs">Location:</span>
                    <span>{_.safe(product, "userId.location.country")}</span>
                  </p> : ""
                }
                <a className="btn btn-block btn-social btn-facebook"
                style={{width : "110px"}}
                  onClick={this.handleProductShare}>
                  <span className="fa fa-facebook"></span> Share
                </a>
                <Rating initialRate={3} empty='fa fa-star-o fa-2x rating-empty hide' full='fa fa-star fa-2x rating-full'/>
                <hr className="chisel-gray-line"/>
                <div className="item-detail-description m-b-s">
                  {_.safe(product, "description", "No description available.")}
                </div>
                {this.renderAddToCartBtn(product.id, product.__self)}
                {this.renderEditBtn(product.id, product.__self)}
                <hr className="chisel-gray-line"/>
                <div className="row">
                  <div className="col-xs-12">
                    <Reviews reviews={_.safe(product, "reviews", [])}
                      isReviewed={product.isReviewed}
                      uploadReview={ProductActions.uploadReview}
                      removeReview={ProductActions.removeReview}/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderEditBtn (id, isOwner) {
    if (!isOwner) {
      return <div/>
    }

    return (
      <Link to={`/product/${id}/edit`}>
        <button className="btn btn-default" style={{marginLeft : "5px"}}>
          <span>Edit Item</span>
        </button>
      </Link>
    )
  }

  renderAddToCartBtn (id, isOwner) {
    if (isOwner) {
      return <div/>
    }

    return (
      <button className="btn btn-success btn-pg-green" onClick={this.handeAddToCart}>
        <span className="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
        <span>Add to Cart</span>
      </button>
    )
  }

  onChange () {
    this.setState(ProductStores.getState());
  }

  listCategory (category, idx, list) {
    return (
      <span key={category}>
        <Link to="/" >
          {category}
        </Link>
        {idx === list.length - 1 ? "" : " / "}
      </span>
    )
  }

  handeAddToCart (e) {
    e.preventDefault();
    const {isLoggedIn} = this.context;
    const {product} = this.state;

    ProductActions.addToCart(isLoggedIn);
  }

  handleProductShare (e) {
    e.preventDefault()
    const {product} = this.state;

    FB.ui({
      method: 'feed',
      appId: fbAppId,
      link: window.location.href,
      redirect_uri: window.location.href,
      picture: _.safe(product, "metadata.photos.1.secure_url", "images/login-product-3.jpg"),
      name: product.name,
      description: product.description,
    }, function (response) {
      if (response && !response.error_code) {
        console.log('Posting completed.');
      } else {
        console.error('Error while posting.');
      }
    });
  }
}

Product.contextTypes = {
  router : PropTypes.object.isRequired,
  isLoggedIn : PropTypes.bool.isRequired
};
