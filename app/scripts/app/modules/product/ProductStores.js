"use strict";

import async from "async";
import {EventEmitter} from "events";
import _ from "underscore";

import Request from "../../../utils/request.js";
import {register} from "../../dispatcher/AppDispatcher.js";

import ProductConstants from "./ProductConstants.js";

const initialState = {
  initialized : false,
  product : {},
  addedToCart : false,
  isLoadingMoreReviews : false
}

let state = _.assign({}, initialState);

let Stores = _.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(ProductConstants.PRODUCT_CHANGE_EVENTS, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(ProductConstants.PRODUCT_CHANGE_EVENTS, callback);
  },

  _emitChange : function () {
    Stores.emit(ProductConstants.PRODUCT_CHANGE_EVENTS);
  },

  getInitialState : function () {
    return initialState;
  },

  getState : function () {
    return state;
  }
});

const Actions = {
  [ProductConstants.PRODUCT_INITIALIZE] : function (params) {
    productInitialize(params, function (err, data) {
      if (err) {
        return productSetState(err, {
          initialized : true
        });
      }

      productSetState(null, {
        initialized : true,
        product : data.product
      });
    });
  },

  [ProductConstants.PRODUCT_RESET] : function (params) {
    productResetState();
  },

  [ProductConstants.PRODUCT_UPLOAD_REVIEW] : function (params) {
    // @TODOS implement optimistic adding/updating.
    productUploadReview(params, function (err, data) {
      if (err) {
        productSetState(err, {});
        return;
      }
    });
  },

  [ProductConstants.PRODUCT_REMOVE_REVIEW] : function (params) {
    const reviews = state.reviews;
    const removed = reviews.splice(params.index, 1);
    productSetState(null, {
      reviews : reviews
    });

    productRemoveReview(params, function (err, data) {
      if (err) {
        const reviews2 = state.reviews;
        reviews2.splice(params.index, 0, removed);
        productSetState(err, {
          reviews : reviews2
        });
        return;
      }
    });
  },

  [ProductConstants.PRODUCT_ADD_TO_CART] : function (params) {
    if (!state.initialized) {
      return;
    } else if (!state.product.quantity) {
      return toastr.warning("Out of stock.");
    }


    productAddToCart(params, function (err, data) {
      if (err) {
        return console.error(err);
      }

      productSetState(null, {
        addedToCart : true
      });
    });
  }
};

Stores.dispatchIndex = register(payload => {
  const {type, params} = payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function productSetState (err, obj) {
  if (err) {
    toastr.error(err.xhr.responseText);
    console.error(err);
  }

  for (var prop in obj) {
    if (state.hasOwnProperty(prop)) {
      state[prop] = obj[prop];
    } else {
      console.warn("Invalid state for product: " + prop);
    }
  }

  Stores._emitChange(ProductConstants.PRODUCT_CHANGE_EVENTS);
}

function productResetState () {
  productSetState(null, initialState);
}

/**
 * Initialize the product view. Fetch product and its reviews.
 *
 * @method     productInitialize
 * @param      {<type>}    options   { description }
 * @param      {Function}  callback  { description }
 */
function productInitialize (options, callback) {
  async.parallel({
    product : function (parallelCb) {
      Request.get("products/" + options.productId, {
        reviews : true
      }, parallelCb);
    }
  }, callback);
}

function productAddToCart(options, callback) {
  if (options.isLoggedIn) {
    Request.post("cart", {
      productId : state.product.id,
      quantity : 1
    }, callback);
    return;
  }

  let items = [];

  try {
    items = JSON.parse(localStorage.__aitc || "[]");
  } catch (e) {
    return console.error(e);
  }

  const idx = _.findIndex(items, {
    __id : state.product.id
  });

  if (idx !== -1 && state.product.quantity <= items[idx].__c) {
    return;
  } else if (idx !== -1) {
    items[idx].__c++;
  } else {
    items.push({
      __id : state.product.id,
      __c : 1
    });
  }

  localStorage.__aitc = JSON.stringify(items);
  callback();
}

function productUploadReview (options, callback) {
  Request.post("reviews", {
    productId : state.product.id,
    content : options.review,
    rating : "1.00"
  }, callback);
}
