"use strict";

import underscore from "underscore";

import AppDispatcher from "../../dispatcher/AppDispatcher.js";
import ProductConstants from "./ProductConstants.js";

const ProductActions = {
  initialize : function (productId) {
    AppDispatcher.handleViewAction({
      type : ProductConstants.PRODUCT_INITIALIZE,
      params : {
        productId
      }
    });
  },

  reset : function () {
    AppDispatcher.handleViewAction({
      type : ProductConstants.PRODUCT_RESET
    });
  },

  uploadReview : function (review, isUpdate) {
    AppDispatcher.handleViewAction({
      type : ProductConstants.PRODUCT_UPLOAD_REVIEW,
      params : {
        review,
        isUpdate
      }
    });
  },

  removeReview : function (reviewId, index) {
    AppDispatcher.handleViewAction({
      type : ProductConstants.PRODUCT_REMOVE_REVIEW,
      params : {
        reviewId,
        index
      }
    });
  },

  addToCart : underscore.throttle(function (isLoggedIn) {
    AppDispatcher.handleViewAction({
      type : ProductConstants.PRODUCT_ADD_TO_CART,
      params : {
        isLoggedIn
      }
    });
  }, 500, {trailing : false})
}

export default ProductActions;
