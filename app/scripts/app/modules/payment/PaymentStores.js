"use strict";

import {EventEmitter} from "events";
import _ from "underscore";

import Request from "../../../utils/request.js";
import {register} from "../../dispatcher/AppDispatcher.js";

import PaymentConstants from "./PaymentConstants.js";

const initialState = {
  initialized : false,
  items : [],
  address : {},
  proceedToPaypal : false,
  proceedToPurchases : false,
  paypalURI : "",
  paymentPlatform : "",
  isProcessingPayment : false
};

let state = _.assign({}, initialState);

let Stores = _.assign({}, EventEmitter.prototype, {
  addChangeListener : function (callback) {
    this.on(PaymentConstants.PAYMENT_CHANGE_EVENTS, callback);
  },

  removeChangeListener : function (callback) {
    this.removeListener(PaymentConstants.PAYMENT_CHANGE_EVENTS, callback);
  },

  emitChange : function () {
    Stores.emit(PaymentConstants.PAYMENT_CHANGE_EVENTS);
  },

  getState : function () {
    return state;
  },

  getInitialState : function () {
    return initialState;
  }
});

const Actions = {
  [PaymentConstants.PAYMENT_INITIALIZE] : function (params) {
    paymentInitialize(function (err, data) {
      if (err) {
        return paymentSetState(err, {
          initialized : true
        });
      }

      paymentSetState(null, {
        initialized : true,
        items : data.items,
        address : data.address,
        paymentPlatform : data.paymentPlatform
      });
    });
  },

  [PaymentConstants.PAYMENT_RESET] : function (params) {
    paymentSetState(null, initialState);
  },

  [PaymentConstants.PAYMENT_PAY] : function (params) {
    paymentSetState(null, {
      isProcessingPayment : true
    });

    paymentPay(params, function (err, data) {
      if (err) {
        return paymentSetState(err, {
          isProcessingPayment : false
        });
      }

      if (data.paymentPlatform === "ecash") {
        paymentSetState(null, {
          proceedToPurchases : true,
          isProcessingPayment : false
        });
      } else if (data.paymentPlatform === "paypal") {
        paymentSetState(null, {
          paypalURI : data.paymentApprovalUrl,
          proceedToPaypal : true,
          isProcessingPayment : false
        });
      }
    });
  }
};

Stores.dispatchIndex = register(payload => {
  const {type, params} = payload.action;

  if (typeof Actions[type] === "function") {
    Actions[type](params);
  }

  return true;
});

export default Stores;

function paymentSetState(err, newState) {
  if (err) {
    toastr.error(err.xhr.responseText);
    console.error(err);
  }

  for (var prop in newState) {
    if (state.hasOwnProperty(prop)) {
      state[prop] = newState[prop];
    } else {
      console.warn("Invalid state for payment: " + prop);
    }
  }

  Stores.emitChange(PaymentConstants.PAYMENT_CHANGE_EVENTS);
}

function paymentInitialize (callback) {
  Request.get("cart", {
    payment : true
  }, callback);
}

function paymentPay (params, callback) {
  Request.post("gonnapaynow", {
    ...params
  }, callback);
}
