"use strict";

import keyMirror from "keymirror";

export default keyMirror({
  PAYMENT_CHANGE_EVENTS : null,
  PAYMENT_INITIALIZE : null,
  PAYMENT_RESET : null,
  PAYMENT_PAY : null,
});
