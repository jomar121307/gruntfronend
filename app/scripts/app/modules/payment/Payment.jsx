"use strict";

import React, {Component} from "react";
import {Link} from "react-router";
import Loader from 'halogen/PulseLoader';

import PaymentActions from "./PaymentActions.js";
import PaymentStores from "./PaymentStores.js";

export default class Payment extends Component {
  constructor (props) {
    super(props);

    this.state = PaymentStores.getInitialState();

    this.onChange = this.onChange.bind(this);

    this.renderEcashModalContent = this.renderEcashModalContent.bind(this);

    this.handlePay = this.handlePay.bind(this);
    this.handleECashPay = this.handleECashPay.bind(this);
  }

  componentWillMount () {
    PaymentStores.addChangeListener(this.onChange);
    PaymentActions.initialize();
  }

  componentWillUpdate (nextProps, nextState, nextContext) {
    if (nextState.proceedToPaypal && !this.state.proceedToPaypal && nextState.paypalURI) {
      window.location.href = nextState.paypalURI;
    } else if (nextState.proceedToPurchases && !this.state.proceedToPurchases) {
      toastr.success("Payment successfull!");
      $("#ecash-proceed-form").modal("hide");
      this.context.router.replace({
        pathname : "/myshop/purchases"
      });
    } else if (this.state.isProcessingPayment && !nextState.isProcessingPayment && !nextState.proceedToPaypal) {
      $("#ecash-proceed-form").modal("hide");
    }
  }

  componentWillUnmount () {
    PaymentActions.reset();
    PaymentStores.removeChangeListener(this.onChange);
  }

  render() {
    const {items, address, initialized} = this.state;
    const subtotal = _.reduce(items, function (acc, item) {
      return acc + (item.quantity * item.productId.price);
    }, 0);
    const shipping = _.reduce(items, function (acc, item) {
      let cost = _.findWhere(_.safe(item, "productId.metadata.shippingData", []),
        {location : address.address.province});

      if (!cost) {
        cost = _.safe(item, "productId.metadata.shippingData.0", {singleCost : 0});
      }

      return acc + parseFloat(cost.singleCost)
    }, 0);
    let handlingFee = subtotal + shipping;

    handlingFee =  (handlingFee * config.platformFee.percentage) / 100;
    handlingFee += config.platformFee.flat;
    handlingFee = Math.ceil(handlingFee * 100) / 100;

    if (!initialized) {
      return <Loader />
    }

    return (
      <div className="container" id="payment-container">
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">Overview & Payment
              <Link to="shipping" className="back-link">
                <span className="pull-right">
                  <span className="glyphicon glyphicon-chevron-left m-r-s"></span>
                  <span>BACK</span>
                </span>
              </Link>
            </h3>
          </div>
          <div className="panel-body">
            <ul className="list-unstyled">
              {items.map(this.listItems)}
            </ul>
            <div className="row">
              <div className="col-sm-12">
                <div>
                  <div>
                    <table className="table table-responsive">
                      <tbody>
                        <tr>
                          <td>
                            Item total
                          </td>
                          <td className="text-right">
                            ₱ {subtotal}
                          </td>
                        </tr>
                        <tr>
                          <td>
                            Shipping fee
                          </td>
                          <td className="text-right">
                            ₱ {shipping}
                          </td>
                        </tr>
                        <tr>
                          <td>
                            Handling fee
                          </td>
                          <td className="text-right">
                            ₱ {handlingFee}
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <strong>Total</strong>
                          </td>
                          <td className="text-right">
                            <strong className="text-success total-pay">₱ {subtotal + shipping + handlingFee}</strong>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <button className="btn btn-primary btn-block"
                      onClick={this.handlePay}>
                        submit order
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal fade"
          data-keyboard="false"
          data-backdrop="static"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="ecash-proceed-form"
          id="ecash-proceed-form">
            <div className="modal-dialog modal-sm">
              {this.renderEcashModalContent()}
            </div>
        </div>
        <div className="modal fade"
          data-keyboard="false"
          data-backdrop="static"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="loading-screen"
          id="loading-screen">
            <div className="modal-dialog modal-sm">
              <div className="modal-content">
                <div className="modal-body text-center">
                  <Loader color="#26A65B" size="16px" margin="4px"/>
                  <p className="bold f-i no-margin">Redirecting you to PayPal</p>
                </div>
              </div>
            </div>
        </div>
      </div>
    );
  }

  listItems (item) {
    return (
      <li key={item.id}>
        <div className="row">
          <div className="col-sm-3">
            <img src={_.safe(item, "productId.metadata.photos.1.secure_url")}
              className="img-responsive thumbnail"/>
          </div>
          <div className="col-sm-9">
              <Link to={`/product/${item.productId.id}`}>
                <h4>{_.safe(item, "productId.name")}</h4>
              </Link>
              <form className="form-inline">
                <div className="form-group">
                  <label for="product-quantity">Quantity:</label>
                  <span className="bold m-l-s">{item.quantity}</span>
                </div>
              </form>
              <h4 className="text-success">₱ {_.safe(item, "productId.price", 0) * item.quantity}</h4>
          </div>
        </div>
      </li>
    )
  }

  renderEcashModalContent () {
    const {paymentPlatform, isProcessingPayment} = this.state;

    if (isProcessingPayment && paymentPlatform === "ecash") {
      return (
        <div className="modal-content">
          <div className="modal-body text-center">
            <Loader color="#26A65B" size="16px" margin="4px"/>
            <p className="bold f-i no-margin">Processing your ecash payment.</p>
          </div>
        </div>
      )
    }

    return (
      <div className="modal-content">
        <div className="modal-header">
          <h4 className="modal-title text-center">Input e-cash credentials to proceed</h4>
        </div>
        <div className="modal-body text-center">
          <div className="row m-t-s">
            <label className="col-xs-4 lh-standard">Username</label>
            <div className="col-xs-8">
              <input ref="ecashUsername" className="form-control"/>
            </div>
          </div>
            <div className="row m-t-s">
              <label className="col-xs-4 lh-standard">Password</label>
              <div className="col-xs-8">
                <input type="password" ref="ecashPassword" className="form-control"/>
              </div>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-xs btn-default" data-dismiss="modal">
            Cancel
          </button>
          <button type="button" className="btn btn-xs btn-primary bold" onClick={this.handleECashPay}>
            <span className="glyphicon glyphicon-credit-card m-r-s"></span>
            Pay
          </button>
        </div>
      </div>
    )
  }

  onChange () {
    this.setState(PaymentStores.getState());
  }

  handlePay (e) {
    e.preventDefault();
    const {paymentPlatform} = this.state;

    if (paymentPlatform === "paypal") {
      $("#loading-screen").modal("show");
      return PaymentActions.pay();
    }

    $("#ecash-proceed-form").modal("show");
  }

  handleECashPay (e) {
    e.preventDefault();
    const username = this.refs["ecashUsername"].value.trim();
    const password = this.refs["ecashPassword"].value.trim();

    if (!username || !password) {
      return toastr.error("Invalid username or password");
    }

    PaymentActions.pay({
      username,
      password
    });
  }
};

Payment.contextTypes = {
  router : React.PropTypes.object.isRequired
}
