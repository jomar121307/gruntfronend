const categories = [
  {
    "name"  : "highlights at Bentanayan",
    "subcategory" : [
      {
        "groupheader" : { "name" : "Special Promotions"},
        "groupling" : [
                {
                  "name" : "Vouchers and Services",

                },
                {
                  "name" : "Flash Sale"
                },
                {
                  "name" : "Groceries"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Selected Sellers"},
        "groupling" : [
                {
                  "name" : "Alcatel"
                },
                {
                  "name" : "Intel"
                },
                {
                  "name" : "Xiaomi"
                },
                {
                  "name" : "Lenovo"
                },
                {
                  "name" : "Lucky HR"
                },
                {
                  "name" : "Great Winpard"
                },
                {
                  "name" : "The Perfumery"
                },
                {
                  "name" : "Nivea"
                },
                {
                  "name" : "Wawawei"
                },
                {
                  "name" : "Pearl Panda"
                },
                {
                  "name" : "The Cracking Money"
                },
                {
                  "name" : "Crown & Glory"
                },
                {
                  "name" : "Intex"
                },
                {
                  "name" : "Microsoft"
                },
                {
                  "name" : "Carbonn"
                },
                ]
      },
      {
        "groupheader" : { "name" : "Clearance Sale"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Bestsellers"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "New arrivals"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "International Deals"},
        "groupling" : []
      },
    ]
  },
  {
    "name"  : "fasion",
    "subcategory" : [
      {
        "groupheader" : { "name" : "Women"},
        "groupling" : [
                {
                  "name" : "New Arrivals"
                },
                {
                  "name" : "On Sale"
                },
                {
                  "name" : "Clothing"
                },
                {
                  "name" : "Bags"
                },
                {
                  "name" : "Shoes"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Men"},
        "groupling" : [
                {
                  "name" : "New Arrivals"
                },
                {
                  "name" : "On Sale"
                },
                {
                  "name" : "Bags"
                },
                {
                  "name" : "Clothing"
                },
                {
                  "name" : "Shoes"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Shop By Brand"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Style Picks"},
        "groupling" : []
      }
    ]
  },
  {
    "name"  : "Health & Beauty",
    "subcategory" : [
      {
        "groupheader" : { "name" : "Beauty for Her"},
        "groupling" : [
                {
                  "name" : "Makeup"
                },
                {
                  "name" : "Bath & Body"
                },
                {
                  "name" : "Skin Care"
                },
                {
                  "name" : "Hair Care"
                },
                {
                  "name" : "Beauty Tools"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Fragrances"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Men's Care"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Foods Supplement"},
        "groupling" : [
                {
                  "name" : "Weight Management"
                },
                {
                  "name" : "Beauty Supplements"
                },
                {
                  "name" : "Sports Nutrition"
                },
                {
                  "name" : "Well Being"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Medical Supplies"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Personal Care"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Pleasure"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Health & Beauty Inspirations"},
        "groupling" : []
      },
    ]
  },
  {
    "name"  : "Watches | Jewellry | Sunglasses",
    "subcategory" : [
      {
        "groupheader" : { "name" : "Watches"},
        "groupling" : [
                {
                  "name" : "Men's Watches"
                },
                {
                  "name" : "Women's Watches"
                },
                {
                  "name" : "New Arrivals"
                },
                {
                  "name" : "Sale"
                },
                ]
      },
      {
        "groupheader" : { "name" : "Jewellry"},
        "groupling" : [
                {
                  "name" : "Men's Jewellry"
                },
                {
                  "name" : "Women's Jewellry"
                },
                {
                  "name" : "New Arrivals"
                },
                {
                  "name" : "Sale"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Sunglasses"},
        "groupling" : [
                {
                  "name" : "Men's Sunglasses"
                },
                {
                  "name" : "Women's Sunglasses"
                },
                {
                  "name" : "New Arrivals"
                },
                {
                  "name" : "Sale"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Watches Brand A-Z Directory"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Style Picks"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Clearance Sale"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Holiday Must Haves"},
        "groupling" : []
      }
    ]
  },
  {
    "name"  : "Mobiles & Tablets",
    "subcategory" : [
      {
        "groupheader" : { "name" : "Mobiles"},
        "groupling" : [
                {
                  "name" : "Basic Phones"
                },
                {
                  "name" : "Smartphones"
                },
                ]
      },
      {
        "groupheader" : { "name" : "Jewellry"},
        "groupling" : [
                {
                  "name" : "Men's Jewellry"
                },
                {
                  "name" : "Women's Jewellry"
                },
                {
                  "name" : "New Arrivals"
                },
                {
                  "name" : "Sale"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Tablets"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Mobile Broadband"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Accessories"},
        "groupling" : [
                {
                  "name" : "Powerbanks"
                },
                {
                  "name" : "Screen Protector"
                },
                {
                  "name" : "Cables and Docks"
                },
                {
                  "name" : "Selfie Sticks"
                },
                {
                  "name" : "Phone Cases"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Shop By Brand"},
        "groupling" : [
                {
                  "name" : "Asus Zenfone"
                },
                {
                  "name" : "Apple iPad"
                },
                {
                  "name" : "Apple iPhone"
                },
                {
                  "name" : "Samsung Galaxy"
                },
                {
                  "name" : "Starmobile"
                },
                {
                  "name" : "Cherry Mobile"
                },
                {
                  "name" : "Lenovo"
                },
                {
                  "name" : "Torque Mobile"
                }
                ]
      }
    ]
  },
  {
    "name"  : "TV, Audio / Video | Gaming, Gadgets",
    "subcategory" : [
      {
        "groupheader" : { "name" : "Televisions"},
        "groupling" : [
                {
                  "name" : "LED TVs"
                },
                {
                  "name" : "Smart TVs"
                },
                ]
      },
      {
        "groupheader" : { "name" : "Audio Devices"},
        "groupling" : [
                {
                  "name" : "Home and Audio Theater"
                },
                {
                  "name" : "Headphones & Headsets"
                },
                {
                  "name" : "MP3 Players"
                },
                {
                  "name" : "Portable Speakers"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Gaming"},
        "groupling" : [
                {
                  "name" : "Gaming Consoles"
                },
                {
                  "name" : "Gaming Accessories"
                },
                {
                  "name" : "Games"
                },
                {
                  "name" : "PC Gaming"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Video"},
        "groupling" : [
                {
                  "name" : "DVD Players"
                },
                {
                  "name" : "Streaming Media Players"
                },
                {
                  "name" : "Projectors"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Wearable Technology"},
        "groupling" : [
                {
                  "name" : "Smartwatches"
                },
                {
                  "name" : "Activity & Fitness Trackers"
                }
                ]
      }
    ]
  },
  {
    "name"  : "Computers & Laptops",
    "subcategory" : [
      {
        "groupheader" : { "name" : "Laptops"},
        "groupling" : [
                {
                  "name" : "2-in-1s & Touchscreens"
                },
                {
                  "name" : "Ultrabooks"
                },
                {
                  "name" : "Macbooks"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Storage"},
        "groupling" : [
                {
                  "name" : "External Hard Drives"
                },
                {
                  "name" : "USB Flash Drives"
                },
                {
                  "name" : "Solid State Drives"
                }
                ]
      },
      {
        "groupheader" : { "name" : "PC Gaming"},
        "groupling" : [
                {
                  "name" : "Gaming Laptops"
                },
                {
                  "name" : "Gaming Mouse"
                },
                {
                  "name" : "Console Gaming"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Computer Accessories"},
        "groupling" : [
                {
                  "name" : "Keyboards"
                },
                {
                  "name" : "Monitors"
                },
                {
                  "name" : "Mouse"
                },
                {
                  "name" : "Speakers"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Desktop PCs"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Computer Components"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Network Components"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Printers & Ink"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Software"},
        "groupling" : []
      }
    ]
  },
  {
    "name"  : "Home Appliances",
    "subcategory" : [
      {
        "groupheader" : { "name" : "What's New"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Best Sellers"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Top Deals Under PHP 999"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Buying Guide"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Small Kitchen Appliances"},
        "groupling" : [
                {
                  "name" : "Blenders, Mixers & Grinders"
                },
                {
                  "name" : "Coffee Machines & Accessories"
                },
                {
                  "name" : "Electric Kettles & Thermo Pots"
                },
                {
                  "name" : "Juicers & Fruit Extractors"
                },
                {
                  "name" : "Rice Cookers & Steamers"
                },
                {
                  "name" : "Toasters & Sandwich Makers"
                },
                {
                  "name" : "Yogurt & Ice Cream Makers"
                },
                {
                  "name" : "Electric Grills"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Large Appliances"},
        "groupling" : [
                {
                  "name" : "Refrigerators"
                },
                {
                  "name" : "Microwaves & Ovens"
                },
                {
                  "name" : "Cooktops"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Cooling & Heating"},
        "groupling" : [
                {
                  "name" : "Air Conditioners"
                },
                {
                  "name" : "Fans"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Garment Care & Housekeeping"},
        "groupling" : [
                {
                  "name" : "Washing Machines"
                },
                {
                  "name" : "Irons"
                },
                {
                  "name" : "Garment Steamers"
                },
                {
                  "name" : "Sewing Machines"
                },
                {
                  "name" : "Vacuum Cleaners"
                }
                ]
      }
    ]
  },
  {
    "name"  : "Cameras",
    "subcategory" : [
      {
        "groupheader" : { "name" : "All Cameras"},
        "groupling" : [
                {
                  "name" : "DSLR/SLR"
                },
                {
                  "name" : "Mirrorless"
                },
                {
                  "name" : "Point & Shoot"
                },
                {
                  "name" : "Instant Cameras"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Video & Action Camera"},
        "groupling" : [
                {
                  "name" : "Sports & Action Cameras"
                },
                {
                  "name" : "Drones"
                },
                {
                  "name" : "Video Cameras"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Camera Acccessories"},
        "groupling" : [
                {
                  "name" : "Memory Cards"
                },
                {
                  "name" : "Lenses"
                },
                {
                  "name" : "Monopods & Tripods"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Gadgets & Others"},
        "groupling" : [
                {
                  "name" : "Security Cameras"
                },
                {
                  "name" : "Spy Cameras"
                },
                {
                  "name" : "Car Cameras"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Shop By Brands"},
        "groupling" : [
                {
                  "name" : "Nikon Cameras"
                },
                {
                  "name" : "Canon Cameras"
                },
                {
                  "name" : "GoPro Hero"
                },
                {
                  "name" : "Sony Cameras"
                },
                {
                  "name" : "Fujifilm"
                },
                {
                  "name" : "SJCAM Cameras"
                }
                ]
      }
    ]
  },
  {
    "name"  : "Home & Living",
    "subcategory" : [
      {
        "groupheader" : { "name" : "What's New"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "New Arrivals"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Home & Living Hot Picks"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Kitchen & Dining"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Bedding"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Outdoor & Garden"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Furniture"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Home Décor"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Home Improvement"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Bath"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Pets"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Stationery"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Storage & Organisation"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Lighting"},
        "groupling" : []
      },
    ]
  },
  {
    "name"  : "Travel & Luggage",
    "subcategory" : [
      {
        "groupheader" : { "name" : "Travel & Luggage"},
        "groupling" : [
                {
                  "name" : "Luggage"
                },
                {
                  "name" : "Kids on the Go"
                },
                {
                  "name" : "Travel Totes"
                },
                {
                  "name" : "Backpacks"
                },
                {
                  "name" : "Bags"
                },
                {
                  "name" : "Belt Bag"
                },
                {
                  "name" : "Laptop Bags & Cases"
                },
                {
                  "name" : "Weekender Bags"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Travel Accessories"},
        "groupling" : [
                {
                  "name" : "Packing Organizers"
                },
                {
                  "name" : "Travel Wallets"
                },
                {
                  "name" : "Passport Covers"
                },
                {
                  "name" : "Travel Kits"
                },
                {
                  "name" : "Travel Pillows and Eyemasks"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Shop By Brand"},
        "groupling" : [
                {
                  "name" : "American Tourister"
                },
                {
                  "name" : "High Sierra"
                },
                {
                  "name" : "Pilot"
                },
                {
                  "name" : "Deuter"
                },
                {
                  "name" : "Halo Bags"
                },
                {
                  "name" : "Hawk"
                },
                {
                  "name" : "Travelex"
                },
                {
                  "name" : "Racini"
                },
                ]
      }
    ]
  },
  {
    "name"  : "Baby & Toddler | Toys & Games",
    "subcategory" : [
      {
        "groupheader" : { "name" : "Baby & Toddler"},
        "groupling" : [
                {
                  "name" : "Diapering & Potty"
                },
                {
                  "name" : "Feeding"
                },
                {
                  "name" : "Baby Gear"
                },
                {
                  "name" : "Clothing & Accessories"
                },
                {
                  "name" : "Nursery"
                }
                ]
      },
      {
        "groupheader" : { "name" : "New Arrivals"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Sale"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Toys & Games"},
        "groupling" : [
                {
                  "name" : "Learning & Education"
                },
                {
                  "name" : "Blocks & Building Toys"
                },
                {
                  "name" : "Remote Control & Play Vehicles"
                },
                {
                  "name" : "Baby & Toddler Toys"
                },
                {
                  "name" : "Electronic Toys"
                }
                ]
      },
      {
        "groupheader" : { "name" : "New Arrivals"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Sale"},
        "groupling" : []
      }
    ]
  },
  {
    "name"  : "Sports & | Automotives",
    "subcategory" : [
      {
        "groupheader" : { "name" : "Sports & Outdoors"},
        "groupling" : [
                {
                  "name" : "Outdoor & Adventure"
                },
                {
                  "name" : "Swim and Surfwear"
                },
                {
                  "name" : "Yoga and Fitness"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Automotives"},
        "groupling" : [
                {
                  "name" : "Helmets"
                },
                {
                  "name" : "Car Care"
                },
                {
                  "name" : "Exterior Accessories"
                },
                {
                  "name" : "Car Accessories"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Shop By Brands"},
        "groupling" : [
                {
                  "name" : "Nike"
                },
                {
                  "name" : "Spalding"
                },
                {
                  "name" : "Babolat"
                },
                {
                  "name" : "Sava Bikes"
                },
                {
                  "name" : "Coleman"
                },
                {
                  "name" : "Ripcurl"
                },
                {
                  "name" : "Deuter"
                },
                {
                  "name" : "Spyder"
                },
                {
                  "name" : "Giant"
                },
                {
                  "name" : "Mars Kingdom Bikes"
                },
                {
                  "name" : "Tinker Motors"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Top Sellers"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "New Arrivals"},
        "groupling" : []
      }
    ]
  },
    {
    "name"  : "Media, Music & Books",
    "subcategory" : [
      {
        "groupheader" : { "name" : "Media, Music & Books"},
        "groupling" : [
                {
                  "name" : "Musical Instruments"
                },
                {
                  "name" : "Books"
                },
                {
                  "name" : "Music"
                },
                {
                  "name" : "Movies"
                },
                {
                  "name" : "TV Series"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Musical Instruments"},
        "groupling" : [
                {
                  "name" : "Guitars"
                },
                {
                  "name" : "Keyboards & Pianos"
                },
                {
                  "name" : "Drums & Percussion"
                },
                {
                  "name" : "Instrument Accessories"
                }
                ]
      },
      {
        "groupheader" : { "name" : "Books"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "Movies"},
        "groupling" : []
      },
      {
        "groupheader" : { "name" : "TV Series"},
        "groupling" : []
      }
    ]
  },
]

export default categories;
