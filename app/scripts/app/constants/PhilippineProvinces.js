const provinces = [
  {
    "_id" : "Abra"
  },
  {
    "_id" : "Agusan del Norte"
  },
  {
    "_id" : "Agusan del Sur"
  },
  {
    "_id" : "Aklan"
  },
  {
    "_id" : "Albay"
  },
  {
    "_id" : "Antique"
  },
  {
    "_id" : "Apayao"
  },
  {
    "_id" : "Aurora"
  },
  {
    "_id" : "Basilan"
  },
  {
    "_id" : "Bataan"
  },
  {
    "_id" : "Batanes"
  },
  {
    "_id" : "Batangas"
  },
  {
    "_id" : "Benguet"
  },
  {
    "_id" : "Biliran"
  },
  {
    "_id" : "Bohol"
  },
  {
    "_id" : "Bukidnon"
  },
  {
    "_id" : "Bulacan"
  },
  {
    "_id" : "Cagayan"
  },
  {
    "_id" : "Camarines Norte"
  },
  {
    "_id" : "Camarines Sur"
  },
  {
    "_id" : "Camiguin"
  },
  {
    "_id" : "Capiz"
  },
  {
    "_id" : "Catanduanes"
  },
  {
    "_id" : "Cavite"
  },
  {
    "_id" : "Cebu"
  },
  {
    "_id" : "Compostela Valley"
  },
  {
    "_id" : "Cotabato"
  },
  {
    "_id" : "Davao Oriental"
  },
  {
    "_id" : "Davao del Norte"
  },
  {
    "_id" : "Davao del Sur"
  },
  {
    "_id" : "Dinagat Islands"
  },
  {
    "_id" : "Eastern Samar"
  },
  {
    "_id" : "Guimaras"
  },
  {
    "_id" : "Ifugao"
  },
  {
    "_id" : "Ilocos Norte"
  },
  {
    "_id" : "Ilocos Sur"
  },
  {
    "_id" : "Iloilo"
  },
  {
    "_id" : "Isabela"
  },
  {
    "_id" : "Kalinga"
  },
  {
    "_id" : "La Union"
  },
  {
    "_id" : "Laguna"
  },
  {
    "_id" : "Lanao del Norte"
  },
  {
    "_id" : "Lanao del Sur"
  },
  {
    "_id" : "Leyte"
  },
  {
    "_id" : "Maguindanao"
  },
  {
    "_id" : "Marinduque"
  },
  {
    "_id" : "Masbate"
  },
  {
    "_id" : "Metro Manila - Caloocan"
  },
  {
    "_id" : "Metro Manila - Las Piñas"
  },
  {
    "_id" : "Metro Manila - Makati"
  },
  {
    "_id" : "Metro Manila - Malabon"
  },
  {
    "_id" : "Metro Manila - Mandaluyong"
  },
  {
    "_id" : "Metro Manila - Manila"
  },
  {
    "_id" : "Metro Manila - Marikina"
  },
  {
    "_id" : "Metro Manila - Muntinlupa"
  },
  {
    "_id" : "Metro Manila - Parañaque"
  },
  {
    "_id" : "Metro Manila - Pasay"
  },
  {
    "_id" : "Metro Manila - Pasig"
  },
  {
    "_id" : "Metro Manila - Pateros"
  },
  {
    "_id" : "Metro Manila - Quezon"
  },
  {
    "_id" : "Metro Manila - San Juan"
  },
  {
    "_id" : "Metro Manila - Taguig"
  },
  {
    "_id" : "Misamis Occidental"
  },
  {
    "_id" : "Misamis Oriental"
  },
  {
    "_id" : "Mountain Province"
  },
  {
    "_id" : "Negros Occidental"
  },
  {
    "_id" : "Negros Oriental"
  },
  {
    "_id" : "Northern Samar"
  },
  {
    "_id" : "Nueva Ecija"
  },
  {
    "_id" : "Nueva Vizcaya"
  },
  {
    "_id" : "Occidental Mindoro"
  },
  {
    "_id" : "Oriental Mindoro"
  },
  {
    "_id" : "Palawan"
  },
  {
    "_id" : "Pampanga"
  },
  {
    "_id" : "Pangasinan"
  },
  {
    "_id" : "Quezon"
  },
  {
    "_id" : "Quirino"
  },
  {
    "_id" : "Rizal"
  },
  {
    "_id" : "Romblon"
  },
  {
    "_id" : "Samar"
  },
  {
    "_id" : "Sarangani"
  },
  {
    "_id" : "Siquijor"
  },
  {
    "_id" : "Sorsogon"
  },
  {
    "_id" : "South Cotabato"
  },
  {
    "_id" : "Southern Leyte"
  },
  {
    "_id" : "Sultan Kudarat"
  },
  {
    "_id" : "Sulu"
  },
  {
    "_id" : "Surigao del Norte"
  },
  {
    "_id" : "Surigao del Sur"
  },
  {
    "_id" : "Tarlac"
  },
  {
    "_id" : "Tawi-Tawi"
  },
  {
    "_id" : "Zambales"
  },
  {
    "_id" : "Zamboanga Sibugay"
  },
  {
    "_id" : "Zamboanga del Norte"
  },
  {
    "_id" : "Zamboanga del Sur"
  }
]

export default provinces;
