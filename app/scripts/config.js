"use strict";

var pjson = require('../../package.json');

//
// Construct www and api urls
//
let wwwPort = location.port;
let apiUrl = location.hostname.split(".");
const isLocal = location.hostname.indexOf("localhost") === 0;
const isStaging = location.hostname.indexOf("staging-www") === 0;

if (isLocal) {
  if (wwwPort.length > 0) {
    wwwPort = ":" + (parseInt(wwwPort) + 1);
  } else {
    wwwPort = ":81";
  }
  apiUrl = "https://" + location.hostname + wwwPort;
} else if (isStaging) {
  apiUrl[1] = pjson.appGroup;
  apiUrl[0] = "staging-api";
  apiUrl = apiUrl.join(".");
  apiUrl = "https://" + apiUrl;
} else {
  apiUrl = `https://api.${pjson.appGroup}.com`
}

module.exports = {

   appname : pjson.appname,

  //  apiServer :  apiUrl,

  //appname :  "bentanayan-com",

  apiServer : "https://api.bentanayan.com/",

  appType : "zcommerce",

  cloudinaryCloudName : "bentanayan-com",

  cloudinaryUrl : "https://api.cloudinary.com/v1_1/bentanayan-com/",

  defaultCoverVideo : "https://res.cloudinary.com/ogagtep/video/upload/v1455795751/wckxrmn1hsskz5dlbd9v.mp4",

  defaultProfileVideo : "https://res.cloudinary.com/ogagtep/video/upload/v1455242687/lcic4ksyagqlsuj1kex3.mp4",

  defaultProductPhoto : "https://res.cloudinary.com/www-zoogtech-com/image/upload/" +
  "c_scale,h_200/v1443083691/defaultproduct.jpg",

  defaultYoutubeProfileVideo : "foE1mO2yM04",

  defaultYoutubeCoverVideo : "CuH3tJPiP-U",

  facebook : {
    fields : "id,about,age_range,picture,bio,context,email,first_name,gender," +
    "hometown,link,location,middle_name,name,timezone,website,work"
  },

  freshTokenDuration : 1000 * 60 * 60 * 24, // 1 day(s)

  isRc : isStaging,

  rc : pjson.rc,

  tokenRefreshInterval : 1000 * 60, // 60 sec(s),

  uploadLimits : {
    videoSize : 20 // MB
  },

  paypalRefundConfig : process.env.PAYPAL_REFUND_CONFIG || "https://www.sandbox.paypal.com/?cmd=_profile-api-grant-authorization",

  paypalApiUsername : process.env.PAYPAL_API_USERNAME || "payments-facilitator_api1.bespokegolf.asia",

  isLocal : isLocal,

  version : pjson.version,

  youtubeKey : "AIzaSyDktaozzO9Yhsho5NsSKT_lXcR8QyBG7Sc",

  platformFee : {
    percentage : 1,
    flat : 0.3,
  },

  emailRegex : /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
};
