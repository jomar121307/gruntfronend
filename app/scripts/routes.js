"use strict";

import React from "react";
import {render} from "react-dom";
import {Router, Route, IndexRoute, hashHistory, browserHistory} from "react-router";
import Utils from "./utils/utils.js";

import About from "./app/views/About.jsx";
import App from "./app/views/App.jsx";
import AddProduct from "./app/modules/addproduct/AddProduct.jsx";
import Cart from "./app/modules/cart/Cart.jsx";
import Market from "./app/modules/market/Market.jsx";
import Messages from "./app/modules/messages/Messages.jsx";
import NotFound from "./app/views/NotFound.jsx";
import PrivacyPolicy from "./app/views/PrivacyPolicy.jsx";
import Product from "./app/modules/product/Product.jsx";
import Profile from "./app/modules/profile/Profile.jsx";
import ProfileComponents from "./app/modules/profile/components/index.js";
import Payment from "./app/modules/payment/Payment.jsx";
import Shipping from "./app/modules/shipping/Shipping.jsx";
import ResetPassword from "./app/modules/resetpassword/ResetPassword.jsx";
import TaC from "./app/views/TermsAndConditions.jsx";

import AppStores from "./app/stores/AppStores.js";

function isCategoryValid (categories, params) {
  const category = _.findWhere(categories, {slug : params[0]}) || [];
  const subcat = category && _.filter(category.subcategories, subcat => subcat.groupheader.slug === params[1]);

  if (params.length === 3) {
    const groupling = !!_.safe(subcat, "length") && _.filter(subcat[0].groupling, el => el.slug === params[2]);
    return !!_.safe(groupling, "length");
  } else if (params.length && params.length < 3) {
    return !!_.safe(subcat, "length");
  }

  return false;
}

function auth (nextState, replace) {
  if (!AppStores.getState().isLoggedIn) {
    replace({
      pathname : "/"
    });
  }
}

function checkAdmin (nextState, replace, callback) {
  if (!AppStores.getState().isLoggedIn) {
    replace({pathname : "/"});
    callback();
    return;
  }

  Utils.getUser((err, user) => {
    if (err) {
      console.error(err);
    } else if (user && user.isAdmin) {
      return callback();
    }

    replace({pathname : "/"});
    callback();
  });
}

function checkCategory (nextState, replace, callback) {
  const params = nextState.params.splat.split("/");
  const categories = AppStores.getState().categories

  if (!categories.length) {
    return Utils.getCategories((err, data) => {
      if (err) {
        console.error(err);
      } else if (isCategoryValid(data[0].metadata, params)) {
        return callback();
      }

      replace({pathname : "/"});
      callback();
    });
  } else if (isCategoryValid(categories, params)) {
    return callback();
  }

  replace({pathname : "/"});
  callback();
}

function checkPasswordResetToken (nextState, replace, callback) {
  const query = nextState.location.query;

  if (query.rt) {
    Utils.verifyToken({
      token : query.rt,
      type : "resetPasswordToken"
    }, (err, data) => {
      if (err) {
        console.error(err);
      } else if (data.status === "valid") {
        return callback();
      }

      replace({pathname : "/notFound"})
      callback();
    });
  } else {
    callback();
  }
}

module.exports = function(defaultRoute, notFound, routes) {
  const categories = AppStores.getState().categories;

  render(
    <Router history={hashHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={Market} />
        <Route path="cart" component={Cart} />
        <Route path="shipping" component={Shipping} />
        <Route path="payment" component={Payment} />
        <Route path="resetpassword" component={ResetPassword} onEnter={checkPasswordResetToken} />
        <Route path="termsandconditions" component={TaC} />
        <Route path="messages(/:id)" component={Messages} onEnter={auth} />
        <Route path="shop/:id" component={Profile} />
        <Route path="myshop" component={Profile} onEnter={auth}>
          <Route path="purchases" component={ProfileComponents.PurchasedItems} />
          <Route path="orders" component={ProfileComponents.CustomerOrders} />
          <Route path="shopsettings" component={ProfileComponents.ShopSettings} />
          <Route path="controlpanel" component={ProfileComponents.ControlPanel} onEnter={checkAdmin} />
        </Route>
        <Route path="about" component={About} />
        <Route path="product/add" component={AddProduct} onEnter={auth} />
        <Route path="product/:id" component={Product} />
        <Route path="product/:id/edit" component={AddProduct} onEnter={auth} />
        <Route path="c/*" component={Market} onEnter={checkCategory} />
        <Route path="*" component={NotFound} />
      </Route>
    </Router>,
    $("#app-body")[0]
  );
};

